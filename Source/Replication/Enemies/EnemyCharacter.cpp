// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "EnemyCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Ui/FloatingBarWidgetComponent.h"

AEnemyCharacter::AEnemyCharacter() {


	BaseDamage = 35.f;
	DamageType = EDamageType::Physical;
	CriticalChance = .25f;

	SightRadius = 800;
	LoseSightRadius = SightRadius + 1000.;
	PatrolRadius = 5000.f;
	AttackRange = 50.f;
	AttackTraceDistance = 90.f;
	ChaseRadius = 1000.f;
	PatrolWalkSpeed = 200.f;
	AggroedWalkSpeed = 600.f;
	FollowPlayerRadius = 2000.f;
	
	bWasAggroed = false;
	bIsAggressive = true;
	PrimaryActorTick.bCanEverTick = false;

	// Setup the perception component
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception Component"));
	AISightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	
	AIPerceptionComponent->ConfigureSense(*AISightConfig);
	AIPerceptionComponent->SetDominantSense(AISightConfig->GetSenseImplementation());
	AIPerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AEnemyCharacter::OnUpdatePerception);
	AIPerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyCharacter::OnUpdateTargetPerception);
	
	AISightConfig->SightRadius = SightRadius;
	AISightConfig->LoseSightRadius = LoseSightRadius;
	AISightConfig->PeripheralVisionAngleDegrees = 90.f;
	AISightConfig->DetectionByAffiliation.bDetectEnemies = true;
	AISightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	AISightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	AIPerceptionComponent->ConfigureSense(*AISightConfig);
	
	WidgetVisibilitySphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::OnWidgetBeginOverlap);
	WidgetVisibilitySphere->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::OnWidgetEndOverlap);

	HitArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("HitArrowComponent"));
	HitArrowComponent->ArrowSize = 0.1f;
	HitArrowComponent->SetRelativeLocation(FVector(0, 0, 20));
	HitArrowComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	MaxHealth = 100.f;
	Health = MaxHealth;

	StartingLocation = GetActorLocation();
	GetCharacterMovement()->MaxWalkSpeed = PatrolWalkSpeed;
	RepAIController = Cast<AAIController>(GetController());
	if (RepAIController) {
		
		RepAIController->ReceiveMoveCompleted.AddDynamic(this, &AEnemyCharacter::OnMoveCompleted);
			
	}

	if (AISightConfig) {
		UAIPerceptionSystem::RegisterPerceptionStimuliSource(GetWorld(), AISightConfig->GetSenseImplementation(), this);
	}
	Patrol();

	BPIE_InitializeUI();
}

void AEnemyCharacter::CheckIsWidgetOverlappingCharacter() {

	//Check for initial overlap of character for showing Enemy Widget bar
	UWorld* World = GetWorld();
	if (World) {
		ACharacter* Character = UGameplayStatics::GetPlayerCharacter(World, 0); //Only works for single player, not multiplayer.
		if (Character) {
			bool isOverlapping = WidgetVisibilitySphere->IsOverlappingActor(Character);
			FloatingBarWidgetComponent->SetVisibility(isOverlapping);
		}
	}
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemyCharacter::Patrol() {

	UWorld* World = GetWorld();
	if (World) {
		UNavigationSystem* NavSystem = World->GetNavigationSystem();
		if (NavSystem) {

			 FNavLocation NavPatrolLocation;
			 NavSystem->GetRandomReachablePointInRadius(StartingLocation, PatrolRadius, NavPatrolLocation);
			 FVector patrolLocation = NavPatrolLocation.Location;
			if (RepAIController) {

				FAIMoveRequest mrequest;
				mrequest.SetGoalLocation(patrolLocation);
				RepAIController->MoveTo(mrequest);
			}
		}
	}
}


void AEnemyCharacter::Chase() {

	CurrentState = EEnemyState::Chase;
	GetCharacterMovement()->MaxWalkSpeed = ChaseSpeed;
	FAIMoveRequest mrequest;
	mrequest.SetGoalActor(TargetActor);
	mrequest.SetAcceptanceRadius(AttackRange);
	RepAIController->MoveTo(mrequest);
		
}

void AEnemyCharacter::ReturnHome() {

	CurrentState = EEnemyState::ReturnHome;
	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Yellow, "ReturnHome");
	FAIMoveRequest mrequest;
	mrequest.SetGoalLocation(StartingLocation);
	RepAIController->MoveTo(mrequest);
	
}



int AEnemyCharacter::GetNextAnimationIndex() {


	if ((CurrentAnimationIndex + 1) >= AttackAnimations.Num()) {

		return 0;
	}

	return (CurrentAnimationIndex + 1);
}

void AEnemyCharacter::Investigate(float DeltaTime) {

	CurrentState = EEnemyState::Investigate;
	float WaitTime = FMath::RandRange(1.f, 4.f);
	GetWorld()->GetTimerManager().SetTimer(FollowDistanceTimerHandle, this, &AEnemyCharacter::Patrol, WaitTime);
}

void AEnemyCharacter::OnWidgetBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {



	if (IsAlive() && !FloatingBarWidgetComponent->IsVisible() && OtherActor->IsA(AReplicationCharacter::StaticClass())) {

		FloatingBarWidgetComponent->SetVisibility(true);
	}
}

void AEnemyCharacter::OnWidgetEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	if (IsAlive() && FloatingBarWidgetComponent->IsVisible() && OtherActor->IsA(AReplicationCharacter::StaticClass())) {

		FloatingBarWidgetComponent->SetVisibility(false);

	}
}

void AEnemyCharacter::FollowTargetDistance() {

	//Distance away from home/start location
	//If too far from home (determined by FollowPlayerRadius), then return home
	float DistanceFromHome = (StartingLocation - GetActorLocation()).Size();
	if (DistanceFromHome > FollowPlayerRadius && !bIsReturningHome) {

		OnReset();
	}
	
}


void AEnemyCharacter::OnUpdatePerception(TArray<AActor*>actors) {

	if (!bWasAggroed && bIsAggressive) {

		for (int i = 0; i < actors.Num(); i++) {

			AActor* actor = actors[i];
			AReplicationCharacter* repCharacter = Cast<AReplicationCharacter>(actor);
			if (repCharacter && repCharacter->IsAlive()) {

				//AReplicationCharacter* repCharacter = Cast<AReplicationCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
				repCharacter->OnPlayerDied.AddDynamic(this, &AEnemyCharacter::OnTest);
				OnAggroPulled(repCharacter);

				break;
			}
		}
	} 
}

void AEnemyCharacter::OnUpdateTargetPerception(AActor* Actor, FAIStimulus stimulus) {


	if (!stimulus.WasSuccessfullySensed()) {

		OnReset();
	}
}

void AEnemyCharacter::OnAggroPulled(ABaseCharacter* Target)
{
	if (!bWasAggroed && !bIsReturningHome) {

		bIsPatrolling = false;
		bWasAggroed = true;
		TargetActor = Target;		
		
		GetCharacterMovement()->MaxWalkSpeed = AggroedWalkSpeed;
		GetWorld()->GetTimerManager().SetTimer(FollowDistanceTimerHandle, this, &AEnemyCharacter::FollowTargetDistance, 1.f, true);
	
		if (InAttackRange()) {
			PerformAttack();
		} else {
			Chase();
		}
	}
}

//-------------------------------------------------------------------- Attacking --------------------------------------------------------

bool AEnemyCharacter::InAttackRange() {

	if (TargetActor) {
		float distance = TargetActor->GetDistanceTo(this);
		return distance <= AttackRange;
	}

	return false;
}

void AEnemyCharacter::Attack() {

	PerformAttack();
}

void AEnemyCharacter::PerformAttack() {

	//if (IsAlive() && TargetActor ) {
	CurrentState = EEnemyState::Attack;

	if (IsAlive() && bWasAggroed && !bIsReturningHome) {

		FVector ActorLocation = this->GetActorLocation();
		FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(ActorLocation, TargetActor->GetActorLocation());
		SetActorRotation(NewRotation);
		
		CurrentAttackMontage = AttackAnimations[CurrentAnimationIndex];
		PlayAnimMontage(CurrentAttackMontage);
	
		float attackDelay = CurrentAttackMontage->SequenceLength;
		GetWorld()->GetTimerManager().SetTimer(AttackTimerHandle, this, &AEnemyCharacter::AttackTimerExpired, attackDelay);
	}

}


//Called from Animation Blueprint when attack is at attack point
void AEnemyCharacter::OnNotifyAttack() {

	const FVector Start = GetActorLocation();
	const FVector End = Start + (GetActorRotation().Vector() * AttackTraceDistance);
	FHitResult OutHit;
	
	bool HasHit = UCombatFunctionLibrary::TraceLine(this, Start, End, OutHit, ECollisionChannel::ECC_Pawn);
	if (HasHit) {

		IDamageable* Damageable = Cast<IDamageable>(OutHit.GetActor());
		if (Damageable) {

			Damageable->OnReceiveDamage(BaseDamage, DamageType, Element, CriticalChance, this, nullptr);
		}
	}
}


void AEnemyCharacter::AttackTimerExpired() {

	CurrentAttackMontage = nullptr;
	CurrentAnimationIndex = GetNextAnimationIndex();

	if (InAttackRange()) {

		PerformAttack();
	} else {
		Chase();
	}
	
}

void AEnemyCharacter::OnReceiveDamage(float Damage, EDamageType Type, TSubclassOf<AMasterElement> ElementClass, int CritChance, ABaseCharacter* Attacker, ASkill* Spell) {

	if (IsAlive() && !UCombatFunctionLibrary::IsEnemy(Attacker)) {

		FLibDamage DamageInfo = UCombatFunctionLibrary::CalculateFinalDamage(Damage, CritChance, ElementClass, Element);
		float AppliedDamage = DamageInfo.Damage* -1;

		FLinearColor FloatingTextColor;

		switch (Type) {

		case EDamageType::Healing:
			FloatingTextColor = FLinearColor::Yellow;
			break;

		default:
			FloatingTextColor = FLinearColor::Red;
			break;
		}

		FText FloatingMessage;
		if (DamageInfo.bIsCritical)
			FloatingMessage = FText::FromString(("Critical Hit!"));

		UCombatFunctionLibrary::ShowFloatingWidget(FloatingMessage, AppliedDamage, FloatingTextColor, this, FloatingWidgetClass);

		UpdateHealth(AppliedDamage);

		if (Health <= 0.f) {

			OnDeath(Attacker);
		} else {

			SimulateHit();
			OnAggroPulled(Attacker);
		}
	}
}


void AEnemyCharacter::UpdateHealth(float value) {

	//Multiply by 1.f because Health is an integer
	Health = FMath::Clamp(Health + value, 0.f, MaxHealth);
	BPIE_UpdateHealthBar();
}


void AEnemyCharacter::OnDeath(AActor* Killer) {

	if (bIsSelected) {

		OnSelectionEnd(SelectingPlayer);
	}

	Super::OnDeath(Killer);
	AReplicationCharacter* RepCharacter = Cast<AReplicationCharacter>(Killer);
	if (RepCharacter) {
		RepCharacter->UpdatExperience(ExpForKill);
	}
	
	FloatingBarWidgetComponent->SetVisibility(false);

	if (FollowDistanceTimerHandle.IsValid()) {

		FollowDistanceTimerHandle.Invalidate();
		GetWorld()->GetTimerManager().ClearTimer(FollowDistanceTimerHandle);
	}

	UWorld* World = GetWorld();
	if (World) {
		World->GetTimerManager().SetTimer(DieTimerHandle, this, &AEnemyCharacter::OnDieTimerHandled, DeadBodyTime);
		bTimerExpired = false;
	}

}

void AEnemyCharacter::OnDieTimerHandled() {

	GetMesh()->GlobalAnimRateScale = 0.f; //Pauses animation
	//bIsAlive = false;
	if (bIsRespawning) {
		GetWorld()->GetTimerManager().SetTimer(WaitRespawnTimerHandle, this, &AEnemyCharacter::OnWaitRespawnTimerHandled, WaitRespawnDelayTime);

	} else {
		Destroy();
	}
	bTimerExpired = true;
	//DieTimerExpired();
}


void AEnemyCharacter::OnWaitRespawnTimerHandled() {
	
	SetActorHiddenInGame(true);
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &AEnemyCharacter::Respawn, RespawnDelayTime);
}


void AEnemyCharacter::Respawn() {

	Super::Respawn();
	BPIE_UpdateHealthBar();
	GetMesh()->GlobalAnimRateScale = 1.f;

	bWasAggroed = false;
	bIsPatrolling = true;
	SetActorLocation(StartingLocation);
	TargetActor = nullptr;
	CurrentAnimationIndex = 0;
	bIsReturningHome = false;
	GetCharacterMovement()->MaxWalkSpeed = PatrolWalkSpeed;
	GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	
	CheckIsWidgetOverlappingCharacter();
	SetActorHiddenInGame(false);
	Patrol();

}


void AEnemyCharacter::OnReset() {

	
	StopAnimMontage();//Stop playing attack animation
	bIsReturningHome = true;
	CurrentAnimationIndex = 0;

	FollowDistanceTimerHandle.Invalidate();
	GetWorld()->GetTimerManager().ClearTimer(FollowDistanceTimerHandle);
	TargetActor = nullptr;
	bWasAggroed = false;
	ReturnHome();
}

void AEnemyCharacter::OnTest() {

	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Purple, "OnTest");
	OnReset();

}


void AEnemyCharacter::OnSelected(AReplicationCharacter* Player) {

	if (IsAlive() && !bIsSelected) {

		GetMesh()->SetRenderCustomDepth(true);
		SelectingPlayer = Player;
		Player->SelectedEnemy = this;
		bIsSelected = true;
		SelectingPlayer->UIUpdateEnemySelected(Name, CurrentLevel);
		BPIE_UpdateHealthBar();
	}

}


void AEnemyCharacter::OnSelectionEnd(AReplicationCharacter* Player) {
	
	if (bIsSelected) {

		GetMesh()->SetRenderCustomDepth(false);
		FText Text = FText::FromString("Empty");
		SelectingPlayer->UIUpdateEnemySelected(Text, CurrentLevel);
		Player->SelectedEnemy = nullptr;
		SelectingPlayer = nullptr;
		bIsSelected = false;
		//UIUpdateHealthBar();
	}

}


void AEnemyCharacter::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type result) {
	FString msg;

	switch (result) {

		case EPathFollowingResult::Success:

				switch (CurrentState) {//Character can complete a move from a patrol or chase state 

					case EEnemyState::Patrol:
						bIsPatrolling = false;
						Investigate(1);
						break;

					case EEnemyState::Chase://MoveToLocation acceptance radius is the attack range, We only only transition from Chase to attack on sucess
						PerformAttack();
						break;

					case EEnemyState::ReturnHome://When made it back to starting location, reset isRunningBack to false

						GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Purple, "OnMoveCompleted() Return home case");

						CurrentState = EEnemyState::Patrol;
						bIsPatrolling = true;
						GetCharacterMovement()->MaxWalkSpeed = PatrolWalkSpeed;
						Patrol();
						bIsReturningHome = false;
						break;

					default:
						Investigate(1);
						break;
					}
			break;//End Success case

		case EPathFollowingResult::Aborted:
			
			switch (CurrentState) {//Character can complete a move from a patrol or chase state 

			case EEnemyState::Patrol:
				msg = "Patrol";
				break;

			case EEnemyState::Chase://MoveToLocation acceptance radius is the attack range, We only only transition from Chase to attack on sucess
				msg = "chase";
				break;

			case EEnemyState::ReturnHome://When made it back to starting location, reset isRunningBack to false

				msg = "ReturnHome";
				break;

			
			}

			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, "Aborted " + RequestID.ToString() + msg);

			break;

		case EPathFollowingResult::Blocked:
			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, "Blocked");
			break;

		case EPathFollowingResult::Invalid:
			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, "Invalid");
			break;

		case EPathFollowingResult::OffPath:
			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, "OffPath");
			break;

		

	}
}
