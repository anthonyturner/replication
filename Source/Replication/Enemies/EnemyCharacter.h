// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Player/BaseCharacter.h"
#include "AIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "SkillSystem/Interfaces/Damageable.h"
#include "SkillSystem/Interfaces/Selectable.h"

#include "EnemyCharacter.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EEnemyState : uint8
{
	Patrol 	UMETA(DisplayName = "Patrol"),
	Chase 	UMETA(DisplayName = "Chase"),
	Attack	UMETA(DisplayName = "Attack"),
	Investigate UMETA(DisplayName = "Investigate"),
	ReturnHome UMETA(DisplayName = "ReturnHome")
};


UCLASS()
class REPLICATION_API AEnemyCharacter : public ABaseCharacter, public IDamageable, public ISelectable
{
	GENERATED_BODY()
	
public:

	AEnemyCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		int ExpForKill;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float SightRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float LoseSightRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float PatrolRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float PatrolWalkSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float AggroedWalkSpeed;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float ChaseRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float ChaseSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float AttackRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float AttackTraceDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		float FollowPlayerRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		bool bWasAggroed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		bool bIsReturningHome;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		bool bIsPatrolling;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		FVector StartingLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		EEnemyState CurrentState; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		ABaseCharacter* TargetActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
		TArray<UAnimMontage*>AttackAnimations;

	UPROPERTY()
		UAnimMontage* CurrentAttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
		int CurrentAnimationIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		UAIPerceptionComponent* AIPerceptionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		UAISenseConfig_Sight* AISightConfig;

	UPROPERTY()
		FTimerHandle AttackTimerHandle;
	
	UPROPERTY()
		FTimerHandle FollowDistanceTimerHandle;

	bool bIsAttacking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		bool bIsSelected;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		AReplicationCharacter* SelectingPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyInfo)
		UArrowComponent* HitArrowComponent;

public:

	//UFUNCTION()
		virtual void OnReceiveDamage(float BaseDamage, EDamageType Type, TSubclassOf<AMasterElement> ElementClass, int CriticalChance, ABaseCharacter* Attacker, ASkill* Spell) override;
		virtual void OnSelected(AReplicationCharacter* Player) override;
		virtual void OnSelectionEnd(AReplicationCharacter* Player) override;
		virtual void UpdateHealth(float value) override;

	//Functions
	UFUNCTION(BlueprintCallable, Category = EnemyInfo)
	void PerformAttack();

	UFUNCTION(BlueprintCallable, Category = EnemyInfo)
		void OnNotifyAttack();

	UFUNCTION()
	void AttackTimerExpired();

	UFUNCTION(BlueprintPure, Category = EnemyInfo)
		int GetNextAnimationIndex();

	UFUNCTION(BlueprintPure, Category = EnemyInfo)
		bool InAttackRange();

	UFUNCTION(BlueprintCallable, Category = EnemyInfo)
		void OnAggroPulled(ABaseCharacter* Target);
	
	UFUNCTION()
		void FollowTargetDistance();

	UFUNCTION()
	void OnReset();

	UFUNCTION()
		void OnTest();

	virtual void OnDeath(AActor* Killer) override;
	virtual void Respawn() override;
	virtual void OnDieTimerHandled() override;
	virtual void OnWaitRespawnTimerHandled() override;

	void CheckIsWidgetOverlappingCharacter();

	UFUNCTION()
		virtual void OnWidgetBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) override;

	UFUNCTION()
		virtual void OnWidgetEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
protected:

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type result);
	
	UFUNCTION()
		void OnUpdatePerception(TArray<AActor*>actors);

	UFUNCTION()
		void OnUpdateTargetPerception(AActor* Actor, FAIStimulus stimulus);

private:

	void Patrol();
	void Attack();
	void Chase();
	void ReturnHome();
	void Investigate(float DeltaTime);
	
	UPROPERTY()
	AAIController* RepAIController;

	float InvestigateTime;
};
