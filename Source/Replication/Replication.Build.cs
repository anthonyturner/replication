// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Replication : ModuleRules
{
	public Replication(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core",
                "CoreUObject",
                "Engine",
                "InputCore",
                "UMG",
                "AIModule",
                "OnlineSubsystem",
                "OnlineSubsystemUtils",
                "GameplayTasks",
                "AssetRegistry",
                 "Landscape"
        });

        PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
        DynamicallyLoadedModuleNames.AddRange(
         new string[] {
                "OnlineSubsystemNull",
                "NetworkReplayStreaming",
                "NullNetworkReplayStreaming",
                "HttpNetworkReplayStreaming"
         }
     );

        PrivateIncludePathModuleNames.AddRange(
        new string[] {
                "NetworkReplayStreaming"
        }
    );
        // END STEAM INTEGRATION
    }
}
