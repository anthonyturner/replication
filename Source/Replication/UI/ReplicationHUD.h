// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "ReplicationHUD.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AReplicationHUD : public AHUD
{
	GENERATED_BODY()
	
	
public:

	virtual void DrawHUD() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = UI)
	UMaterialInstance* crossHair_MAT;

	UFUNCTION(BlueprintImplementableEvent)
	void ShowInventory();
	
	UFUNCTION(BlueprintImplementableEvent)
	void HideInventory();
};
