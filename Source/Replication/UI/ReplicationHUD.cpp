/*
File: ReplicationGameHUD.cpp
Description: The way in which the game communicates and interacts with the player is extremely important. 
User Interfaces (UIs) and Heads-up Displays (HUDs) are the games' way of providing information 
about the game to the player and in some cases allowing the player to interact with the game.

Unreal Engine 4 provides multiple means of creating UIs and HUDs. 
The Canvas class can be used to draw directly to the screen at a low level, overlaid onto the world.

The game's Interface is used to convey information to the player and provide a means of prompting the user for directed input. 
A game interface generally consists of two main elements: the heads-up display (HUD) and menus or user interfaces (UIs).

The HUD refers to the status and information overlaid on the screen during gameplay. 
The purpose of the HUD is to inform the player of the current state of the game, i.e. the score, their health, the amount of time remaining, etc. 
The HUD is usually non-interactive, meaning the player does not click on elements of the HUD, 
though this becomes a gray area in certain types of games where the HUD and user interfaces are hard to separate.

The HUD is the base object for displaying elements overlaid on the screen. 
Every human-controlled player in the game has their own instance of the AHUD class which draws to their individual Viewport. 
In the case of splitscreen multiplayer games, multiple Viewports share the same screen, but each HUD still draws to its own Viewport. 
The type, or class, of HUD to use is specified by the gametype being used.

https://docs.unrealengine.com/latest/INT/Gameplay/Framework/UIAndHUD/index.html


*/
#include "Replication.h"
#include "ReplicationHUD.h"


/*
The Main Draw loop for the hud.
Gets called before any messaging. Should be subclassed
*/
void AReplicationHUD::DrawHUD() {

	Super::DrawHUD();
	float midPointX = (Canvas->SizeX / 2);
	float midPointY = (Canvas->SizeY / 2);

	DrawMaterial(crossHair_MAT, midPointX, midPointY, 100.f, 100.f, 0.f, 0.f, 1.f, 1.f);
	
}
