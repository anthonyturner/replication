// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "LobbyMenu.h"



void ULobbyMenu::SetMapImage(UTexture2D* newMapImage) {

	mapImage = newMapImage;
}
void ULobbyMenu::SetMapName(const FText& newMapName) {

	mapName = newMapName;

}

void ULobbyMenu::SetMapTime(const FText& newMapTime) {

	mapTime = newMapTime;
}