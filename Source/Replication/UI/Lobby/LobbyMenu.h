// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "LobbyMenu.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API ULobbyMenu : public UUserWidget
{
	GENERATED_BODY()
	
	
public:

	void SetMapImage(UTexture2D* mapImage);

	void SetMapName(const FText& mapName);

	void SetMapTime(const FText& mapTime);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerMenuSettings)
		UTexture2D* mapImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerMenuSettings)
		FText mapName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerMenuSettings)
		FText mapTime;
	
};
