// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "FloatingBarWidget.h"


void UFloatingBarWidget::UpdateFloatingBar(FText Name, FText Level, bool IsAgressive) {

	OnUpdateNameText(Name, IsAgressive);
	OnUpdateLevelText(Level, IsAgressive);
}
