// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FloatingWidget.generated.h"

class ABaseCharacter;
UCLASS()
class REPLICATION_API UFloatingWidget : public UUserWidget
{
	GENERATED_BODY()


	public:

		virtual void NativeConstruct() override;
		virtual void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;

		UFUNCTION(BlueprintImplementableEvent)
		void OnReceiveValue(const FText& Msg, float value, FLinearColor TextColor);

		void SetActorReference(AActor* ActorRef);
		
	private:
		void HandleShowingWidget();
	
		UPROPERTY()
		FTimerHandle FloatingWidgetTimerHandle;
		
		UPROPERTY()
		AActor* ActorReference;
	
		UPROPERTY()
			APlayerController* PC;

		UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
			float ShowWidgetTime;
};
