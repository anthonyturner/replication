// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "FloatingWidget.h"


void UFloatingWidget::NativeConstruct() 
{
	
	SetAlignmentInViewport(FVector2D(.5, .5));
	UWorld* World = GetWorld();
	check(World);
	World->GetTimerManager().SetTimer(FloatingWidgetTimerHandle, this, &UFloatingWidget::HandleShowingWidget, ShowWidgetTime);
	PC = UGameplayStatics::GetPlayerController(World, 0);

}


void UFloatingWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	//Update widget's location to actor's location
	FVector ActorLocation = ActorReference->GetActorLocation() + FVector(0.f, 0.f, 60.f);
	FVector2D ScreenLocation;
	
	bool HasProjected = PC->ProjectWorldLocationToScreen(ActorLocation, ScreenLocation);

	ESlateVisibility WidgetVisibility = HasProjected ? ESlateVisibility::Visible : ESlateVisibility::Hidden;
	SetVisibility(WidgetVisibility);
	SetPositionInViewport(ScreenLocation);
}

void UFloatingWidget::HandleShowingWidget() {

	SetVisibility(ESlateVisibility::Hidden);

}

void UFloatingWidget::SetActorReference(AActor* ActorRef) {

	ActorReference = ActorRef;
}