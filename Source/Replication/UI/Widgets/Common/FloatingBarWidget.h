// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FloatingBarWidget.generated.h"


UCLASS()
class REPLICATION_API UFloatingBarWidget : public UUserWidget
{
	GENERATED_BODY()
	
	
public:

	void UpdateFloatingBar(FText Name, FText Level, bool IsAgressive);

	UFUNCTION(BlueprintImplementableEvent, Category = "FloatingBarWidget")
		void OnUpdateHealthBar(float percent);

	UFUNCTION(BlueprintImplementableEvent, Category = "FloatingBarWidget")
		void OnUpdateLevelText(const FText& LevelText, bool IsRed);

	UFUNCTION(BlueprintImplementableEvent, Category = "FloatingBarWidget")
		void OnUpdateNameText(const FText& NameText, bool IsRed);
};
