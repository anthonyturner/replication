// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameHud.generated.h"

UCLASS()
class REPLICATION_API UGameHud : public UUserWidget
{
	GENERATED_BODY()
	
	

private:

	UPROPERTY(BlueprintReadWrite, Category = "GameHud", meta = (AllowPrivateAccess = "true"))
	UStatBarWidget* HealthBar;

	
	
};
