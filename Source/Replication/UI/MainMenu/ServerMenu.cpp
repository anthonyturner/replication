// Fill out your copyright notice in the Description page of Project Settings.
#include "Replication.h"
#include "ServerMenu.h"

void UServerMenu::NativeConstruct()
{
	Super::NativeConstruct();
	playerModeHeader = FText::FromString("Choose play method");
	playerModeText = FText::FromString("INTERNET");
	isLanEnabled = false;
	timerValue = 10;
	isPresence = true;

	// Bind delegates here.
}


/** C++ Session timer */
void UServerMenu::SessionTimer() {


	if (sessionFound) {
		int countDownTimer = 0;


		do {
			countDownTimer++;

		} while (countDownTimer <= 10);
		ResetTimer();
		HandleCountdown_Implementation();
	} else {

	}
}

void UServerMenu::ResetTimer() {

	
}

void UServerMenu::HandleCountdown_Implementation() {


	timerValue--;
	if (timerValue <= 0) {

		sessionFound = false;
		timerValue = 10;
		buttonVisibility = ESlateVisibility::Visible;
		playerModeHeader = FText::FromString("Choose play method.");
		HandleCountdown();
		
	}
}


void UServerMenu::ToggleLan() {

	if (!isLanEnabled) {
		isLanEnabled = true;
		playerModeText = FText::FromString("LAN");
	}

}

void UServerMenu::ToggleInternet() {

	if (isLanEnabled) {
		isLanEnabled = false;
		playerModeText = FText::FromString("Internet");
	}

}