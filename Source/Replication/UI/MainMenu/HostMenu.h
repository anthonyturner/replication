// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "HostMenu.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API UHostMenu : public UUserWidget
{
	GENERATED_BODY()
	
	
public:

	virtual void NativeConstruct() override;


	UFUNCTION(BlueprintCallable)
		void HandleOnBackButtonClicked();

	UFUNCTION(BlueprintCallable)
		void HandleAcceptBackButtonClicked();


//Member variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSettings)
		bool acceptIsEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSettings)
		FName serverName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSettings)
		bool isLanEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSettings)
		int numPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSettings)
		int maxPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSettings)
		FText playyModeText;

	UFUNCTION(BlueprintCallable)
	void DecreaseNumPlayers();

	UFUNCTION(BlueprintCallable)
	void IncreaseNumPlayers();

	UFUNCTION(BlueprintCallable)
		void ToggleLan();

	UFUNCTION(BlueprintCallable)
		void ToggleInternet();


private:

	UPROPERTY()
		UReplicationGameInstance* repGameInstance;
	
};
