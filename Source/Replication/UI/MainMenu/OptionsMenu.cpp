// Fill out your copyright notice in the Description page of Project Settings.
#include "Replication.h"
#include "OptionsMenu.h"




void UOptionsMenu::SaveGameCheck_Implementation() {


	foundSaveGame = UGameplayStatics::DoesSaveGameExist(playerSettingsSave, 0);
	if (foundSaveGame) {

		LoadGame();
	} else {

		SaveGame();
	}
}

void UOptionsMenu::LoadGame() {

	playerSaveGame = UGameplayStatics::LoadGameFromSlot(playerSettingsSave, 0);
	//Call Blueprints Load game because the playerInfo we need is in the Blueprint SaveGameWidget and not in C++
	UILoadGame();

}


void UOptionsMenu::SaveGame() {

	if (playerSaveGame) {
		//Call Blueprints Save game because playerInfo is in Blueprint SaveGameWidget and not in C++
		UISaveGame();
		//playerSaveGame->Set
	} else {

		if (SaveGameClass) {
			playerSaveGame = UGameplayStatics::CreateSaveGameObject(SaveGameClass);
			UISaveGame();

		}
	}

}

void UOptionsMenu::EmptyNameCheck(FText name) {


	enteredPlayerName = name;

	if (name.IsEmptyOrWhitespace()) {

		acceptEnabled = false;
	}
	else {

		acceptEnabled = true;
	}

}


void UOptionsMenu::SetupAvatarDisplay() {


}
