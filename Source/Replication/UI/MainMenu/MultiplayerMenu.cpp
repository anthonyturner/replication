// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "MultiplayerMenu.h"



void UMultiplayerMenu::NativeConstruct()
{
	Super::NativeConstruct();

	UWorld* world = GetWorld();
	if (world) {
		repGameInstance = Cast<UReplicationGameInstance>(GetWorld()->GetGameInstance());

	}else
		GEngine->AddOnScreenDebugMessage(-1, 8.f, FColor::Red, "MultiplayerMenu: World does not exists");



}

