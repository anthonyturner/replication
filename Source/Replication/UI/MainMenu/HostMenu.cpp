// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "HostMenu.h"




void UHostMenu::NativeConstruct()
{
	Super::NativeConstruct();

	repGameInstance = Cast<UReplicationGameInstance>(GetWorld()->GetGameInstance());
	
	isLanEnabled = true;
	acceptIsEnabled = false;
	serverName = FName("");
	numPlayers = 2;
	maxPlayers = 8;
	playyModeText = FText::FromString("LAN");

}

void UHostMenu::HandleOnBackButtonClicked() {

	RemoveFromParent();
	repGameInstance->ShowMainMenu();
}


void UHostMenu::HandleAcceptBackButtonClicked() {

	RemoveFromParent();
	repGameInstance->LaunchLobby(numPlayers, isLanEnabled, serverName);
}


void UHostMenu::IncreaseNumPlayers() {

	numPlayers++;
	numPlayers = FMath::Clamp(numPlayers, 2, maxPlayers);
	
}

void UHostMenu::DecreaseNumPlayers() {

	numPlayers--;
	numPlayers = FMath::Clamp(numPlayers, 2, maxPlayers);
}

void UHostMenu::ToggleLan() {

	if (!isLanEnabled) {
		isLanEnabled = true;
		playyModeText = FText::FromString("LAN");
	}

}

void UHostMenu::ToggleInternet() {

	if (isLanEnabled) {
		isLanEnabled = false;
		playyModeText = FText::FromString("Internet");
	}

}