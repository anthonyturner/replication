// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MultiplayerMenu.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API UMultiplayerMenu : public UUserWidget
{
	GENERATED_BODY()

public:

		virtual void NativeConstruct() override;

private:

	UPROPERTY()
		UReplicationGameInstance* repGameInstance;
	
};
