// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "ServerMenu.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API UServerMenu : public UUserWidget
{
	GENERATED_BODY()
	
	
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerServerSearchSettings)
		FText playerModeText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerServerSearchSettings)
		FText playerModeHeader;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSearchSettings)
		int timerValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSearchSettings)
		ESlateVisibility buttonVisibility; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSearchSettings)
		bool isLanEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSearchSettings)
		bool sessionFound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ServerSearchSettings)
		bool isPresence;

	//-------------------------------------------------------------------
	//Functions

	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, Category = ServerSearchSettings)
		void SessionTimer();

	UFUNCTION(BlueprintCallable, Category = ServerSearchSettings)
		void ResetTimer();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = ServerSearchSettings)
		void HandleCountdown();

	UFUNCTION(BlueprintCallable, Category = ServerSearchSettings)
		void ToggleLan();

	UFUNCTION(BlueprintCallable, Category = ServerSearchSettings)
		void ToggleInternet();

	
};
