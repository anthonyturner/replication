// Fill out your copyright notice in the Description page of Project Settings.
#include "Replication.h"
#include "MainMenu.h"

void UMainMenu::NativeConstruct()
{
	Super::NativeConstruct();
	
	UWorld* world = GetWorld();
	if (!world)
		GEngine->AddOnScreenDebugMessage(-1, 8.f, FColor::Red, "World does not exists");


	repGameInstance = Cast<UReplicationGameInstance>(GetWorld()->GetGameInstance());

}



void UMainMenu::OnShowHostClicked(){

	repGameInstance->ShowHostMenu();
	RemoveFromParent();
}


void UMainMenu::HandleOnMultiplayerButtonClicked() {

	RemoveFromParent();
	repGameInstance->ShowMultiplayerMenu();
	
}