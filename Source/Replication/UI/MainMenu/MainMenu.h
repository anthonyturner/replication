// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"


class UReplicationGameInstance;
UCLASS()
class REPLICATION_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()
	
	
	
public:

	UFUNCTION(BlueprintCallable)
	void OnShowHostClicked();
	
	UFUNCTION(BlueprintCallable)
	void HandleOnMultiplayerButtonClicked();


	virtual void NativeConstruct() override;

private:

	UPROPERTY()
		UReplicationGameInstance* repGameInstance;
};
