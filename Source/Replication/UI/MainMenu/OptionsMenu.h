// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "OptionsMenu.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API UOptionsMenu : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = OptionsMenu)
		void SaveGameCheck();

	UFUNCTION(BlueprintCallable, Category = OptionsMenu)
		void EmptyNameCheck(FText name);

	UFUNCTION(BlueprintCallable, Category = OptionsMenu)
		void LoadGame();

	UFUNCTION(BlueprintCallable, Category = OptionsMenu)
		void SaveGame();

	UFUNCTION(BlueprintCallable, Category = OptionsMenu)
	void SetupAvatarDisplay();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = OptionsMenu)
		void UISaveGame();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = OptionsMenu)
		void UILoadGame();



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
	FText enteredPlayerName;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
		bool foundSaveGame;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
		bool acceptEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
		TSubclassOf<USaveGame> SaveGameClass;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
		USaveGame* playerSaveGame;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
		UTexture2D* avatarImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
		TArray<UTexture2D*> characterAvatars;
	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerSettings)
		FString playerSettingsSave = "playerSettingsSave";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerSettings)
		ESlateVisibility welcomeMessageVisibility;


	
};
