// Fill out your copyright notice in the Description page of Project Settings.
#include "Replication.h"
#include "CityPathBuilder_Exp.h"

#include "BuildingModTile.h"

bool _DEBUG_LOG = false;

ABuildingModTile::ABuildingModTile(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	BaseRoot = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("BaseSphereComponent"));
	RootComponent = BaseRoot;

	//Create all parts of the smart tile 
	Floor =			ObjectInitializer.CreateDefaultSubobject < UStaticMeshComponent >(this, TEXT("SMeshFloor"));
	Wall =			ObjectInitializer.CreateDefaultSubobject < UStaticMeshComponent >(this, TEXT("SMeshEXTWall"));
	InternalMesh =	ObjectInitializer.CreateDefaultSubobject < UStaticMeshComponent >(this, TEXT("SMeshINTWall"));
	DecoMesh =		ObjectInitializer.CreateDefaultSubobject < UStaticMeshComponent >(this, TEXT("SMeshDecoration"));
	FurnMesh =		ObjectInitializer.CreateDefaultSubobject < UStaticMeshComponent >(this, TEXT("SMeshFurnature"));

	Floor->AttachTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
	Wall->AttachTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
	InternalMesh->AttachTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
	DecoMesh->AttachTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
	FurnMesh->AttachTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);

	//This was a defined struct that was orginally planned for 8 position offsets. May use for later developments. Mainly BM MRM TM and MLM are used for placements normally.
	WallRotPOSData.SetNum(BuildPart_POSTypes::PT_MAX);

	WallRotPOSData[0].WallType = BuildPart_POSTypes::PT_NONE;
	WallRotPOSData[0].XOffset = 0;
	WallRotPOSData[0].YOffset = 0;
	WallRotPOSData[0].ZRotMod = 0;

	WallRotPOSData[1].WallType = BuildPart_POSTypes::PT_BLC;
	WallRotPOSData[1].XOffset = 0;
	WallRotPOSData[1].YOffset = 1;
	WallRotPOSData[1].ZRotMod = 90;

	WallRotPOSData[2].WallType = BuildPart_POSTypes::PT_BM;
	WallRotPOSData[2].XOffset = 0;
	WallRotPOSData[2].YOffset = 0;
	WallRotPOSData[2].ZRotMod = 0;

	WallRotPOSData[3].WallType = BuildPart_POSTypes::PT_BRC;
	WallRotPOSData[3].XOffset = 0;
	WallRotPOSData[3].YOffset = 0;
	WallRotPOSData[3].ZRotMod = 0;

	WallRotPOSData[4].WallType = BuildPart_POSTypes::PT_MRM;
	WallRotPOSData[4].XOffset = 1;
	WallRotPOSData[4].YOffset = 0;
	WallRotPOSData[4].ZRotMod = -90;

	WallRotPOSData[5].WallType = BuildPart_POSTypes::PT_TRC;
	WallRotPOSData[5].XOffset = 1;
	WallRotPOSData[5].YOffset = 0;
	WallRotPOSData[5].ZRotMod = -90;

	WallRotPOSData[6].WallType = BuildPart_POSTypes::PT_TM;
	WallRotPOSData[6].XOffset = 1;
	WallRotPOSData[6].YOffset = 1;
	WallRotPOSData[6].ZRotMod = 180;

	WallRotPOSData[7].WallType = BuildPart_POSTypes::PT_TLC;
	WallRotPOSData[7].XOffset = 1;
	WallRotPOSData[7].YOffset = 1;
	WallRotPOSData[7].ZRotMod = 180;

	WallRotPOSData[8].WallType = BuildPart_POSTypes::PT_MLM;
	WallRotPOSData[8].XOffset = 0;
	WallRotPOSData[8].YOffset = 1;
	WallRotPOSData[8].ZRotMod = 90;

}


/*
The C++ equelevant of Construct script
*/
void ABuildingModTile::OnConstruction(const FTransform & Transform){
	Super::OnConstruction(Transform);
	UpdateUnitSettings();
}

/*
	Called from the parent object to send the data into the object
*/
void ABuildingModTile::setMasterData(TArray<UStaticMesh*> NewList, FVector UnitSize){

	//Modular part size IE 400x-400x400 cm
	//Y must be negitive since i build from the lower left to the top right moving backwards
	TileSize = UnitSize;

	//Copy the mesh data from the parent
	PartType_Meshes.Empty();
	for (int i = 0; i < NewList.Num(); i++){
		PartType_Meshes.Add(NewList[i]);
	}

	//Update Displayed tiled data
	UpdateUnitSettings();
}

/*
	Note:	This function can only be called from a root construction function
*/
void  ABuildingModTile::UpdateUnitSettings(){
	if (TileSize == FVector(0, 0, 0)){
		TileSize = FVector(400, -400, 400);
	}

	BuildFloorAndWall();
	UpdateWallPOS();
	UpdateInternalPOS();
	UpdateDecoPOS();
	UpdateFurnPOS();
}


void ABuildingModTile::BuildFloorAndWall(){
		//Here we initalize the build of meshes to a default value. 
	if (PartType_Meshes.Num() > 0){

			/************************************************************************************************************************************************************/
			//Load Floor Mesh
			if (FloorIndex == BuildPart_Types::NONE){
			}
			else{
				if (PartType_Meshes.Num() > FloorIndex - 1)
				{
					if (PartType_Meshes[FloorIndex - 1] != NULL){
						Floor->SetStaticMesh(PartType_Meshes[FloorIndex - 1]);
						FTransform temp = FTransform();
						temp.SetLocation(FVector(0, 0, 0));
						Floor->SetRelativeTransform(temp);
					}
					else{
						if(_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 01: Mesh is NULL"));
					}
				}
				else{
					if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 02: Index Exceeds the avaliavle meshes Please add mesh place holders for these mesh types"));
				}
			}

			/************************************************************************************************************************************************************/
			//Load EXT Wall Mesh
			if (WallType == BuildPart_Types::NONE || WallType == BuildPart_Types::BLANK){
			}else{
				if (PartType_Meshes.Num() > WallType - 1)
				{
					if (PartType_Meshes[WallType - 1] != NULL){
						Wall->SetStaticMesh(PartType_Meshes[WallType - 1]);
						FTransform temp = FTransform();
						temp.SetLocation(FVector(0, 0, 0));
						Wall->SetRelativeTransform(temp);
					}
					else{
						if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 03: Mesh is NULL"));
					}
				}else{
					if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 04: Index Exceeds the avaliavle meshes Please add mesh place holders for these mesh types"));
				}
			}

			/************************************************************************************************************************************************************/
			//Load INT Wall Mesh
			if (InternalType == BuildPart_Types::NONE){
				
			}
			else{
				if (PartType_Meshes.Num() > InternalType - 1)
				{
					if (PartType_Meshes[InternalType - 1] != NULL){
						InternalMesh->SetStaticMesh(PartType_Meshes[InternalType - 1]);
						FTransform temp = FTransform();
						temp.SetLocation(FVector(0, 0, 0));
						InternalMesh->SetRelativeTransform(temp);
					}
					else{
						if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 05: Mesh is NULL"));
					}
				}
				else{
					if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 06: Index Exceeds the avaliavle meshes Please add mesh place holders for these mesh types"));
				}
			}

			/************************************************************************************************************************************************************/
			//Load Decoration Mesh
			if (DecoType == BuildPart_Types::NONE){
			}
			else{
				if (PartType_Meshes.Num() > DecoType - 1)
				{
					if (PartType_Meshes[DecoType - 1] != NULL){
						DecoMesh->SetStaticMesh(PartType_Meshes[DecoType - 1]);
						FTransform temp = FTransform();
						temp.SetLocation(FVector(0, 0, 0));
						DecoMesh->SetRelativeTransform(temp);
					}
					else{
						if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 07: Mesh is NULL"));
					}
				}
				else{
					if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 08: Index Exceeds the avaliavle meshes Please add mesh place holders for these mesh types"));
				}
			}

			/************************************************************************************************************************************************************/
			//Load Furnature Mesh
			if (FurnType == BuildPart_Types::NONE){
			}
			else{
				if (PartType_Meshes.Num() > FurnType - 1)
				{
					if (PartType_Meshes[FurnType - 1] != NULL){
						FurnMesh->SetStaticMesh(PartType_Meshes[FurnType - 1]);
						FTransform temp = FTransform();
						temp.SetLocation(FVector(0, 0, 0));
						FurnMesh->SetRelativeTransform(temp);
					}
					else{
						if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 09: Mesh is NULL"));
					}
				}
				else{
					if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 10: Index Exceeds the avaliavle meshes Please add mesh place holders for these mesh types"));
				}
			}
		}else{
			if (_DEBUG_LOG)UE_LOG(LogTemp, Log, TEXT("Error 11: Index Exceeds the avaliavle meshes Please add mesh place holders for these mesh types"));
		}
}

/*
	Updated the location of the mesh inside of the smart object
*/
void ABuildingModTile::UpdateWallPOS(){
	if (WallPOS == BuildPart_POSTypes::PT_NONE)return;
	for (int i = 0; i < WallRotPOSData.Num(); i++){
		if (WallRotPOSData[i].WallType == WallPOS){
			FTransform temp = FTransform();
			temp.SetLocation(FVector(WallRotPOSData[i].XOffset * TileSize.X, WallRotPOSData[i].YOffset * TileSize.Y, 0));
			temp.SetRotation(FQuat(FRotator(0, WallRotPOSData[i].ZRotMod, 0)));
			Wall->SetRelativeTransform(temp);
			Floor->SetRelativeTransform(temp);
		}
	}
}

/*
Updated the location of the mesh inside of the smart object
*/
void ABuildingModTile::UpdateInternalPOS(){
	if (InternalPOS == BuildPart_POSTypes::PT_NONE)return;
	for (int i = 0; i < WallRotPOSData.Num(); i++){
		if (WallRotPOSData[i].WallType == InternalPOS){
			FTransform temp = FTransform();
			temp.SetLocation(FVector(WallRotPOSData[i].XOffset * TileSize.X, WallRotPOSData[i].YOffset * TileSize.Y, 0));
			temp.SetRotation(FQuat(FRotator(0, WallRotPOSData[i].ZRotMod, 0)));
			InternalMesh->SetRelativeTransform(temp);
			Floor->SetRelativeTransform(temp);
		}
	}
}

/*
Updated the location of the mesh inside of the smart object
*/
void ABuildingModTile::UpdateDecoPOS(){
	if (DecoPOS == BuildPart_POSTypes::PT_NONE)return;
	for (int i = 0; i < WallRotPOSData.Num(); i++){
		if (WallRotPOSData[i].WallType == DecoPOS){
			FTransform temp = FTransform();
			temp.SetLocation(FVector(WallRotPOSData[i].XOffset * TileSize.X, WallRotPOSData[i].YOffset * TileSize.Y, 0));
			temp.SetRotation(FQuat(FRotator(0, WallRotPOSData[i].ZRotMod, 0)));
			DecoMesh->SetRelativeTransform(temp);
		}
	}
}

/*
Updated the location of the mesh inside of the smart object
*/
void ABuildingModTile::UpdateFurnPOS(){
	if (FurnPOS == BuildPart_POSTypes::PT_NONE)return;
	for (int i = 0; i < WallRotPOSData.Num(); i++){
		if (WallRotPOSData[i].WallType == FurnPOS){
			FTransform temp = FTransform();
			temp.SetLocation(FVector(WallRotPOSData[i].XOffset * TileSize.X, WallRotPOSData[i].YOffset * TileSize.Y, 0));
			temp.SetRotation(FQuat(FRotator(0, WallRotPOSData[i].ZRotMod, 0)));
			//FurnMesh->ClearInstances();
			//FurnMesh->AddInstance(temp);
			FurnMesh->SetRelativeTransform(temp);
		}
	}
}

/*
Scans the Bake list for the static mesh, if found it adds it's location data to the mesh location array, if not it creates a new entry into the mesh list object
*/
void ABuildingModTile::ScanList(UStaticMeshComponent * object, TArray<FInstancedStructures> & BakedParts){
	if (object->StaticMesh == nullptr)return;

	bool isFound = false;
	for (int32 i = 0; i < BakedParts.Num(); i++){
		if (isSmartObjectType(BakedParts[i].SmartObjectType))continue;
		if (BakedParts[i].sMesh->GetName() == object->StaticMesh->GetName()){
			BakedParts[i].MeshLocations.Add(object->ComponentToWorld);
			isFound = true;
			break;
		}
	}

	if (!isFound){
		BakedParts.Add(FInstancedStructures());
		int32 indx = BakedParts.Num() - 1;
		BakedParts[indx].MeshObject = nullptr;
		BakedParts[indx].sMesh = object->StaticMesh;
		BakedParts[indx].MeshLocations.Add(object->ComponentToWorld);
	}
}
/*
Scans the Bake list for the static mesh, if found it adds it's location data to the mesh location array, if not it creates a new entry into the mesh list object
*/
void ABuildingModTile::ScanListSmartProp(UStaticMeshComponent * object, TArray<FInstancedStructures> & BakedParts, TEnumAsByte<BuildPart_Types::BuildPart_Type> SmartObjectType){
	if (object->StaticMesh == nullptr)return;

	bool isFound = false;
	for (int32 i = 0; i < BakedParts.Num(); i++){
		if (BakedParts[i].SmartObjectType == SmartObjectType){
			BakedParts[i].MeshLocations.Add(object->ComponentToWorld);
			isFound = true;
			break;
		}
	}

	if (!isFound){
		BakedParts.Add(FInstancedStructures());
		int32 indx = BakedParts.Num() - 1;
		BakedParts[indx].MeshObject = nullptr;
		BakedParts[indx].sMesh = object->StaticMesh;
		BakedParts[indx].SmartObjectType = SmartObjectType;
		BakedParts[indx].MeshLocations.Add(object->ComponentToWorld);
	}
}

/*
	Called from the parent. Sends in the static mesh from each object to scan and add its mesh location to the bake list.
*/
void ABuildingModTile::ExpandMeshList(TArray<FInstancedStructures> & BakedParts){
	
	ScanList(Floor, BakedParts);
	ScanList(DecoMesh, BakedParts);
	ScanList(FurnMesh, BakedParts);

	if (isSmartObjectType(WallType)){
		ScanListSmartProp(Wall, BakedParts, WallType);
	}
	else{

		ScanList(Wall, BakedParts);
	}
	if (isSmartObjectType(InternalType)){
		ScanListSmartProp(InternalMesh, BakedParts, InternalType);
	}
	else{
		ScanList(InternalMesh, BakedParts);
	}
}
