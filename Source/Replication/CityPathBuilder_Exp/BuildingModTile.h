// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BuildingMod_Defines.h"
#include "BuildingModTile.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class REPLICATION_API ABuildingModTile : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
		USphereComponent* BaseRoot;

	//Same as the constrution script
	void OnConstruction (const FTransform & Transform) override;

	//Marks the Size of the modular set
	//X Width Y Depth Z Height
	FVector TileSize;


public:
	/*******************************************************************************************/
	ABuildingModTile(const FObjectInitializer& ObjectInitializer);

	//Takes class settings from parent controll class
	void setMasterData(TArray<UStaticMesh*> NewList, FVector UnitSize);
	
	void UpdateUnitSettings();
	void BuildFloorAndWall();
	void UpdateWallPOS();
	void UpdateInternalPOS();
	void UpdateDecoPOS();
	void UpdateFurnPOS();

	//Baking functions used by parent class
	void ExpandMeshList(TArray<FInstancedStructures> & BakedParts);
	void ScanList(UStaticMeshComponent * object, TArray<FInstancedStructures> & BakedParts);
	void ScanListSmartProp(UStaticMeshComponent * object, TArray<FInstancedStructures> & BakedParts, TEnumAsByte<BuildPart_Types::BuildPart_Type> SmartObjectType);

	/*******************************************************************************************/

	//Inital Create Panel
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
		TArray<UStaticMesh*> PartType_Meshes;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Setup")
		FVector2D iD;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Setup")
		TArray<FWallPOSType> WallRotPOSData;


	/*******************************************************************************************/
	//Detail Modifier
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_Types::BuildPart_Type> FloorIndex;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_Types::BuildPart_Type> WallType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_POSTypes::BuildPart_POSType> WallPOS;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_Types::BuildPart_Type> InternalType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_POSTypes::BuildPart_POSType> InternalPOS;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_Types::BuildPart_Type> DecoType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_POSTypes::BuildPart_POSType> DecoPOS;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_Types::BuildPart_Type> FurnType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PartLocation")
		TEnumAsByte<BuildPart_POSTypes::BuildPart_POSType> FurnPOS;

	/*******************************************************************************************/

	//Instance Holders
	UPROPERTY()
		UStaticMeshComponent * Floor;
	UPROPERTY()
		UStaticMeshComponent * Wall;
	UPROPERTY()
		UStaticMeshComponent * InternalMesh;
	UPROPERTY()
		UStaticMeshComponent * DecoMesh;
	UPROPERTY()
		UStaticMeshComponent * FurnMesh;

};
