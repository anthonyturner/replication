// Fill out your copyright notice in the Description page of Project Settings.

//
#include "Replication.h"
#include "CityPathBuilder_Exp.h"

#include "CityPathBuilder.h"

#include "Runtime/Landscape/Classes/Landscape.h"
/*
Copies N number of bytes from the file binary data, into data
*/
void ACityPathBuilder::setData(void* data, TArray<uint8>& TheBinaryArray, int * offset, int bytestoread){
	int read = 0;
	int myVal = 0;
	while (read != bytestoread){
		myVal |= TheBinaryArray[(int)(*offset + read)] << (read * 8);
		read++;
	}
	memcpy(data, &myVal, bytestoread);
	*offset += bytestoread;
}

/*
Looks for BLD files form the desired file path, optionally you can filter the files grabbed with a prefix.
*/
int ACityPathBuilder::SearchDirectory(FString filepath, FString Prefix = "")
{
	std::string     strFilePath;             // Filepath
	std::string     strPattern;              // Pattern
	std::string     strExtension;            // Extension
	HANDLE          hFile;                   // Handle to file
	WIN32_FIND_DATAA FileInformation;         // File information

	//Build Search String
	const std::string        &refcstrRootDirectory = TCHAR_TO_ANSI(*filepath);
	int indx = 0;
	strPattern = refcstrRootDirectory + "/" + TCHAR_TO_ANSI(*Prefix) + "*.BLD";

	//Clear Building Files
	BuildingFiles.Empty();
	BuildingDimensions.Empty();
	//Scan for files
	hFile = ::FindFirstFileA(strPattern.c_str(), &FileInformation);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (FileInformation.cFileName[indx] != '.')
			{
				//Build File path
				FString tmpFileLoc = refcstrRootDirectory.c_str();
				tmpFileLoc += "/";
				tmpFileLoc += FileInformation.cFileName;

				//Load Binary Data
				TArray<uint8> TheBinaryArray;
				if (!FFileHelper::LoadFileToArray(TheBinaryArray, *tmpFileLoc))
				{
					UE_LOG(LogTemp, Log, TEXT("FFILEHELPER:>> Invalid File"));
					return 0xFE;
				}

				int32 FilePOS = 0;
				int32 pFileVersion = 0;
				int32 Levels = 0;
				int32 GridYSize = 0;
				int32 GridXSize = 0;

				//Load File Version
				setData(&pFileVersion, TheBinaryArray, &FilePOS, 2);

				//Only load versions of the BLD files that match the systems version
				if (pFileVersion == FileVersion){
					BuildingFiles.Add(tmpFileLoc);

					//Only load the building details but not the total data
					//This is used for proxy info and other data that could be used to pick buildings to generate
					setData(&Levels, TheBinaryArray, &FilePOS, 2);
					setData(&GridXSize, TheBinaryArray, &FilePOS, 2);
					setData(&GridYSize, TheBinaryArray, &FilePOS, 2);
					BuildingDimensions.Add(FVector(GridXSize, GridYSize, Levels));
				}
			}
			indx++;
		} while (FindNextFileA(hFile, &FileInformation) == 1);
		::FindClose(hFile);
	}
	return 0;
}


ACityPathBuilder::ACityPathBuilder(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)

{
	BaseRoot = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("BaseSphereComponent"));
	RootComponent = BaseRoot;
	
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Root path used for the main road street outline
	PathRoot = ObjectInitializer.CreateDefaultSubobject<USplineComponent>(this, TEXT("RootSpline"));
	PathRoot->AttachTo(RootComponent);

	_LSpline = ObjectInitializer.CreateDefaultSubobject<USplineComponent>(this, TEXT("LSpline"));
	_LSpline->AttachTo(RootComponent);
	_RSpline = ObjectInitializer.CreateDefaultSubobject<USplineComponent>(this, TEXT("RSpline"));
	_RSpline->AttachTo(RootComponent);

	//Make the Spline points easier to select on spawn.
	PathRoot->ClearSplinePoints();
	PathRoot->AddSplineLocalPoint(FVector(0, 0, 0));
	PathRoot->AddSplineLocalPoint(FVector(1000, 0, 0));

	//Magic numbers derived from testing
	BuildingSpacing = 2000;
	StreetWidth = 4000;
	ModularSize = FVector(400, 400, 400);
	RoadScale=FVector2D(20,20);
	RoadUpDir = FVector(0, 0, 1);
	NumberOfRoads = 5;
	RoadTerrWidth=1000;
	RoadTerrFalloff=1000;
	BuildingTerrWidth=3000;
	BuildingTerrFalloff=1000;
	L_BlockActive = true;
	R_BlockActive = true;
	ShowProxy = true;

	//Using a cube for the proxy... This mesh must be 1 x 1 x 1
	proxyMesh = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), NULL, TEXT("StaticMesh'/Game/CityPathBuilder/ProxyMesh.ProxyMesh'")));

	//Create Base Folder Path for fine search. "Content\Data"
	BuildingDataPath = FPaths::GameContentDir() + "Data";

	//Forced File Version
	FileVersion = 0x0102;
}


//If this object is spawned from a parrent class you might want to move the body lines into a function call thats made public for this can be updated from the constructon script of the parrent
void ACityPathBuilder::OnConstruction(const FTransform & Transform){
	Super::OnConstruction(Transform);

	//Load building
	if (LoadBuildingSettings){
		SearchDirectory(BuildingDataPath, PrefabFilterPrefix);
		LoadBuildingSettings = false;
	}

	switch (CurrentBuildState){
		case CityStructStates::CS_Proxy:	
			
			//State responsible for positioning the prozy data and aligning the road mesh
			//This state also handles the flattening of the terrain undet the building proxys
			ClearBakedData();
			cleanBuildingData(true);
			cleanBuildingData(false);
			SetupProxy();
			CreateRoad();
			//if (UpdateTerrain){
				//UpdateTerrain = false;
				//UpdateBuildingTerrain();
			//}

			break;
		case CityStructStates::CS_Generate:
			//Generates the children using the proxy placement data
				cleanBuildingData(true);
				cleanBuildingData(false);
				ClearBakedData();

				for (int BBlock = 0; BBlock < LeftBlockCount; BBlock++){
					LeftBlock.Add(GetWorld()->SpawnActor< ABuildingModGen >());
					LeftBlock[BBlock]->AttachRootComponentTo(BaseRoot, NAME_None, EAttachLocation::KeepWorldPosition);
				}
				for (int BBlock = 0; BBlock < RightBlockCount; BBlock++){

					RightBlock.Add(GetWorld()->SpawnActor< ABuildingModGen >());
					RightBlock[BBlock]->AttachRootComponentTo(BaseRoot, NAME_None, EAttachLocation::KeepWorldPosition);
				}
				if (L_BlockActive){
					ProcessCreationBuildings(L_BlockIDS, L_Block_Transform, true);
				}
				if (R_BlockActive){
					ProcessCreationBuildings(R_BlockIDS, R_Block_Transform, false);
				}
				CurrentBuildState = CityStructStates::CS_Tweaking;
				break;
		case CityStructStates::CS_Bake:
				//Starts the baking process
				ClearBakedData();
				BuildInstanceList();
				DisplayInstanceList();
				CurrentBuildState = CityStructStates::CS_Dormate;
				break;
		case CityStructStates::CS_Tweaking:
				CreateRoad();
				/*
				if (UpdateTerrain){
					UpdateTerrain = false;
					UpdateBuildingTerrain();
				}*/
				break;
		case CityStructStates::CS_Dormate:
				CreateRoad();
				/*
				if (UpdateTerrain){
					UpdateTerrain = false;
					UpdateBuildingTerrain();
				}*/
				break;
			default: 
				//Idle / unneeded states
				break;
	}
}
// Called when the game starts or when spawned
void ACityPathBuilder::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACityPathBuilder::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ACityPathBuilder::cleanBuildingData(bool IsLeftSide)
{
	if (IsLeftSide){
		for (int cleanBlocks = 0; cleanBlocks < LeftBlock.Num(); cleanBlocks++){
			LeftBlock[cleanBlocks]->cleanUp();
			LeftBlock[cleanBlocks]->Destroy();
		}
		LeftBlock.Empty();
	}
	else{
		for (int cleanBlocks = 0; cleanBlocks < RightBlock.Num(); cleanBlocks++){
			RightBlock[cleanBlocks]->cleanUp();
			RightBlock[cleanBlocks]->Destroy();
		}
		RightBlock.Empty();
	}
}
//Basses the master settings from the proxy into the child for building generation
void ACityPathBuilder::ProcessCreationBuildings(TArray<int32> BuildingID, TArray<FTransform> BuildingTransform, bool IsLeftSide){
	if (BuildingTheme == nullptr){
		UE_LOG(LogTemp, Log, TEXT("ACityPathBuilder: No Building Theme set in BuildingTheme!"));
		return;
	}
	for (int BBlock = 0; BBlock < BuildingID.Num(); BBlock++){
		if (IsLeftSide){
			if (LeftBlock.Num() < BuildingID.Num())return;
			LeftBlock[BBlock]->MainDoor = BuildingTheme.GetDefaultObject()->MainDoor;
			LeftBlock[BBlock]->InsideDoor = BuildingTheme.GetDefaultObject()->InsideDoor;
			LeftBlock[BBlock]->SlidingDoor = BuildingTheme.GetDefaultObject()->SlidingDoor;
			LeftBlock[BBlock]->PrefabFilterPrefix = PrefabFilterPrefix;
			LeftBlock[BBlock]->DesiredPrefab = BuildingID[BBlock]+1;
			LeftBlock[BBlock]->LoadPrefabData = true;
			LeftBlock[BBlock]->CreatePrefab = true;
			LeftBlock[BBlock]->LevelMeshList = BuildingTheme.GetDefaultObject()->LevelMeshList;
			LeftBlock[BBlock]->UnitSizeLDH = BuildingTheme.GetDefaultObject()->UnitSizeLDH;
			LeftBlock[BBlock]->updateConstruction();
			//Adjust the rotation of the building AFTER the construction. Doing the alighment to the spline before causes the child tiles to be placed incorrectly because the child placements are based on 0'd out rotaions 
			LeftBlock[BBlock]->SetActorLocationAndRotation(BuildingTransform[BBlock].GetLocation(), FRotator(BuildingTransform[BBlock].GetRotation()));
			//LeftBlock[BBlock]->SetActorLocation(BuildingTransform[BBlock].GetLocation());
		}
		else{
			if (RightBlock.Num() < BuildingID.Num())return;
			RightBlock[BBlock]->MainDoor = BuildingTheme.GetDefaultObject()->MainDoor;
			RightBlock[BBlock]->InsideDoor = BuildingTheme.GetDefaultObject()->InsideDoor;
			RightBlock[BBlock]->SlidingDoor = BuildingTheme.GetDefaultObject()->SlidingDoor;
			RightBlock[BBlock]->PrefabFilterPrefix = PrefabFilterPrefix;
			RightBlock[BBlock]->DesiredPrefab = BuildingID[BBlock]+1;
			RightBlock[BBlock]->LoadPrefabData = true;
			RightBlock[BBlock]->CreatePrefab = true;
			RightBlock[BBlock]->LevelMeshList = BuildingTheme.GetDefaultObject()->LevelMeshList;
			RightBlock[BBlock]->UnitSizeLDH = BuildingTheme.GetDefaultObject()->UnitSizeLDH;
			RightBlock[BBlock]->updateConstruction();
			//Adjust the rotation of the building AFTER the construction. Doing the alighment to the spline before causes the child tiles to be placed incorrectly because the child placements are based on 0'd out rotaions 
			RightBlock[BBlock]->SetActorLocationAndRotation(BuildingTransform[BBlock].GetLocation(), FRotator(BuildingTransform[BBlock].GetRotation()));
			//RightBlock[BBlock]->SetActorLocation(BuildingTransform[BBlock].GetLocation());
		}

	}
}

/*
	Updates the buildings rotation on spline
*/
void ACityPathBuilder::UpdateBlockTransform(TArray<FTransform> BuildingTransform, bool IsLeftSide){
	if (IsLeftSide){
		for (int BBlock = 0; BBlock < LeftBlock.Num(); BBlock++){
			LeftBlock[BBlock]->SetActorLocationAndRotation(BuildingTransform[BBlock].GetLocation(), FRotator(BuildingTransform[BBlock].GetRotation()));
		}
	}
	else{
		for (int BBlock = 0; BBlock < RightBlock.Num(); BBlock++){
			RightBlock[BBlock]->SetActorLocationAndRotation(BuildingTransform[BBlock].GetLocation(), FRotator(BuildingTransform[BBlock].GetRotation()));
			
		}
	}
}

/*
	Gets the number of buildings on a given block
*/
int32 ACityPathBuilder::GetNumberOfBuildings(bool IsLeftSide){
	if (IsLeftSide){
		return LeftBlock.Num();
	}
	else{
		return RightBlock.Num();
	}
}

/*
	Gets the width of a building on a given side at a given index
*/
float ACityPathBuilder::GetBuildingWidth(int32 index, bool IsLeftSide){
	if (IsLeftSide){
		if (index >= 0 && LeftBlock.Num() > index){
			return LeftBlock[index]->LevelDementionUnits.Y*LeftBlock[index]->UnitSizeLDH.Y;
		}
		else {
			UE_LOG(LogTemp, Log, TEXT("ACityPathBuilder: Invalid Index!"));
			return 0;
		}
	}
	else{
		if (index >= 0 && RightBlock.Num() > index){
			return RightBlock[index]->LevelDementionUnits.Y*RightBlock[index]->UnitSizeLDH.Y;
		}
		else {
			UE_LOG(LogTemp, Log, TEXT("ACityPathBuilder: Invalid Index!"));
			return 0;
		}
	}
}

/*
	Sends the baking request to all children
*/
void ACityPathBuilder::BuildInstanceList(){
	for (int BBlock = 0; BBlock < LeftBlock.Num(); BBlock++){
		LeftBlock[BBlock]->BuildInstanceList();
	}
	for (int BBlock = 0; BBlock < RightBlock.Num(); BBlock++){
		RightBlock[BBlock]->BuildInstanceList();
	}
	UE_LOG(LogTemp, Log, TEXT("ACityPathBuilder: Instance Tree Created"));
}

/*
	Starts the baking process in all children
*/
void ACityPathBuilder::DisplayInstanceList(){
	for (int BBlock = 0; BBlock < LeftBlock.Num(); BBlock++){
		LeftBlock[BBlock]->DisplayInstanceList();
	}
	for (int BBlock = 0; BBlock < RightBlock.Num(); BBlock++){
		RightBlock[BBlock]->DisplayInstanceList();
	}
}


/*
	Clears all bake data in all children
*/
void ACityPathBuilder::ClearBakedData(){
	for (int BBlock = 0; BBlock < LeftBlock.Num(); BBlock++){
		LeftBlock[BBlock]->ClearBakedData();
	}
	for (int BBlock = 0; BBlock < RightBlock.Num(); BBlock++){
		RightBlock[BBlock]->ClearBakedData();
	}
}

void ACityPathBuilder::CreateRoad(){
	if (RoadMesh == nullptr)return;
	int32 LengthORoad = (int32)(PathRoot->GetSplineLength() / NumberOfRoads);

	for (int i = 0; i < RoadParts.Num(); i++){
		RoadParts[i]->DestroyComponent();
	}
	RoadParts.Empty();
	for (int32 segs = 0; segs < NumberOfRoads; segs++){
		RoadParts.Add(NewObject<USplineMeshComponent>(this, USplineMeshComponent::StaticClass()));

		if (RoadParts[segs] != nullptr){
			RoadParts[segs]->AttachTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
			RoadParts[segs]->CreationMethod = EComponentCreationMethod::SimpleConstructionScript;
			//All UObjects must be registed before they can be used. 
			RoadParts[segs]->SetStaticMesh(RoadMesh);
			RoadParts[segs]->RegisterComponent();
			FVector startL = PathRoot->GetWorldLocationAtDistanceAlongSpline(segs*LengthORoad);
			FVector startR = PathRoot->GetWorldTangentAtDistanceAlongSpline(segs*LengthORoad);
			FVector endL = PathRoot->GetWorldLocationAtDistanceAlongSpline((segs + 1) * LengthORoad);
			FVector endRotR = PathRoot->GetWorldTangentAtDistanceAlongSpline((segs + 1) * LengthORoad);

			RoadParts[segs]->SetStartAndEnd(startL, startR, endL, endRotR);
			RoadParts[segs]->SetStartScale(RoadScale);
			RoadParts[segs]->SetSplineUpDir(RoadUpDir);
			RoadParts[segs]->SetEndScale(RoadScale);
			RoadParts[segs]->UpdateChildTransforms();
		}
	}
}

void ACityPathBuilder::SetupProxy(){
	if (ShowProxy){
		_LSpline->ClearSplinePoints();
		if (L_BlockActive == true){
			L_BlockIDS.Empty();
			L_Block_Transform.Empty();
			BuildBuildingSingleSide(true, L_BlockIDS, L_Block_Transform);
			LeftBlockCount = L_BlockIDS.Num();
		}
		_LSpline->UpdateSpline();
		_RSpline->ClearSplinePoints();
		if (R_BlockActive == true){
			R_BlockIDS.Empty();
			R_Block_Transform.Empty();
			BuildBuildingSingleSide(false, R_BlockIDS, R_Block_Transform);
			RightBlockCount = R_BlockIDS.Num();
		}
		_RSpline->UpdateSpline();
	}
}

void ACityPathBuilder::BuildBuildingSingleSide(bool isLeftSide, TArray<int32>& IDList, TArray<FTransform>& TransformList){
	float DistanceOnSpline=0;
	float TotalSplineLength = PathRoot->GetSplineLength();
	if (TotalSplineLength == 0){
		return;
	}
	FVector CurrDimentions;
	float BuildingLength = 0;
	while (PickRandomBuilding(ModularSize, IDList, CurrDimentions, BuildingLength, DistanceOnSpline, TotalSplineLength )){
		DistanceOnSpline = DistanceOnSpline + (BuildingLength / 2);
		FRotator tempRot;
		FVector tempLocation;
		float temp360Rot;
		
		GetLookAtRotation(isLeftSide, DistanceOnSpline,temp360Rot, tempRot);
		GetDisplacedLocation(temp360Rot,isLeftSide, DistanceOnSpline,tempLocation);
		AddTerrainSplinePoints(isLeftSide, tempLocation, DistanceOnSpline, BuildingLength);
		
		FTransform temp;
		temp.SetLocation(tempLocation);
		temp.SetRotation(tempRot.Quaternion());
		temp.SetScale3D(CurrDimentions);

		TransformList.Add(temp);
		//Create proxy at location
		UStaticMeshComponent * tmpMesh = NewObject<UStaticMeshComponent>(this);
		if (tmpMesh != nullptr){
			tmpMesh->AttachTo(BaseRoot, NAME_None);
			tmpMesh->RegisterComponent();
			//Auto destroy on next construct call
			tmpMesh->CreationMethod = EComponentCreationMethod::SimpleConstructionScript;
			tmpMesh->SetStaticMesh(proxyMesh);
			tmpMesh->SetWorldLocationAndRotation(temp.GetLocation(), temp.GetRotation());
			tmpMesh->SetRelativeScale3D(temp.GetScale3D());

		}
		//Add on remaining offsets
		DistanceOnSpline = DistanceOnSpline + BuildingSpacing + (BuildingLength/2);
	}
}

bool ACityPathBuilder::PickRandomBuilding(FVector& ModularDimentions, TArray<int32>& IDList,
						FVector& SelectedDimentions, float& BuildingLength, 
						float& DistanceOnSpline, float& TotalSplineLength){

	int32 RandomNum= (int32)RNG.FRandRange(0, BuildingDimensions.Num() - 1);
	if (BuildingDimensions.Num() == 0)return false;
	if (BuildingDimensions[RandomNum].X*ModularDimentions.X + DistanceOnSpline > TotalSplineLength)return false;

	IDList.Add(RandomNum);
	SelectedDimentions = BuildingDimensions[RandomNum] * ModularDimentions;
	BuildingLength = (BuildingDimensions[RandomNum].X * ModularDimentions.X);

	return true;
}

void ACityPathBuilder::GetLookAtRotation(bool isLeftSide, float DistanceOnSpline, float& temp360Rot, FRotator& tempRot){
	temp360Rot = PathRoot->GetWorldRotationAtDistanceAlongSpline(DistanceOnSpline).Yaw + 180;
	tempRot = isLeftSide ? FRotator(0, temp360Rot, 0) : FRotator(0, temp360Rot + 180, 0);
}

void ACityPathBuilder::GetDisplacedLocation(float temp360Rot, bool isLeftSide, float DistanceOnSpline, FVector& tempLocation){
	float PerpAngle = temp360Rot + (isLeftSide ? -90 : 90);

	float XOffset = FMath::Cos(PerpAngle*(PI/180))/**/*StreetWidth;
	float YOffset = FMath::Sin(PerpAngle*(PI/180))/**(180 / PI)*/*StreetWidth;

	tempLocation = FVector(
		(PathRoot->GetWorldLocationAtDistanceAlongSpline(DistanceOnSpline).X + XOffset),
		(PathRoot->GetWorldLocationAtDistanceAlongSpline(DistanceOnSpline).Y + YOffset),
		(PathRoot->GetWorldLocationAtDistanceAlongSpline(DistanceOnSpline).Z)
		);
}

void ACityPathBuilder::AddTerrainSplinePoints(bool isLeftSide, FVector BuildingLocation, float DistanceOnSpline, float BuildingLength){
	if (isLeftSide && _LSpline == nullptr){
		return;
	}
	if (!isLeftSide && _RSpline == nullptr){
		return;
	}
	FRotator tempRotB4;
	FVector tempLocationB4;
	float temp360RotB4;
	FRotator tempRotAFT;
	FVector tempLocationAFT;
	float temp360RotAFT;
	GetLookAtRotation(isLeftSide, DistanceOnSpline - (BuildingLength / 2), temp360RotB4, tempRotB4);
	GetDisplacedLocation(temp360RotB4, isLeftSide, DistanceOnSpline - (BuildingLength / 2), tempLocationB4);
	GetLookAtRotation(isLeftSide, DistanceOnSpline + (BuildingLength / 2), temp360RotAFT, tempRotAFT);
	GetDisplacedLocation(temp360RotAFT, isLeftSide, DistanceOnSpline + (BuildingLength / 2), tempLocationAFT);

	if (isLeftSide){
		_LSpline->AddSplineWorldPoint(FVector(tempLocationB4.X, tempLocationB4.Y, BuildingLocation.Z));
		_LSpline->AddSplineWorldPoint(FVector(tempLocationAFT.X, tempLocationAFT.Y, BuildingLocation.Z));
	}else{
		_RSpline->AddSplineWorldPoint(FVector(tempLocationB4.X, tempLocationB4.Y, BuildingLocation.Z));
		_RSpline->AddSplineWorldPoint(FVector(tempLocationAFT.X, tempLocationAFT.Y, BuildingLocation.Z));
	}

}

//Not supported in C++ as of yet... Using BP Function to replace the portability of this tool from project to project
/*
void ACityPathBuilder::UpdateBuildingTerrain(){
	if (TerrTarget != nullptr){
		UE_LOG(LogTemp, Log, TEXT("ACityPathBuilder:TerrTarget is not set, please set."));
		return;
	}
	if (_RSpline != nullptr){
		TerrTarget->EditorApplySpline(_RSpline, BuildingTerrWidth, BuildingTerrWidth, BuildingTerrFalloff, BuildingTerrFalloff);
	}

	if (_LSpline != nullptr){
		TerrTarget->EditorApplySpline(_LSpline, BuildingTerrWidth, BuildingTerrWidth, BuildingTerrFalloff, BuildingTerrFalloff);
	}

	if (PathRoot != nullptr){
		TerrTarget->EditorApplySpline(PathRoot, BuildingTerrWidth, BuildingTerrWidth, BuildingTerrFalloff, BuildingTerrFalloff);
	}
}
/**/
