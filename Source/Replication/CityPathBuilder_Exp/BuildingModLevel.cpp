// Fill out your copyright notice in the Description page of Project Settings.
#include "Replication.h"
#include "CityPathBuilder_Exp.h"

#include "BuildingModLevel.h"





ABuildingModLevel::ABuildingModLevel(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	BaseRoot = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("BaseSphereComponent"));
	RootComponent = BaseRoot;

	//Init Default data
	LevelDementionUnits.Set(3, 3, 0);
	UnitSizeLDH.Set(400, -400, 400);
	ResetLevel = false;
}

/*
	Clears all child data
*/
void ABuildingModLevel::cleanUp()
{
	for (int cleanrow = 0; cleanrow < Rows.Num(); cleanrow++){
		for (int cleancol = 0; cleancol <Rows[cleanrow]->Column.Num(); cleancol++){
			Rows[cleanrow]->Column[cleancol]->Destroy();
		}
	}
	Rows.Empty();
}
/*
	Called from parent to send in the setup data for the level grid data
	Note: Function must be called from a construction function 
*/
void ABuildingModLevel::setMasterData(TArray<UStaticMesh*> NewList, FVector UnitSize, FVector DementionUnits, FTileFloors Layout){

	//Modular part size IE 400x-400x400 cm
	//Y must be negitive since i build from the lower left to the top right moving backwards
	UnitSizeLDH = UnitSize;

	//Get level grid size
	LevelDementionUnits = DementionUnits;

	//Store the prefab data
	LayoutData = Layout;

	//Copy the mesh data from the parent
	LevelMeshList.Empty();
	for (int i = 0; i < NewList.Num(); i++){
		LevelMeshList.Add(NewList[i]);
	}

	//Force the creation of the level in the construction function
	ResetLevel = true;
	updatesettings();
}
void ABuildingModLevel::setMasterData(TArray<UStaticMesh*> NewList, FVector UnitSize, FVector DementionUnits){
	//Modular part size IE 400x-400x400 cm
	//Y must be negitive since i build from the lower left to the top right moving backwards
	UnitSizeLDH = UnitSize;

	//Get level grid size
	LevelDementionUnits = DementionUnits;

	//Copy the mesh data from the parent
	LevelMeshList.Empty();
	for (int i = 0; i < NewList.Num(); i++){
		LevelMeshList.Add(NewList[i]);
	}

	//Force the creation of the level in the construction function
	ResetLevel = true;
	updatesettings();
}

/*
The C++ equelevant of Construct script
*/
void ABuildingModLevel::OnConstruction(const FTransform & Transform){
	Super::OnConstruction(Transform);
	updatesettings();
}

/*
	Used to filter External wall/roof types that do not need floor auto added to it
*/
bool ignoreFloorType(int type){

	switch (type){
		
	case BuildPart_Types::Floor:
		return true;
	case BuildPart_Types::RF_TOP:
		return true;
	case BuildPart_Types::BLANK:
		return true;
	case BuildPart_Types::EX_BDTrimDecoFloor:
		return true;
	case BuildPart_Types::EX_BDTrimDecoMid:
		return true;
	case BuildPart_Types::EX_BDTrimDecoCorner:
		return true;
	case BuildPart_Types::RF_SIDE:
		return true;
	case BuildPart_Types::RF_ANGL:
		return true;
	case BuildPart_Types::RF_CORN:
		return true;
	case BuildPart_Types::RF_DOUB:
		return true;
	case BuildPart_Types::RF_END:
		return true;
	case BuildPart_Types::RF_INSI:
		return true;
	default: 
		return false;

	}

}

void ABuildingModLevel::updatesettings(){
	FString asd = "";

	if (ResetLevel){
		cleanUp();
		//UE_LOG(LogTemp, Log, TEXT("------------------------------------------------------------------"));
		for (int XLength = 0; XLength < LevelDementionUnits.X; XLength++){
			Rows.Add(new FTiles());
			for (int YLength = 0; YLength < LevelDementionUnits.Y; YLength++){
				
				//Spawn SmartTile
				Rows[XLength]->Column.Add(GetWorld()->SpawnActor< ABuildingModTile >());
				Rows[XLength]->Column[YLength]->AttachRootComponentTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);

				//ID used for visual ordering confirmation can be removed if not desired
				Rows[XLength]->Column[YLength]->iD = FVector2D(XLength, YLength);
				
				int32 _pos = (XLength*LevelDementionUnits.Y) + YLength;

				/********************************************************************************************************************/
				//Load Prefab data if any

				if (LayoutData.EXT_Wall.Num() > 0){
					
					//Load EXT Wall data, and create floor data based on EXT wall types
					Rows[XLength]->Column[YLength]->WallPOS = LayoutData.EXT_Wall[_pos].PosType;
					if (LayoutData.EXT_Wall[_pos].MeshType != 0){
						if (LayoutData.INT_Wall[_pos].MeshType == BuildPart_Types::INT_ANGL_WALL || LayoutData.EXT_Wall[_pos].MeshType == BuildPart_Types::EXT_ANGL_WALL || LayoutData.EXT_Wall[_pos].MeshType == BuildPart_Types::RF_ANGL){
							if (LayoutData.EXT_Wall[_pos].MeshType == BuildPart_Types::RF_ANGL){
								//Ignore Floor
							}
							else{
								Rows[XLength]->Column[YLength]->FloorIndex = BuildPart_Types::FloorSL;
							}
						}
						else{
							if (ignoreFloorType(LayoutData.EXT_Wall[_pos].MeshType)){
								//Ignore Floor
							}else{
								Rows[XLength]->Column[YLength]->FloorIndex = BuildPart_Types::Floor;
							}
						}
					}
					Rows[XLength]->Column[YLength]->WallType = LayoutData.EXT_Wall[_pos].MeshType;

					//Load INT Wall data Tileset ID 0xFE makes a blank and unused tile from in the data
					if (LayoutData.INT_Wall[_pos].TileSetIDs < 0xFE){
						Rows[XLength]->Column[YLength]->InternalPOS = LayoutData.INT_Wall[_pos].PosType;
						Rows[XLength]->Column[YLength]->InternalType = LayoutData.INT_Wall[_pos].MeshType;
					}

					//Load Decoration data Tileset ID 0xFE makes a blank and unused tile from in the data
					if (LayoutData.Deco_Layer[_pos].TileSetIDs < 0xFE){
						Rows[XLength]->Column[YLength]->DecoPOS = LayoutData.Deco_Layer[_pos].PosType;
						Rows[XLength]->Column[YLength]->DecoType = LayoutData.Deco_Layer[_pos].MeshType;
					}

					//Load Furniture data Tileset ID 0xFE makes a blank and unused tile from in the data
					if (LayoutData.Furn_Layer.Num() && LayoutData.Furn_Layer[_pos].TileSetIDs < 0xFE){
						Rows[XLength]->Column[YLength]->FurnPOS = LayoutData.Furn_Layer[_pos].PosType;
						Rows[XLength]->Column[YLength]->FurnType = LayoutData.Furn_Layer[_pos].MeshType;
					}
				}
			}
		}
		ResetLevel = false;
	}

	//If some how you dont have rows get out and never talk to me again!
	if (Rows.Num() == 0)return;

	//Positions the tiles and sets the mesh data and the size information for all tiles
	for (int32 Row = 0; Row < LevelDementionUnits.X; Row++){
		for (int32 Col = 0; Col < LevelDementionUnits.Y; Col++){
			Rows[Row]->Column[Col]->SetActorLocation((GetTransform().GetLocation() + FVector(Col*UnitSizeLDH.X, Row*UnitSizeLDH.Y, 0)));
			Rows[Row]->Column[Col]->setMasterData(LevelMeshList, UnitSizeLDH);
		}
	}
}

/*
	Called from the parent to send the bake list into the tile for baking.
*/
void ABuildingModLevel::ExpandMeshList(TArray<FInstancedStructures> & BakedParts){

	for (int32 Row = 0; Row < LevelDementionUnits.X; Row++){
		for (int32 Col = 0; Col < LevelDementionUnits.Y; Col++){
			Rows[Row]->Column[Col]->ExpandMeshList(BakedParts);
		}
	}
}
