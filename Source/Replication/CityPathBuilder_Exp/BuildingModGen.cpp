// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "CityPathBuilder_Exp.h"
#include "BuildingModGen.h"

#include "Archive.h"
//#include "ArchiveBase.h"
#include "FileManager.h"

/*
	Copies N number of bytes from the file binary data, into data
*/
void ABuildingModGen::setData(void* data, TArray<uint8>& TheBinaryArray, int * offset, int bytestoread){
	int read=0;
	int myVal = 0;
	while (read!=bytestoread){
		myVal |= TheBinaryArray[(int)(*offset+read)] << (read * 8);
		read++;
	}
	memcpy(data, &myVal, bytestoread);
	*offset += bytestoread;
}

/*
	Looks for BLD files form the desired file path, optionally you can filter the files grabbed with a prefix.
*/
int ABuildingModGen::SearchDirectory(FString filepath, FString Prefix="")
{
	std::string     strFilePath;             // Filepath
	std::string     strPattern;              // Pattern
	std::string     strExtension;            // Extension
	HANDLE          hFile;                   // Handle to file
	WIN32_FIND_DATAA FileInformation;         // File information

	//Build Search String
	const std::string        &refcstrRootDirectory = TCHAR_TO_ANSI(*filepath);
	int indx = 0;
	strPattern = refcstrRootDirectory + "/" + TCHAR_TO_ANSI(*Prefix) + "*.BLD";

	//Clear Building Files
	BuildingFiles.Empty();
	BuildingDimensions.Empty();
	//Scan for files
	hFile = ::FindFirstFileA(strPattern.c_str(), &FileInformation);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (FileInformation.cFileName[indx] != '.')
			{
				//Build File path
				FString tmpFileLoc = refcstrRootDirectory.c_str();
				tmpFileLoc += "/";
				tmpFileLoc += FileInformation.cFileName;

				//Load Binary Data
				TArray<uint8> TheBinaryArray;
				if (!FFileHelper::LoadFileToArray(TheBinaryArray, *tmpFileLoc))
				{
					UE_LOG(LogTemp, Log, TEXT("FFILEHELPER:>> Invalid File"));
					return 0xFE;
				}

				int32 FilePOS = 0;
				int32 pFileVersion=0;
				int32 Levels = 0;
				int32 GridYSize = 0;
				int32 GridXSize = 0;

				//Load File Version
				setData(&pFileVersion, TheBinaryArray, &FilePOS, 2);

				//Only load versions of the BLD files that match the systems version
				if (pFileVersion == FileVersion){
					BuildingFiles.Add(tmpFileLoc);

					//Only load the building details but not the total data
					//This is used for proxy info and other data that could be used to pick buildings to generate
					setData(&Levels, TheBinaryArray, &FilePOS, 2);
					setData(&GridXSize, TheBinaryArray, &FilePOS, 2);
					setData(&GridYSize, TheBinaryArray, &FilePOS, 2);
					BuildingDimensions.Add(FVector(GridXSize, GridYSize, Levels));
				}
			}
			indx++;
		} while (FindNextFileA(hFile, &FileInformation) == 1);
		::FindClose(hFile);
	}
	return 0;
}

ABuildingModGen::ABuildingModGen(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	BaseRoot = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("BaseSphereComponent"));
	RootComponent = BaseRoot;
	
	//Initalize Data
	LevelDementionUnits = FVector(3, 3, 0);
	UnitSizeLDH = FVector(400, -400, 400);
	CreateLevel = false;
	UpdateLevels = false;
	DesiredPrefab=0;
	CreatePrefab=false;
	LoadPrefabData = false;
	
	//Create Base Folder Path for fine search. "Content\Data"
	BuildingDataPath = FPaths::GameContentDir()+"Data";
	
	//Forced File Version
	FileVersion = 0x0102;
}

/*
	Load Data for the desired building to load
*/
void ABuildingModGen::LoadLevelTypeData(int loadBuildingID){
	
	if (BuildingFiles.Num() == 0 || BuildingFiles.Num() <= loadBuildingID){
		UE_LOG(LogTemp, Log, TEXT("ABuildingModGen ERROR: No building files found"));
		return;
	}

	//Load Building data 
	TArray<uint8> TheBinaryArray;
	if (!FFileHelper::LoadFileToArray(TheBinaryArray, *BuildingFiles[loadBuildingID]))
	{
		UE_LOG(LogTemp, Log, TEXT("FFILEHELPER:>> Invalid File"));
		return;
	}

	//File Load Error
	if (TheBinaryArray.Num() <= 0) return;

	int FilePOS = 0;
	int32 pFileVersion = 0;
	int32 Levels = 0;
	int32 GridXSize = 0;
	int32 GridYSize = 0;

	setData(&pFileVersion, TheBinaryArray, &FilePOS, 2);
	setData(&Levels, TheBinaryArray, &FilePOS, 2);
	setData(&GridYSize, TheBinaryArray, &FilePOS, 2);
	setData(&GridXSize, TheBinaryArray, &FilePOS, 2);

	//Set the array for the levels
	LevelData.SetNum(Levels);
	
	//Load Mesh Data and Alignment
	//TilesetID is used ti ignore nulled data from the grid data
	
	//Externale Wall Data
	for (int32 lvl = 0; lvl < Levels; lvl++){
		LevelData[lvl].GridX = GridXSize;
		LevelData[lvl].GridY = GridYSize;

		LevelData[lvl].EXT_Wall.SetNum(GridYSize*GridXSize);
		for (int32 tile = 0; tile < GridYSize*GridXSize; tile++){
			LevelData[lvl].EXT_Wall[tile].TileSetIDs = 0;
			LevelData[lvl].EXT_Wall[tile].MeshType = BuildPart_Types::NONE;
			LevelData[lvl].EXT_Wall[tile].PosType = BuildPart_POSTypes::PT_NONE;
				
			setData(&LevelData[lvl].EXT_Wall[tile].TileSetIDs, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].EXT_Wall[tile].MeshType, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].EXT_Wall[tile].PosType, TheBinaryArray, &FilePOS, 1);
		}
	}
	//Internal Wall DAta
	for (int32 lvl = 0; lvl < Levels; lvl++){
		LevelData[lvl].GridX = GridXSize;
		LevelData[lvl].GridY = GridYSize;
		LevelData[lvl].INT_Wall.SetNum(GridYSize*GridXSize);

		for (int32 tile = 0; tile < GridYSize*GridXSize; tile++){
			LevelData[lvl].INT_Wall[tile].TileSetIDs = 0;
			LevelData[lvl].INT_Wall[tile].MeshType = BuildPart_Types::NONE;
			LevelData[lvl].INT_Wall[tile].PosType = BuildPart_POSTypes::PT_NONE;

			setData(&LevelData[lvl].INT_Wall[tile].TileSetIDs, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].INT_Wall[tile].MeshType, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].INT_Wall[tile].PosType, TheBinaryArray, &FilePOS, 1);
		}
	}
	//Decoration Data
	for (int32 lvl = 0; lvl < Levels; lvl++){
		LevelData[lvl].GridX = GridXSize;
		LevelData[lvl].GridY = GridYSize;
		LevelData[lvl].Deco_Layer.SetNum(GridYSize*GridXSize);

		for (int32 tile = 0; tile < GridYSize*GridXSize; tile++){
			LevelData[lvl].Deco_Layer[tile].TileSetIDs = 0;
			LevelData[lvl].Deco_Layer[tile].MeshType = BuildPart_Types::NONE;
			LevelData[lvl].Deco_Layer[tile].PosType = BuildPart_POSTypes::PT_NONE;

			setData(&LevelData[lvl].Deco_Layer[tile].TileSetIDs, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].Deco_Layer[tile].MeshType, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].Deco_Layer[tile].PosType, TheBinaryArray, &FilePOS, 1);
		}
	}
	//Furnature Data
	for (int32 lvl = 0; lvl < Levels; lvl++){
		LevelData[lvl].GridX = GridXSize;
		LevelData[lvl].GridY = GridYSize;
		LevelData[lvl].Furn_Layer.SetNum(GridYSize*GridXSize);

		for (int32 tile = 0; tile < GridYSize*GridXSize; tile++){
			LevelData[lvl].Furn_Layer[tile].TileSetIDs = 0;
			LevelData[lvl].Furn_Layer[tile].MeshType = BuildPart_Types::NONE;
			LevelData[lvl].Furn_Layer[tile].PosType = BuildPart_POSTypes::PT_NONE;

			setData(&LevelData[lvl].Furn_Layer[tile].TileSetIDs, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].Furn_Layer[tile].MeshType, TheBinaryArray, &FilePOS, 1);
			setData(&LevelData[lvl].Furn_Layer[tile].PosType, TheBinaryArray, &FilePOS, 1);
		}
	}
}


/*
	Used to help clean up child data when baking data and removing old unneeded elements
*/
void ABuildingModGen::cleanUp()
{
	for (int cleancol = 0; cleancol < Level.Num(); cleancol++){
		Level[cleancol]->cleanUp();
		Level[cleancol]->Destroy();
	}
	Level.Empty();
}

/*
	The C++ equelevant of Construct script
*/
void ABuildingModGen::OnConstruction(const FTransform & Transform){
	Super::OnConstruction(Transform);
	updateConstruction();
}

/*
	Called from this construct script or a partrent object to update this and child objects
*/
void ABuildingModGen::updateConstruction(){

	//Clears all data from children
	if (ClearData){
		cleanUp();
		ClearData = false;
	}

	//Load file data for the building prefab
	if (LoadPrefabData){
		SearchDirectory(BuildingDataPath, PrefabFilterPrefix);

		PreFabList.Empty();
		PreFabList.Add("NONE");

		for (int i = 0; i < BuildingFiles.Num(); i++){
			int32 start = 0;
			BuildingFiles[i].FindLastChar('/', start);
			FString temp = BuildingFiles[i];
			PreFabList.Add(temp.RightChop(start + 1));
		}

		LoadPrefabData = false;
	}

	//Creates the building using desired building type
	if (CreatePrefab){
		//Besure to clear children if already created
		cleanUp();
		
		if (DesiredPrefab > 0 && PreFabList.Num()>1){
			//lamp highnumbers to last index
			if (DesiredPrefab > (PreFabList.Num() - 1))DesiredPrefab = PreFabList.Num() - 1;
			LoadLevelTypeData((DesiredPrefab - 1));
			NumberOfFloors = LevelData.Num();
			LevelDementionUnits.X = LevelData[0].GridX;
			LevelDementionUnits.Y = LevelData[0].GridY;
			LevelDementionUnits.Z = 0;
			CreateLevel = true;
			UpdateLevels = true;
		}
		CreatePrefab = false;
	}

	//Created the child actor Level structures
	if (CreateLevel){
		//Besure to clear children if already created
		cleanUp();
		for (int YLength = 0; YLength < NumberOfFloors; YLength++){
			Level.Add(GetWorld()->SpawnActor< ABuildingModLevel >());
			Level[YLength]->AttachRootComponentTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
		}
		UpdateLevels = true;
		CreateLevel = false;
	}
	//If there is no level data get out now!
	if (Level.Num() == 0){

		UE_LOG(LogTemp, Log, TEXT("ABuildingModGen ERROR: No level data"));
		return;
	}
	
	if (UpdateLevels)
	{
		for (int lvl = 0; lvl < NumberOfFloors; lvl++){

			//Updates the placement location of the child parts.
			//In the parent controller we need the level to be centered to the building Mod Gen this helps the rotation of the building on the path spline in a later class
			Level[lvl]->SetActorLocation((GetTransform().GetLocation() + FVector(((LevelDementionUnits.Y/2)*UnitSizeLDH.Y), ((LevelDementionUnits.X/2)*UnitSizeLDH.X), lvl*UnitSizeLDH.Z)));

			if (LevelData.Num() > 0){
				//Section for Prefab Building Data
				Level[lvl]->setMasterData(LevelMeshList, UnitSizeLDH, LevelDementionUnits, LevelData[lvl]);
			}else{
				//Section for manual creation
				Level[lvl]->setMasterData(LevelMeshList, UnitSizeLDH, LevelDementionUnits);
			}
		}
	}
	UpdateLevels = false;

	if (BakeBuilding){
		BuildInstanceList();
		DisplayInstanceList();
		BakeBuilding = false;
	}
}
/*
	Used to reach all the way into the deepest child object to gather mesh data for bake
*/
void ABuildingModGen::ExpandMeshList(TArray<FInstancedStructures> & BakedParts){
	for (int YLength = 0; YLength < Level.Num(); YLength++){
		Level[YLength]->ExpandMeshList(BakedParts);
	}
}

/*
	Called to initalize the bake data
*/
void ABuildingModGen::BuildInstanceList(){
	ExpandMeshList(BakedParts);
	UE_LOG(LogTemp, Log, TEXT("ABuildingModGen: Instance Tree Created"));
}








/***********************************************************************************************************
//Content browser reference to object PTR
/***********************************************************************************************************/
template <typename ObjClass>
static FORCEINLINE ObjClass* LoadObjFromPathPTR(const FName& Path)
{
	if (Path == NAME_None) return NULL;
	return Cast<ObjClass>(StaticLoadObject(ObjClass::StaticClass(), NULL, *Path.ToString()));
}
// Load Static Mesh From Path 
static FORCEINLINE UBlueprint* LoadBPFromPath(const FName& Path)
{
	if (Path == NAME_None) return NULL;
	return LoadObjFromPathPTR<UBlueprint>(Path);
}

UBlueprint *  ConvertPathToUBlueprint(FString Path)
{
	UBlueprint* Object = LoadBPFromPath(*Path);//load the object as per tutorial below or use static load object directly, or use ObjectFinder.
	//AnimObject->GetAnimBlueprintGeneratedClass();
	return Object;
}



/*
	Deletes all child data and places the instance mesh into the correct positions
	Note:	This function can only be called from a root construction function 
			otherwise the creation and registration of the component will not happen
*/
void ABuildingModGen::DisplayInstanceList(){
	if (BakedParts.Num() == 0)return;
	cleanUp();
	for (int32 i = 0; i < BakedParts.Num(); i++){

		switch (BakedParts[i].SmartObjectType){
		case BuildPart_Types::EXT_DOOR:

			for (int32 j = 0; j < BakedParts[i].MeshLocations.Num(); j++){
				
				AActor * tempA = GetWorld()->SpawnActor<AActor>(MainDoor->GeneratedClass);
				//Note: Instances location need to be local
				if (tempA != nullptr){
					tempA->AttachRootComponentTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
					tempA->SetActorRelativeTransform(BakedParts[i].MeshLocations[j].GetRelativeTransform(GetTransform()));
					BakedParts[i].SmartObject.Add(tempA);
				}
			}

			break;
		case BuildPart_Types::INT_DOOR:

			for (int32 j = 0; j < BakedParts[i].MeshLocations.Num(); j++){
				AActor * tempA = GetWorld()->SpawnActor<AActor>(InsideDoor->GeneratedClass);
				//Note: Instances location need to be local
				if (tempA != nullptr){
					tempA->AttachRootComponentTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
					tempA->SetActorRelativeTransform(BakedParts[i].MeshLocations[j].GetRelativeTransform(GetTransform()));
					BakedParts[i].SmartObject.Add(tempA);
				}
			}
			break;
		case BuildPart_Types::EXT_SLIDEDOOR:

			for (int32 j = 0; j < BakedParts[i].MeshLocations.Num(); j++){
				AActor * tempA = GetWorld()->SpawnActor<AActor>(SlidingDoor->GeneratedClass);
				//Note: Instances location need to be local
				if (tempA != nullptr){
					tempA->AttachRootComponentTo(BaseRoot, NAME_None, EAttachLocation::KeepRelativeOffset);
					tempA->SetActorRelativeTransform(BakedParts[i].MeshLocations[j].GetRelativeTransform(GetTransform()));
					BakedParts[i].SmartObject.Add(tempA);
				}
			}
			break;
			default:
				break;
		}
		if (BakedParts[i].MeshObject == nullptr){
			BakedParts[i].MeshObject = NewObject<UInstancedStaticMeshComponent>(this, UInstancedStaticMeshComponent::StaticClass());
			if (BakedParts[i].MeshObject != nullptr){
				BakedParts[i].MeshObject->AttachTo(BaseRoot, NAME_None);
				//All UObjects must be registed before they can be used. 
				BakedParts[i].MeshObject->RegisterComponent();
			}
		}
		else{
			BakedParts[i].MeshObject->ClearInstances();
		}

		if (BakedParts[i].MeshObject != nullptr){
			BakedParts[i].MeshObject->SetStaticMesh(BakedParts[i].sMesh);

			for (int32 j = 0; j < BakedParts[i].MeshLocations.Num(); j++){
				//Note: Instances location need to be local
				BakedParts[i].MeshObject->AddInstance(BakedParts[i].MeshLocations[j].GetRelativeTransform(GetTransform()));
			}
		}
	}
	UE_LOG(LogTemp, Log, TEXT("ACityPathBuilder: DisplayInstanceList Complete"));
}

/*
	Clears all baked instance meshes
*/
void ABuildingModGen::ClearBakedData(){
	for (int32 i = 0; i < BakedParts.Num(); i++){
		if (BakedParts[i].MeshObject != nullptr){
			BakedParts[i].MeshObject->ClearInstances();
			BakedParts[i].MeshObject->DestroyComponent();
		}
		if (BakedParts[i].SmartObject.Num()){
			for (int32 j = 0; j < BakedParts[i].SmartObject.Num(); j++){
				BakedParts[i].SmartObject[j]->Destroy();
			}
		}
	}
	BakedParts.Empty();
}
