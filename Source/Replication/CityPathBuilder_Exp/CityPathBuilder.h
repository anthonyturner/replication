// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Classes/Components/SplineComponent.h"
#include "Runtime/Engine/Classes/Components/SplineMeshComponent.h"
#include "Runtime/Landscape/Classes/LandscapeProxy.h"
#include "BuildingModGen.h"
#include "GameFramework/Actor.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <windows.h>
#include <conio.h>
#include "CityPathBuilder.generated.h"


using namespace std;

UCLASS()
class REPLICATION_API ACityPathBuilder : public AActor
{
	GENERATED_BODY()
	
public:	
	

	/**********************************************************************************************/

	// Sets default values for this actor's properties
	ACityPathBuilder(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	//Same as the constrution script
	void OnConstruction(const FTransform & Transform) override;
	
	int SearchDirectory(FString filepath, FString Prefix);
	void setData(void* data, TArray<uint8>& TheBinaryArray, int * offset, int bytestoread);

	//Bakign functions
	void BuildInstanceList();
	void DisplayInstanceList();
	void ClearBakedData();

	/**********************************************************************************************/
	int32 FileVersion;
	FString BuildingDataPath;
	TArray<FString> BuildingFiles;

	/**********************************************************************************************/

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void ProcessCreationBuildings(TArray<int32> BuildingID, TArray<FTransform> BuildingTransform, bool IsLeftSide);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void UpdateBlockTransform(TArray<FTransform> BuildingTransform, bool IsLeftSide);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void cleanBuildingData(bool IsLeftSide);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		int32 GetNumberOfBuildings(bool IsLeftSide);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		float GetBuildingWidth(int32 index, bool IsLeftSide);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void CreateRoad();
	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void SetupProxy();
	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void BuildBuildingSingleSide(bool isLeftSide, TArray<int32>& IDList, TArray<FTransform>& TransformList);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		bool PickRandomBuilding(FVector& ModularDimentions, TArray<int32>& IDList,FVector& SelectedDimentions, float& BuildingLength,float& DistanceOnSpline, float& TotalSplineLength);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void GetLookAtRotation(bool isLeftSide, float DistanceOnSpline,	float& temp360Rot, FRotator& tempRot);

	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void GetDisplacedLocation(float temp360Rot, bool isLeftSide, float DistanceOnSpline, FVector& tempLocation);
	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void AddTerrainSplinePoints(bool isLeftSide, FVector BuildingLocation, float DistanceOnSpline, float BuildingLength);
		/*
	UFUNCTION(BlueprintCallable, Category = "CityPath")
		void UpdateBuildingTerrain();
		*/
	/**********************************************************************************************/

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRandomStream RNG;

	/**********************************************************************************************/
	UPROPERTY()
		USphereComponent* BaseRoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USplineComponent * PathRoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USplineComponent* _LSpline;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USplineComponent* _RSpline;
	
	UPROPERTY()
		TArray<ABuildingModGen*> LeftBlock;
	UPROPERTY()
		TArray<ABuildingModGen*> RightBlock;

	/**********************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "A_Setup")
		UStaticMesh* proxyMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "A_Setup")
		FVector ModularSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "A_Setup")
		TSubclassOf<class ABuildingModGen> BuildingTheme;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "A_Setup")
		FString PrefabFilterPrefix;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "A_Setup")
		bool LoadBuildingSettings;

	/**********************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		float BuildingSpacing;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		float StreetWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		bool L_BlockActive;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		bool R_BlockActive;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		TEnumAsByte<CityStructStates::CityStructState> CurrentBuildState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		bool ShowProxy;
	/**********************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		TArray<ABuildingModGen*>  GenBuildings;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BuildingData")
		TArray<FVector> BuildingDimensions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		int32 LeftBlockCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		TArray<int32> L_BlockIDS;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		TArray<FTransform> L_Block_Transform;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		int32 RightBlockCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		TArray<int32> R_BlockIDS;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		TArray<FTransform> R_Block_Transform;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingData")
		TArray<int32> ProxyHandles;


	/**********************************************************************************************/

	/**********************************************************************************************/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		TArray<USplineMeshComponent *> RoadParts;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		UStaticMesh * RoadMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		FVector2D RoadScale;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		FVector RoadUpDir;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		int32 NumberOfRoads;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		ALandscapeProxy * TerrTarget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		float RoadTerrWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		float RoadTerrFalloff;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		float BuildingTerrWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		float BuildingTerrFalloff;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TerrainSettings")
		bool UpdateTerrain;
};
