// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#include "Replication.h"
#include "CityPathBuilder_Exp.h"
#include "CityPathBuilder_ExpGameMode.h"
#include "CityPathBuilder_ExpCharacter.h"

ACityPathBuilder_ExpGameMode::ACityPathBuilder_ExpGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Pawns/PCs/BP_ReplicationCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
