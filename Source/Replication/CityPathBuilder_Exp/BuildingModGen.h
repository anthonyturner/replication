// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BuildingModLevel.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <windows.h>
#include <conio.h>
#include "BuildingModGen.generated.h"


using namespace std;

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class REPLICATION_API ABuildingModGen : public AActor
{
	GENERATED_BODY()

	void OnConstruction(const FTransform & Transform);

	UPROPERTY()
		USphereComponent* BaseRoot;

	//File version of imported build files
	int32 FileVersion;

	//File Data Vars
	/*****************************************************************************************/
	FString BuildingDataPath;
	TArray<FString> BuildingFiles;
	TArray<FTileFloors> LevelData;
	void setData(void* data, TArray<uint8>& TheBinaryArray, int * offset, int bytestoread);
	/*****************************************************************************************/

public:

	ABuildingModGen(const FObjectInitializer& ObjectInitializer);

	/*****************************************************************************************/

	int SearchDirectory(FString filepath, FString Prefix);
	void cleanUp();
	void updateConstruction();

	//Loading Funcs
	void LoadBuildingData();
	void LoadLevelTypeData(int loadBuildingID);

	//Baking Fucntions
	void ExpandMeshList(TArray<FInstancedStructures> & BakedParts);
	void BuildInstanceList();
	void DisplayInstanceList();
	void ClearBakedData();

	/*****************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
		TArray<UStaticMesh*> LevelMeshList;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
		UBlueprint * MainDoor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
		UBlueprint * InsideDoor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
		UBlueprint * SlidingDoor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		int32 NumberOfFloors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		FVector LevelDementionUnits;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		FVector UnitSizeLDH;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		FString PrefabFilterPrefix;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingSettings")
		int32 DesiredPrefab;

	/*****************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingActions")
		bool CreateLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingActions")
		bool UpdateLevels;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingActions")
		bool ClearData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingActions")
		bool LoadPrefabData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingActions")
		bool CreatePrefab;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingActions")
		bool BakeBuilding;

	/*****************************************************************************************/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BuildingData")
		TArray<FString> PreFabList;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BuildingData")
		TArray<FVector> BuildingDimensions;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BuildingData")
		TArray<FInstancedStructures> BakedParts;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BuildingData")
		TArray<UBlueprint *> SmartObjects;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BuildingData")
		TArray<ABuildingModLevel*> Level;



};
