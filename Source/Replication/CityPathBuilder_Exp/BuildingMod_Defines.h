
#pragma once

#include "Object.h"
#include "Runtime/Engine/Classes/Components/InstancedStaticMeshComponent.h"
#include "BuildingMod_Defines.generated.h"

//ENUMS
UENUM(BlueprintType)       //"BlueprintType" is essential specifier
namespace BuildPart_Types
{
	enum BuildPart_Type{

		NONE = 0,
		BLANK,

		Floor,
		FloorSL,

		INT_NORM_WALL,
		INT_CORN_WALL,
		INT_ANGL_WALL,
		INT_DOUB_WALL,
		INT_END_WALL,
		INT_DOOR,
		INT_STAIRS,
		INT_WINDOW,

		EXT_NORM_WALL,
		EXT_CORN_WALL,
		EXT_ANGL_WALL,
		EXT_DOUB_WALL,
		EXT_END_WALL,
		EXT_DOOR,
		EXT_STAIRS,
		EXT_WINDOW,
		EXT_SLIDEDOOR,
		EXT_WIDEWINDOW,

		RF_SIDE,
		RF_CORN,
		RF_INSI,
		RF_ANGL,
		RF_DOUB,
		RF_END,
		RF_TOP,
		RF_SL,

		EX_BDTrimDecoFloor,
		EX_BDTrimDecoMid,
		EX_BDTrimDecoCorner,
		EX_Lattice,
		EX_LatticeCorner,
		DEC_RailMid,
		DEC_RailCorner,
		DEC_RailBare,
		DEC_Column,

		DEC_Blinds,
		DEC_Chandelier,
		DEC_Curtains,
		DEC_HangLamp,
		DEC_FLLamp,
		DEC_Frame1,
		DEC_Frame2,
		DEC_Frame3,
		DEC_Rug,
		DEC_BowlLamp,
		DEC_OutDoorLamp1,
		DEC_OutDoorLamp2,
		DEC_WallLamp1,
		DEC_WallLamp2,

		FUR_BreakfastTable,
		FUR_SofaChair,
		FUR_Counter,
		FUR_Desk,
		FUR_DinningTable,
		FUR_Sofa,
		FUR_TV,
		FUR_VicBed,
		FUR_BookShelf,
		FUR_Crate,
		FUR_CrateStack,
		FUR_DresserShort,
		FUR_ForierTable,
		FUR_Shelf,
		FUR_TallDresser
	};
}
//ENUMS
UENUM(BlueprintType)       //"BlueprintType" is essential specifier
namespace BuildPart_POSTypes
{
	enum BuildPart_POSType{
		PT_NONE = 0,
		PT_BLC,
		PT_BM,
		PT_BRC,
		PT_MRM,
		PT_TRC,
		PT_TM,
		PT_TLC,
		PT_MLM,
		PT_MAX
	};
}

USTRUCT(BlueprintType)
struct FTileSetData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		int32 TileSetIDs;

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		TEnumAsByte<BuildPart_Types::BuildPart_Type> MeshType;

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		TEnumAsByte<BuildPart_POSTypes::BuildPart_POSType> PosType;
	
};

USTRUCT(BlueprintType)
struct FTileFloors
{
	GENERATED_USTRUCT_BODY()
		UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
			int32 GridX;
		UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
			int32 GridY;
		UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
			TArray<FTileSetData> EXT_Wall;
		UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
			TArray<FTileSetData> INT_Wall;
		UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
			TArray<FTileSetData> Deco_Layer;
		UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
			TArray<FTileSetData> Furn_Layer;
};

USTRUCT(BlueprintType)
struct FWallPOSType {
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		TEnumAsByte<BuildPart_POSTypes::BuildPart_POSType> WallType;

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		int32 XOffset;

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		int32 YOffset;

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		int32 ZRotMod;

};


//ENUMS
UENUM(BlueprintType)       //"BlueprintType" is essential specifier
namespace CityStructStates
{
	enum CityStructState{
		CS_Proxy = 0,
		CS_Generate,
		CS_Tweaking,
		CS_Bake,
		CS_Dormate
	};
}

USTRUCT(BlueprintType)
struct FBuildingTheme
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		TArray<UStaticMesh*> LevelMeshList;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		FVector UnitSizeLDH;

};


USTRUCT(BlueprintType)
struct FInstancedStructures
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "BakedData")
		UInstancedStaticMeshComponent * MeshObject;
	
	UPROPERTY(BlueprintReadOnly, Category = "BakedData")
		TArray<AActor *> SmartObject;

	UPROPERTY(BlueprintReadOnly, Category = "BakedData")
		UStaticMesh * sMesh;
	
	UPROPERTY(BlueprintReadOnly, Category = "BakedData")
		TArray<FTransform> MeshLocations;

	UPROPERTY(BlueprintReadOnly, Category = "WallTypes")
		TEnumAsByte<BuildPart_Types::BuildPart_Type> SmartObjectType;
};


inline bool isSmartObjectType(TEnumAsByte<BuildPart_Types::BuildPart_Type> Type){
	switch (Type){
	case BuildPart_Types::EXT_DOOR:
	case BuildPart_Types::INT_DOOR:
	case BuildPart_Types::EXT_SLIDEDOOR:
		return true;
		break;
	}
	return false;
}
