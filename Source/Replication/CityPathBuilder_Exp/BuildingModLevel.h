// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BuildingModTile.h"
#include "BuildingModLevel.generated.h"

USTRUCT(BlueprintType)
struct FTiles {
	GENERATED_USTRUCT_BODY()

		UPROPERTY()
		TArray<ABuildingModTile*> Column;

	FTiles::~FTiles(){
		Column.Empty();
	}
};
/**
 * 
 */
UCLASS()
class REPLICATION_API ABuildingModLevel : public AActor
{
	GENERATED_BODY()

	ABuildingModLevel(const FObjectInitializer& ObjectInitializer);

	UPROPERTY()
		USphereComponent* BaseRoot;

	/**********************************************************************************************/
	FTileFloors LayoutData;

	/**********************************************************************************************/

public:

	/**********************************************************************************************/

	void OnConstruction(const FTransform & Transform);
	void cleanUp();
	void updatesettings();

	void setMasterData(TArray<UStaticMesh*> NewList, FVector UnitSize, FVector DementionUnits, FTileFloors Layout);
	void setMasterData(TArray<UStaticMesh*> NewList, FVector UnitSize, FVector DementionUnits);

	void ExpandMeshList(TArray<FInstancedStructures> & BakedParts);

	/**********************************************************************************************/
	TArray<FTiles*> Rows;

	/**********************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
		TArray<UStaticMesh*> LevelMeshList;

	/**********************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TileSettings")
		FVector LevelDementionUnits;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TileSettings")
		FVector UnitSizeLDH;

	/**********************************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TileAction")
		bool ResetLevel;
	
	
	
};
