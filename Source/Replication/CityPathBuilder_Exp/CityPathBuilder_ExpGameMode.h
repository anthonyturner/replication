// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "CityPathBuilder_ExpGameMode.generated.h"

UCLASS(minimalapi)
class ACityPathBuilder_ExpGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ACityPathBuilder_ExpGameMode();
};



