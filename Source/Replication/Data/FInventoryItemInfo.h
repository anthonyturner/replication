#pragma once

#include "FInventoryItemInfo.generated.h"


USTRUCT(BlueprintType)
struct REPLICATION_API FInventoryItemInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Price;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* Icon;

	

	FInventoryItemInfo() {


	}
};