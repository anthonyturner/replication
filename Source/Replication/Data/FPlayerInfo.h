// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FPlayerInfo.generated.h"


USTRUCT(BlueprintType)
struct REPLICATION_API FPlayerInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName playerName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* playerImage;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ACharacter* playerCharacter;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* playerCharacterImage;
	bool PlayerStatus;

	FPlayerInfo() {


	}
};