// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "GameWeapon_Instance.h"

AGameWeapon_Instance::AGameWeapon_Instance() {

	//InstantConfig.WeaponRange = 
}
//////////////////////////////////////////////////////////////////////////
// Weapon usage

void AGameWeapon_Instance::UseWeapon()
{
	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = GetCurrentSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpread * 0.5f);

	const FVector AimDir = GetWeaponDirection();
	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	const FVector EndTrace = StartTrace + GetPawnOwner()->GetActorForwardVector() * InstantConfig.WeaponRange;

	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	//ProcessInstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);

	CurrentFiringSpread = FMath::Min(InstantConfig.FiringSpreadMax, CurrentFiringSpread + InstantConfig.FiringSpreadIncrement);
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage helpers

float AGameWeapon_Instance::GetCurrentSpread() const
{
	return 4.0f;
}


