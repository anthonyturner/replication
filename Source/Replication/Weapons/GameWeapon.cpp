// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "GameWeapon.h"

// Sets default values
AGameWeapon::AGameWeapon()
{

	bIsAttacking = false;
	AttackCounter = 0;	
	bPlayingAttackAnim = false;
	bPendingEquip = false;
	bIsEquipped = false;

}

// Called when the game starts or when spawned
void AGameWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}


void AGameWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	DetachMeshFromPawn();
}

void AGameWeapon::UseItem() {


}

//////////////////////////////////////////////////////////////////////////
// Inventory

void AGameWeapon::OnEquip(const AGameWeapon* LastWeapon)
{

	bPendingEquip = true;

	// Only play animation if last weapon is valid
	if (LastWeapon)
	{
		float Duration = PlayWeaponAnimation(EquipAnim);
		if (Duration <= 0.0f)
		{
			// failsafe
			Duration = 0.5f;
		}
		EquipStartedTime = GetWorld()->GetTimeSeconds();
		EquipDuration = Duration;
		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipFinished, this, &AGameWeapon::OnEquipFinished, EquipAnim.TimerHandlerDuration, false);
	}
	else
	{
		OnEquipFinished();
	}

	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		PlayWeaponSound(EquipSound);
	}
}

void AGameWeapon::OnEquipFinished()
{
	AttachMeshToPawn();

	bIsEquipped = true;
	bPendingEquip = false;

}

void AGameWeapon::OnUnEquipFinished()
{
	DetachMeshFromPawn();
	bIsEquipped = false;
	bPendingUnEquip = false;

}

void AGameWeapon::OnUnEquip()
{

	bPendingUnEquip = true;
	float Duration = PlayWeaponAnimation(UnEquipAnim);
	if (Duration <= 0.0f)
	{
		// failsafe
		Duration = 0.5f;
	}
	UnEquipStartedTime = GetWorld()->GetTimeSeconds();
	UnEquipDuration = Duration;
	GetWorldTimerManager().SetTimer(TimerHandle_OnUnEquipFinished, this, &AGameWeapon::OnUnEquipFinished, UnEquipAnim.TimerHandlerDuration, false);

	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		PlayWeaponSound(UnEquipSound);
	}
	
}

void AGameWeapon::OnEnterInventory(AReplicationCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
}

void AGameWeapon::OnLeaveInventory()
{
	if (Role == ROLE_Authority)
	{
		SetOwningPawn(NULL);
	}

	if (IsAttachedToPawn())
	{
		OnUnEquip();
	}
}

bool AGameWeapon::IsAttachedToPawn() const
{
	return bIsEquipped || bPendingEquip;
}


void AGameWeapon::AttachMeshToPawn()
{
	if (MyPawn)
	{
		// Remove and hide both first and third person meshes
		DetachMeshFromPawn();

		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		FName AttachPoint = MyPawn->GetWeaponAttachPoint();
		if (MyPawn->IsLocallyControlled() == true)
		{
			USkeletalMeshComponent* PawnMesh3p = MyPawn->GetSpecifcPawnMesh(false);
			Mesh3P->SetHiddenInGame(false);
			Mesh3P->AttachToComponent(PawnMesh3p, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint);
		}
		else
		{
			USkeletalMeshComponent* UseItemMesh = GetItemMesh();
			USkeletalMeshComponent* UsePawnMesh = MyPawn->GetPawnMesh();
			UseItemMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint);
			UseItemMesh->SetHiddenInGame(false);
		}
	}
}


void AGameWeapon::DetachMeshFromPawn()
{
	USkeletalMeshComponent* UseItemMesh = GetItemMesh();

	if (UseItemMesh) {

		if (MyPawn) {
			USkeletalMeshComponent* PawnMesh3p = MyPawn->GetSpecifcPawnMesh(false);
			if (PawnMesh3p) {


				UseItemMesh->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
				//UseItemMesh->SetHiddenInGame(true);
				Mesh3P->AttachToComponent(PawnMesh3p, FAttachmentTransformRules::KeepRelativeTransform, FName("SecondaryAttachPoint"));
			}
		}



	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// Start-stop attacks

void AGameWeapon::StartAttack() {

	if (Role < ROLE_Authority) { //If client, call server

		ServerStartAttack();
	}

	if (!bIsAttacking) {

		bIsAttacking = true;
		
		OnAttackStarted();
	}
}




bool AGameWeapon::ServerStartAttack_Validate()
{
	return true;
}

void AGameWeapon::ServerStartAttack_Implementation()
{
	StartAttack();
}


void AGameWeapon::OnAttackStarted() {


	HandleAttacking();
}


void AGameWeapon::StopAttack() {

	if (Role < ROLE_Authority)
	{
		ServerStopAttack();
	}

	if (bIsAttacking)
	{
		bIsAttacking = false;
		OnAttackFinished();
	}

}


bool AGameWeapon::ServerStopAttack_Validate()
{
	return true;
}

void AGameWeapon::ServerStopAttack_Implementation()
{
	StopAttack();
}

void AGameWeapon::OnAttackFinished() {

	// stop attacking FX on remote clients
	AttackCounter = 0;

	// stop attacking FX locally, unless it's a dedicated server
	if (GetNetMode() != NM_DedicatedServer)
	{
		StopSimulatingWeaponAttack();
	}

}



///////////////////////////////////////////////////////////////////////////////////////////
// Handle attacks

void AGameWeapon::HandleAttacking() {

	//Clients simulate there weapon and sound animations
	//And then they call the server to handle other clients ( and server ) simulated weapon and sound animations
	if (true) {//if CanAttack();

		//Clients and server simulate weapon attacks
		SimulateWeaponAttack();
		// update attack FX on remote clients if function was called on server
		if (Role == ROLE_Authority) {
			AttackCounter++; //Triggered if Server called attack and triggers OnRep_Attack on clients
		}

	} else if (MyPawn && MyPawn->IsLocallyControlled()) {//Can't attack so stop attack

		// stop weapon attack FX
		if (AttackCounter > 0) {
			OnAttackFinished();
		}
	}

	//Server call to handle attack
	if (MyPawn && MyPawn->IsLocallyControlled()) {

		// local client will notify server
		if (Role < ROLE_Authority) {

			//Actual attack happens on server
			ServerHandleAttacking();
		}
	}
//	LastFireTime = GetWorld()->GetTimeSeconds();
}


bool AGameWeapon::ServerHandleAttacking_Validate()
{
	return true;
}

void AGameWeapon::ServerHandleAttacking_Implementation(){

	HandleAttacking();//Allows server to handle attack fx for itself
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage helpers

UAudioComponent* AGameWeapon::PlayWeaponSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound && MyPawn)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, MyPawn->GetRootComponent());
	}

	return AC;
}

float AGameWeapon::PlayWeaponAnimation(const FWeaponAnim& Animation)
{
	float Duration = 0.0f;
	if (MyPawn){

		UAnimMontage* UseAnim =  Animation.weaponAnim;
		if (UseAnim){

			UAnimInstance* animInstance = MyPawn->GetMesh()->GetAnimInstance();
			FName AttackOne("Attack1");
			FName AttackTwo("Attack2");

			Duration = MyPawn->PlayAnimMontage(UseAnim, 1.0f);

			/*if (animInstance->Montage_IsActive(UseAnim)) {

				Duration = MyPawn->PlayAnimMontage(UseAnim, 1.0f, AttackTwo);
				GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Yellow, "Not Playing weapon montage, starting play");
			} else {

				GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Yellow, "Playing weapon montage, switch section");
				//Get current section that's playing
				FName section = animInstance->Montage_GetCurrentSection(UseAnim);

				if (section.IsEqual(AttackOne)) {

					animInstance->Montage_SetNextSection(section, AttackTwo);
				}
			}*/
		}
	}

	return Duration;
}

void AGameWeapon::StopWeaponAnimation(const FWeaponAnim& Animation)
{
	if (MyPawn)
	{
		UAnimMontage* UseAnim = Animation.weaponAnim;
		if (UseAnim)
		{
			MyPawn->StopAnimMontage(UseAnim);
			count = 0;
		}
	}
}

FVector AGameWeapon::GetAdjustedAim() const
{
	APlayerController* const PlayerController = Instigator ? Cast<APlayerController>(Instigator->Controller) : NULL;
	FVector FinalAim = FVector::ZeroVector;
	// If we have a player controller use it for the aim
	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}

	return FinalAim;
}

FVector AGameWeapon::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	APlayerController* PC = MyPawn ? Cast<APlayerController>(MyPawn->Controller) : NULL;
	APlayerController* AIPC = MyPawn ? Cast<APlayerController>(MyPawn->Controller) : NULL;
	FVector OutStartTrace = FVector::ZeroVector;
	OutStartTrace = GetWeaponLocation();

	return OutStartTrace;
}

FVector AGameWeapon::GetWeaponLocation() const
{
	USkeletalMeshComponent* UseMesh = GetItemMesh();
	return UseMesh->GetSocketLocation(WeaponAttachPoint);
}

FVector AGameWeapon::GetWeaponDirection() const
{
	USkeletalMeshComponent* UseMesh = GetItemMesh();
	return UseMesh->GetSocketRotation(WeaponAttachPoint).Vector();
}

FHitResult AGameWeapon::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{

	FHitResult Hit(ForceInit);
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
	UWorld* world = GetWorld();
	if (world) {
		world->DebugDrawTraceTag = WeaponFireTag;
		// Perform trace to retrieve hit info
		FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;
		TraceParams.TraceTag = WeaponFireTag;
		world->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECollisionChannel::ECC_Camera, TraceParams);
	}


	return Hit;
}



//////////////////////////////////////////////////////////////////////////
// Controls

bool AGameWeapon::CanAttack() const
{
	return true;
//	return MyPawn && MyPawn->CanAttack();
	
}

////////////////////////////////////////////////////////////////////////////////////
// Rep notifies

void AGameWeapon::OnRep_AttackCounter() {

	if (AttackCounter > 0)
	{
		SimulateWeaponAttack();
	}
	else
	{
		StopSimulatingWeaponAttack();
	}
}

void AGameWeapon::SimulateWeaponAttack()
{

	if (!bPlayingAttackAnim)
	{
		PlayWeaponAnimation(AttackAnim);
		bPlayingAttackAnim = true;
	}

	PlayWeaponSound(AttackSound);
}

void AGameWeapon::DoWeaponAttack() {
	//Called from AnimBlueprint when attack/anim montage is at a notify point

	//If you're trying to make sure something is only performed by the owning client, 
	//you can run a Multicast and you can query Is Local Player Controller,
	//and then only the client that owns it will execute.
	//https://www.reddit.com/r/unrealengine/comments/4a45c3/how_to_grab_local_playercontroller_in_multiplayer/
	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		UseWeapon();
	}
	
}

void AGameWeapon::StopSimulatingWeaponAttack()
{
	
	if (bPlayingAttackAnim)
	{
		StopWeaponAnimation(AttackAnim);
		bPlayingAttackAnim = false;
	}

	/*if (AttackAC)
	{
		AttackAC->FadeOut(0.1f, 0.0f);
		AttackAC = NULL;

		PlayWeaponSound(AttackFinishSound);
	}*/
}

void AGameWeapon::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(AGameWeapon, AttackCounter, COND_SkipOwner);
}
