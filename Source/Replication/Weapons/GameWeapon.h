// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Items/GameItem.h"
#include "GameWeapon.generated.h"



USTRUCT()
struct FWeaponAnim
{
	GENERATED_USTRUCT_BODY()

	/** animation played on pawn (3rd person view) */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* weaponAnim;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		float TimerHandlerDuration;

};

UCLASS()
class REPLICATION_API AGameWeapon : public AGameItem
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameWeapon();
	int count = 0;

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** weapon is being equipped by owner pawn */
	virtual void OnEquip(const AGameWeapon* LastWeapon);

	/** weapon is now equipped by owner pawn */
	virtual void OnEquipFinished();
	/** weapon is now equipped by owner pawn */
	virtual void OnUnEquipFinished();

	/** weapon is holstered by owner pawn */
	virtual void OnUnEquip();

	/** [server] weapon was added to pawn's inventory */
	virtual void OnEnterInventory(AReplicationCharacter* NewOwner);

	/** [server] weapon was removed from pawn's inventory */
	virtual void OnLeaveInventory();

	/** check if it's currently equipped */
	bool IsEquipped() const;

	/** check if mesh is already attached */
	bool IsAttachedToPawn() const;

	/** is weapon currently equipped? */
	uint32 bIsEquipped : 1;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** perform initial setup */
	virtual void PostInitializeComponents() override;

	bool bIsAttacking;

	/** is attack animation playing? */
	bool bPlayingAttackAnim;

	

	/** attack counter, used for replicating attack events to remote clients */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_AttackCounter)
		int32 AttackCounter;
	
	/** single attack sound (bLoopedattackSound not set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* AttackSound;

	/** looped attack sound (bLoopedattackSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* AttackLoopSound;

	/** finished burst sound (bLoopedattackSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* AttackFinishSound;

	/** attack animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FWeaponAnim AttackAnim;
	/** equip sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* EquipSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* UnEquipSound;

	/** equip animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FWeaponAnim EquipAnim;

	/** equip animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		FWeaponAnim UnEquipAnim;

	/** Handle for efficient management of OnEquipFinished timer */
	FTimerHandle TimerHandle_OnEquipFinished;

	/** Handle for efficient management of OnEquipFinished timer */
	FTimerHandle TimerHandle_OnUnEquipFinished;

	/** last time when this weapon was switched to */
	float EquipStartedTime, UnEquipStartedTime;

	/** how much time weapon needs to be equipped */
	float EquipDuration, UnEquipDuration;



	/** is equip animation playing? */
	uint32 bPendingEquip : 1;
	uint32 bPendingUnEquip : 1;
	//////////////////////////////////////////////////////////////////////////
	// Input - server side

	UFUNCTION(reliable, server, WithValidation)
		void ServerStartAttack();

	UFUNCTION(reliable, server, WithValidation)
		void ServerStopAttack();

	

	//////////////////////////////////////////////////////////////////////////
	// Replication & effects
	
	virtual void UseItem() override;

	UFUNCTION()
		void OnRep_AttackCounter();

	UFUNCTION(BlueprintCallable, Category = WeaponAnims)
	void DoWeaponAttack();

	/** Called in network play to do the cosmetic fx for attacking */
	virtual void SimulateWeaponAttack();

	/** Called in network play to stop cosmetic fx (e.g. for a looping attack). */
	virtual void StopSimulatingWeaponAttack();

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	/** [local] weapon specific use implementation */
	virtual void UseWeapon() PURE_VIRTUAL(AGameWeapon::UseWeapon, );

	/** [server] attack */
	UFUNCTION(reliable, server, WithValidation)
		void ServerHandleAttacking();

	/** [local + server] handle weapon attacking */
	void HandleAttacking();

	/** [local + server] Attacking started */
	virtual void OnAttackStarted();

	/** [local + server] Attacking finished */
	virtual void OnAttackFinished();

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** attaches weapon mesh to pawn's mesh */
	void AttachMeshToPawn();

	/** detaches weapon mesh from pawn */
	void DetachMeshFromPawn();

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage helpers

	/** play weapon sounds */
	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	/** play weapon animations */
	float PlayWeaponAnimation(const FWeaponAnim& Animation);

	/** stop playing weapon animations */
	void StopWeaponAnimation(const FWeaponAnim& Animation);

	/** Get the aim of the weapon, allowing for adjustments to be made by the weapon */
	virtual FVector GetAdjustedAim() const;

	/** Get the aim of the camera */
	//FVector GetCameraAim() const;

	/** get the originating location for camera damage */
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	/** get the muzzle location of the weapon */
	//FVector GetMuzzleLocation() const;
	FVector GetWeaponLocation() const;
	/** get direction of weapon's muzzle */
	FVector GetWeaponDirection() const;

	/** name of bone/socket for muzzle in weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
		FName WeaponAttachPoint;

	/** find hit */
	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;

public:	
	

	//Input
	virtual void StartAttack();
	virtual void StopAttack();

	//////////////////////////////////////////////////////////////////////////
	// Control

	/** check if weapon can attack */
	bool CanAttack() const;


};
