// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseWeapon.generated.h"



USTRUCT()
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

		/** weapon range */
		UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
		float WeaponRange;

	/** damage amount */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
		int32 HitDamage;

	/** type of damage */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
		TSubclassOf<UDamageType> DamageType;

	/** weapon range */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
		USoundCue* AttackSound;

	/** weapon range */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
		UAnimMontage* AttackAnim;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		class UForceFeedbackEffect* HitSuccessFeedback;

	/** defaults */
	FWeaponData()
	{

		WeaponRange = 100.f;
		HitDamage = 10;
		DamageType = UDamageType::StaticClass();

	}
};
UCLASS()
class REPLICATION_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	/** weapon config */
	UPROPERTY(EditDefaultsOnly, Category = Config)
		FWeaponData WeaponConfig;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void StartAttack();
	
	
};
