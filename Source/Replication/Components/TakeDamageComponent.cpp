// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "TakeDamageComponent.h"


// Sets default values for this component's properties
UTakeDamageComponent::UTakeDamageComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
//	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTakeDamageComponent::BeginPlay()
{
	Super::BeginPlay();
	owner = Cast<AReplicationCharacter>(GetOwner());
	owner->OnTakeAnyDamage.AddDynamic(this, &UTakeDamageComponent::HandleDamage);
	
}


// Called every frame
void UTakeDamageComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTakeDamageComponent::HandleDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser) {

	
	if (owner->Role == ROLE_Authority) {
				
		StartTakeDamage(Damage);
	}
	else {//This is the client

		GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, "client calling");
		ServerHandleDamage(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);
	}
}



bool UTakeDamageComponent::ServerHandleDamage_Validate(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser) { return true; }

void UTakeDamageComponent::ServerHandleDamage_Implementation(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser) {

	HandleDamage(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);
}



void UTakeDamageComponent::StartTakeDamage(float damage) {

	GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, "take damage");

	float Health = owner->Health - damage;
	if (Health <= 0) {
		Health = 0;
	}
	owner->Health = Health;
}




