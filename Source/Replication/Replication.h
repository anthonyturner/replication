// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#ifndef __REPLICATION_H__
#define __REPLICATION_H__

#include "Engine.h"
#include "Net/UnrealNetwork.h"
#include "Online.h"
#include "Libraries/CombatFunctionLibrary.h"
#include "Core/ReplicationGameInstance.h"
#include "Core/ReplicationGameMode.h"
#include "Online/ReplicationGameStateBase.h"
#include "Player/ReplicationCharacter.h"
#include "Player/ReplicationPlayerController.h"

DECLARE_LOG_CATEGORY_EXTERN(LogReplication, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogReplicationWeapon, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogUI, Log, All);

/** when you modify this, please note that this information can be saved with instances
* also DefaultEngine.ini [/Script/Engine.CollisionProfile] should match with this list **/
#define COLLISION_WEAPON		ECC_GameTraceChannel1
#define COLLISION_PROJECTILE	ECC_GameTraceChannel2
#define COLLISION_PICKUP		ECC_GameTraceChannel3


#endif
