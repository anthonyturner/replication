// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IInteractable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
//UINTERFACE(meta = (CannotImplementInterfaceInBlueprint))
class UIInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class REPLICATION_API IIInteractable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual void OnEnterPlayerRadius(ABaseCharacter* characterRef);
	virtual void OnLeavePlayerRadius(ABaseCharacter* characterRef);
	virtual void OnInteract(ABaseCharacter* characterRef);
};
