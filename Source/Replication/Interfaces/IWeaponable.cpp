// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "IWeaponable.h"


// This function does not need to be modified.
UIWeaponable::UIWeaponable(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IIWeaponable functions that are not pure virtual.
