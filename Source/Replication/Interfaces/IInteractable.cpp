// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "IInteractable.h"


// Add default functionality here for any IIInteractable functions that are not pure virtual.

void IIInteractable::OnEnterPlayerRadius(ABaseCharacter* characterRef) {

	
}

void IIInteractable::OnLeavePlayerRadius(ABaseCharacter* characterRef) {


}

void IIInteractable::OnInteract(ABaseCharacter* characterRef) {


}