// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "QuestMainWidget.generated.h"

class AQuestManager;
class UQuestWidget;
UCLASS()
class REPLICATION_API UQuestMainWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	void SetQuestManager(AQuestManager* QuestManageRef);
	AQuestManager* GetQuestManager();

	UFUNCTION(BlueprintCallable)
	UQuestWidget* AddQuestToList(AMasterQuest* QuestToAdd);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		TSubclassOf<UQuestWidget>QuestWidgetClass;

	UFUNCTION(BlueprintImplementableEvent, Category = Quest)
	void AddQuestToUi(UQuestWidget* questForUi);

	UPROPERTY()
		TArray<UQuestWidget*> QuestWidgets;

	private:
		UPROPERTY()
		AQuestManager* QuestManager;

	
};
