// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "QuestMainWidget.h"
#include "QuestSystem/Widgets/QuestWidget.h"

UQuestWidget* UQuestMainWidget::AddQuestToList(AMasterQuest* QuestToAdd) {

	if (QuestToAdd && QuestWidgetClass) {
		
		UQuestWidget* questWidget =  CreateWidget<UQuestWidget>(GetWorld(), QuestWidgetClass);
		questWidget->QuestManager = QuestManager;
		questWidget->AssignedQuest = QuestToAdd;
		QuestWidgets.Add(questWidget);
		AddQuestToUi(questWidget);
		return questWidget;
	}

	return nullptr;
}




void UQuestMainWidget::SetQuestManager(AQuestManager* QuestManageRef)
{
	QuestManager = QuestManageRef;
}

AQuestManager* UQuestMainWidget::GetQuestManager()
{
	return QuestManager;
}