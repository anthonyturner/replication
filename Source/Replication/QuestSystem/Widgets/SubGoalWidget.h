// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SubGoalWidget.generated.h"


UCLASS()
class REPLICATION_API USubGoalWidget : public UUserWidget
{
	GENERATED_BODY()

public:

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
			FGoalInfo GoalInfo;
	
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
			AMasterQuest* AssignedQuest;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
			UQuestWidget* QuestWidget;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
			int GoalIndex;
};
