// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "QuestWidget.generated.h"

class AMasterQuest;
class USubGoalWidget;

UCLASS()
class REPLICATION_API UQuestWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	
	UFUNCTION(BlueprintCallable, Category = Quest)
	bool IsCurrentQuest();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AQuestManager* QuestManager;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		AMasterQuest* AssignedQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		TArray<USubGoalWidget*> SubGoalWidgets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		USubGoalWidget* SelectedSubGoal;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Quest)
	void OnUiUpdateQuest();
	
};
