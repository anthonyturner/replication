// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "EQuestCategories.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EQuestCategories : uint8
{
	MainQuest 	UMETA(DisplayName = "Main Quest"),
	SideQuest 	UMETA(DisplayName = "Side Quest"),
	Events	UMETA(DisplayName = "Events")
};
