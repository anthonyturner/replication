#pragma once
#include "ERegions.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ERegions : uint8
{
	Kanto 	UMETA(DisplayName = "Kanto"),
	Junto 	UMETA(DisplayName = "Junto"),
	Soho	UMETA(DisplayName = "Soho")
};
