#pragma once
#include "EGoalTypes.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EGoalTypes : uint8
{
	Custom 	UMETA(DisplayName = "Custom Goal"),
	Hunt 	UMETA(DisplayName = "Hunt"),
	Find	UMETA(DisplayName = "Find"),
	Talk 	UMETA(DisplayName = "Talk")
};
