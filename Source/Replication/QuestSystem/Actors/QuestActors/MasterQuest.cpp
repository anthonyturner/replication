// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "MasterQuest.h"
#include  "QuestSystem/Widgets/QuestWidget.h"
#include  "QuestSystem/Widgets/SubGoalWidget.h"

// Sets default values
AMasterQuest::AMasterQuest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AMasterQuest::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMasterQuest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMasterQuest::SetupStartingGoals(){

	CurrentGoalIndices.Empty();
	CurrentGoalIndices = StartingSubGoalIndices;

	UpdateSubGoals();
}

void AMasterQuest::UpdateSubGoals()
{

	CurrentGoals.Empty();
	for (int32 i = 0; i < CurrentGoalIndices.Num(); i++) {

		FGoalInfo goalInfo = QuestInfo.SubGoals[i];
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Yellow, TEXT("MasterQuest::UpdateSubGoals Unique id is ") + goalInfo.UniqueID);

		CurrentGoals.Add(goalInfo);
	}
}

bool AMasterQuest::GotoNextSubGoal()
{
	int32 indexOfMax;
	int32 theMaxIndex;
	UKismetMathLibrary::MaxOfIntArray(CurrentGoalIndices, indexOfMax, theMaxIndex);
	
	//SubGoals num -1 is relative to the Last valid index in array (Blueprint LasIndex)
	int32 nextPossibleIndex = QuestInfo.SubGoals.Num() - 1;
	//Are there any SubGoals in our array after the Max of CurrentGoalIndices
	if ( (theMaxIndex + 1) <= nextPossibleIndex) {

		NextIndex = nextPossibleIndex;
		CurrentGoalIndices.Empty(); 
		CurrentGoalIndices.Add(NextIndex);
		UpdateSubGoals();
		return true;
	}

	return false;
}


bool AMasterQuest::CompleteSubGoal(int32 SubGoalIndex) {

	const bool bHasSubGoalToComplete = CurrentGoalIndices.Contains(SubGoalIndex);
	if (bHasSubGoalToComplete)
	{

		FGoalInfo CompletedSubGoal = QuestInfo.SubGoals[SubGoalIndex];
		CompletedSubGoals.Add(CompletedSubGoal);
		CurrentGoals.Remove(CompletedSubGoal);
		//Gets the index at which the UI Widget for the subgoal is at.
		int32 SubGoalWidgetIndex = CurrentGoalIndices.Find(SubGoalIndex);
		CurrentGoalIndices.Remove(SubGoalIndex);
		USubGoalWidget* SubGoalWidget = QuestWidget->SubGoalWidgets[SubGoalWidgetIndex];
		SubGoalWidget->RemoveFromParent();
	}

	return true;
}

