// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "QuestSystem/Structures/FQuestInfo.h"
#include "QuestSystem/Structures/FGoalInfo.h"
#include "GameFramework/Actor.h"
#include "MasterQuest.generated.h"

class UQuestWidget;

UCLASS()
class REPLICATION_API AMasterQuest : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMasterQuest();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = QuestInformation)
		FQuestInfo QuestInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = QuestInformation)
		TArray<int32>StartingSubGoalIndices;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoNotTouch)
		TArray<int32>CurrentGoalIndices;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoNotTouch)
		TArray<FGoalInfo>CurrentGoals;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoNotTouch)
		TArray<FGoalInfo>CompletedSubGoals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoNotTouch)
		UQuestWidget* QuestWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoNotTouch)
		int32 CurrentHuntedAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoNotTouch)
		int32 SelectedSubGoalIndex;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DoNotTouch)
		int32 NextIndex;

	//----------------------------------------------- Functions
	
	void SetupStartingGoals();
	void UpdateSubGoals();
	bool GotoNextSubGoal();
	bool CompleteSubGoal(int32 SubGoalIndice);
};
