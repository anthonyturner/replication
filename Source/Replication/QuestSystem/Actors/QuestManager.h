// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "QuestSystem/Structures/FGoalInfo.h"
#include "QuestManager.generated.h"

class AMasterQuest;
class AGoalActor;
UCLASS()
class REPLICATION_API AQuestManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AQuestManager();

	void SetCharacterRef(AReplicationCharacter* characterRef);
	AReplicationCharacter* GetCharacterRef();

	void SetQuestMainWidgetRef(UQuestMainWidget* questWidget);
	UQuestMainWidget* GetQuestMainWidgetRef();

	UFUNCTION()
	AMasterQuest* GetCurrentQuest();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AReplicationCharacter* ReplicationCharacter;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		UQuestMainWidget* QuestMainWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		AMasterQuest* CurrentQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		TArray<AMasterQuest*>QuestActors;
	
	//meta = (AllowPrivateAccess = "true"))
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		TArray<TSubclassOf<AMasterQuest>>AllQuestClasses;

	FGoalInfo CurrrentSubGoal;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		AGoalActor* CurrentGoalActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
		TSubclassOf<AGoalActor>GoalActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Quest)
	int CurrentDistance;

	int ShowDirectionArrowAmount;
	
	UFUNCTION(BlueprintCallable, Category = Quest)
		int32 DistanceToGoal();

	UFUNCTION(BlueprintCallable, Category = Quest)
	void OnSwitchSubQuest();

	UFUNCTION(BlueprintImplementableEvent, Category = Quest)
	void OnUiSwitchSubQuest(bool hasLocation);
	
	
	UFUNCTION(BlueprintImplementableEvent, Category = Quest)
	void UICompleteAddNewQuest(bool HasNewQuest, AMasterQuest* LocalQuest);

	UFUNCTION(BlueprintCallable, Category = Quest)
	bool AddNewQuest(TSubclassOf<AMasterQuest> QuestClass, bool directlyStarted);

	
};
