// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "QuestManager.h"
#include "QuestSystem/Actors/QuestActors/MasterQuest.h"
#include "QuestSystem/Actors/GoalActor.h"
#include "QuestSystem/Widgets/QuestMainWidget.h"
#include "QuestSystem/Widgets/QuestWidget.h"

// Sets default values
AQuestManager::AQuestManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

void AQuestManager::SetCharacterRef(AReplicationCharacter* characterRef)
{
	ReplicationCharacter = characterRef;
}

AReplicationCharacter* AQuestManager::GetCharacterRef()
{
	return ReplicationCharacter;
}

void AQuestManager::SetQuestMainWidgetRef(UQuestMainWidget * questWidget)
{
	QuestMainWidget = questWidget;
}

UQuestMainWidget* AQuestManager::GetQuestMainWidgetRef(){

	return QuestMainWidget;
}

AMasterQuest * AQuestManager::GetCurrentQuest()
{
	return CurrentQuest;
}

// Called when the game starts or when spawned
void AQuestManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AQuestManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AQuestManager::OnSwitchSubQuest(){

	bool hasLocation = false;

	if (CurrentGoalActor) {

		CurrentGoalActor->Destroy();
	}

	FGoalInfo GoalInfo = CurrentQuest->QuestInfo.SubGoals[CurrentQuest->SelectedSubGoalIndex];
	CurrrentSubGoal = GoalInfo;
	hasLocation = CurrrentSubGoal.GoalLocation.HasLocation;
	if (hasLocation) {

		UWorld* world = GetWorld();
		if (world && GoalActorClass) {//Spawn a new GoalActor (of Blueprint Subclass type)

			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			CurrentGoalActor = world->SpawnActor<AGoalActor>(GoalActorClass, spawnParams);
			CurrentDistance = DistanceToGoal();
			
		}
	} else {

		//CurrentGoalActor

	}
	OnUiSwitchSubQuest(hasLocation);
	
}

bool AQuestManager::AddNewQuest(TSubclassOf<AMasterQuest> QuestClass, bool directlyStarted){

	
	TSubclassOf<AMasterQuest> LocalClass;
	AMasterQuest* LocalQuestActor;
	bool LocalStart;

	bool success = false;
	LocalClass = QuestClass;
	LocalStart = directlyStarted;

	if (UKismetSystemLibrary::IsValidClass(QuestClass) && !AllQuestClasses.Contains(QuestClass)) {

		AllQuestClasses.Add(LocalClass);
		UWorld*World = GetWorld();
		if (World) {
			FActorSpawnParameters spawnParams;
			spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			LocalQuestActor = World->SpawnActor<AMasterQuest>(LocalClass, spawnParams);
			QuestActors.Add(LocalQuestActor);
			LocalQuestActor->SetupStartingGoals();
			UQuestWidget* questWidget = QuestMainWidget->AddQuestToList(LocalQuestActor);
			LocalQuestActor->QuestWidget = questWidget;
			questWidget->OnUiUpdateQuest();
			
			bool hasQuest = (LocalStart || QuestActors.Num() <= 1);
			UICompleteAddNewQuest(hasQuest, LocalQuestActor);
			success = true;
		}
	} else {

		success = false;
	}

	return success;
}


int32 AQuestManager::DistanceToGoal() {

	FVector CharLocation = ReplicationCharacter->GetActorLocation();
	CharLocation.Z = 0.f;
	
	FVector GoalLocation = CurrentGoalActor->GetActorLocation();
	GoalLocation.Z = 0.f; //Don't want Z axis/up

	float length = (CharLocation - GoalLocation).Size();//Vector length if distance
	return UKismetMathLibrary::Round(length / 100.f);//100.f is just a value that works well

}
