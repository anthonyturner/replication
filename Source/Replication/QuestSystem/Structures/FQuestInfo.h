#pragma once

#include "QuestSystem/Enums/EQuestCategories.h"
#include "QuestSystem/Enums/ERegions.h"
#include "QuestSystem/Structures/FQuestReward.h"
#include "QuestSystem/Structures/FGoalInfo.h"
#include "FQuestInfo.generated.h"


USTRUCT(BlueprintType)
struct REPLICATION_API FQuestInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EQuestCategories Category;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ERegions Region;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FQuestReward CompletionReward;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int SuggestedLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Difficulty;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FGoalInfo> SubGoals;


	FQuestInfo() {


	}
};