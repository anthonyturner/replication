#pragma once
#include "QuestSystem/Enums/EGoalTypes.h"
#include "FGoalLocation.h"
#include "FGoalInfo.generated.h"

USTRUCT(BlueprintType)
struct REPLICATION_API FGoalInfo
{
	GENERATED_BODY()


	FString UniqueID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EGoalTypes Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isCustomGoal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText GoalText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText AdditionalName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int AmountToHunt;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FGoalLocation GoalLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool CanUpdateQuestDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText UpdatedDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int FollowingSubGoalIndices;

	//check to see if the aggro record matches another aggro record by overloading the "==" operator.
	FORCEINLINE bool operator==(const FGoalInfo& Other) const
	{
		return ( (UniqueID.Equals(Other.UniqueID)) && (Type == Other.Type) && (AdditionalName.EqualTo( Other.AdditionalName)));
	}

	FGoalInfo() {
		
		FString UniqueStrID = UBasicUtilitiesFunctionLibrary::GetStructUniqueID();
		UniqueID = UniqueStrID;
	
	}
};