#pragma once

#include "FGoalLocation.generated.h"


USTRUCT(BlueprintType)
struct REPLICATION_API FGoalLocation
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Goals")
		bool HasLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Goals")
		FVector Location;



	FGoalLocation() {


	}
};