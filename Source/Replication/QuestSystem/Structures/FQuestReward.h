// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FQuestReward.generated.h"

class ABaseItem;
USTRUCT(BlueprintType)
struct REPLICATION_API FQuestReward
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Experience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int PrestigePoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Money;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ABaseItem* item;
	
	FQuestReward() {


	}
};