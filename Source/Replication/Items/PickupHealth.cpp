// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "PickupHealth.h"


void APickupHealth::OnPickup()
{

	Super::OnPickup();

	AReplicationCharacter* player = Cast<AReplicationCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
	if (player) {

		player->AddHealth(healthAmount);

		if (ItemSound)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ItemSound, this->GetActorLocation());

		DestroyPickup();
		
	}
}
