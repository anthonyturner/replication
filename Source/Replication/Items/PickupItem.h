// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseItem.h"
#include "PickupItem.generated.h"

UCLASS()
class REPLICATION_API APickupItem : public ABaseItem
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupItem();


	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Show(bool visible) override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnPickup();

public:	
	
	UPROPERTY(EditAnywhere)
		UBoxComponent* BoxCollider;

	UFUNCTION()
	void DestroyPickup();

	UFUNCTION(Server, reliable, WithValidation)
	void ServerDestroyPickup();

	UFUNCTION(NetMulticast, reliable)
	void MulticastDestroyPickup();
		
};
