// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/PickupItem.h"
#include "PickupGoldCoin.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API APickupGoldCoin : public APickupItem
{
	GENERATED_BODY()


		virtual void OnPickup() override;
	
};
