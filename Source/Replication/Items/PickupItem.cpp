//Base class for Pickups
#include "Replication.h"
#include "PickupItem.h"


// Sets default values
APickupItem::APickupItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	this->BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	this->BoxCollider->bGenerateOverlapEvents = true;
	this->BoxCollider->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	this->BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &APickupItem::OnOverlapBegin);
	this->BoxCollider->AttachToComponent(this->RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	bReplicates = true;
}

// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
	Super::BeginPlay();
	
}



void APickupItem::Show(bool visible)
{

	Super::Show(visible);

	ECollisionEnabled::Type collision = visible ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision;
	this->ItemMesh->SetCollisionEnabled(collision);
	this->BoxCollider->SetCollisionEnabled(collision);
}

// Called every frame
void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	this->AddActorLocalRotation(this->RotationRate * DeltaTime * RotationSpeed);
	
}


void APickupItem::OnPickup()
{

	
}

void APickupItem::DestroyPickup() {

	if (Role == ROLE_Authority) {

		Destroy();
		MulticastDestroyPickup();
	}
	else {

		ServerDestroyPickup();
	}

}

void APickupItem::ServerDestroyPickup_Implementation() {

	DestroyPickup();
}


bool APickupItem::ServerDestroyPickup_Validate() {

	return true;
}


void APickupItem::MulticastDestroyPickup_Implementation() {

	Destroy();
}

void APickupItem::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if(OtherActor!= nullptr && OtherActor != this && OtherComp != nullptr){

		OnPickup();
	}
}
