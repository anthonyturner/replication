// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "InteractableItem.h"

// Sets default values
AInteractableItem::AInteractableItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	this->BoxCollider->bGenerateOverlapEvents = true;
	this->BoxCollider->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	this->BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &AInteractableItem::OnOverlapBegin);
	this->BoxCollider->AttachToComponent(this->RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}

// Called when the game starts or when spawned
void AInteractableItem::BeginPlay()
{
	Super::BeginPlay();

}

void AInteractableItem::Show(bool visible)
{

	Super::Show(visible);

	ECollisionEnabled::Type collision = visible ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision;
	this->ItemMesh->SetCollisionEnabled(collision);
	this->BoxCollider->SetCollisionEnabled(collision);
}

// Called every frame
void AInteractableItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	this->AddActorLocalRotation(this->RotationRate * DeltaTime * RotationSpeed);

}


void AInteractableItem::OnInteract()
{

	if (ItemSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ItemSound, this->GetActorLocation());

	//AReplicationCharacter* player = Cast<AReplicationCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
	//if (player) {

		/*bool hasAdded = player->AddToInventory(this);
		if (hasAdded) {//If item was added, play pickup sound and hide item.

			if (PickupSound)
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickupSound, this->GetActorLocation());

			Show(false);
		}*/
//	}
}

void AInteractableItem::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {

		OnInteract();
	}
}
