// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/PickupItem.h"
#include "PickupMana.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API APickupMana : public APickupItem
{
	GENERATED_BODY()
	
	
public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickup)
		float manaAmount;

	virtual void OnPickup() override;
	
};
