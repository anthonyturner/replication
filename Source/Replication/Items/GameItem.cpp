// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "GameItem.h"


// Sets default values
AGameItem::AGameItem()
{

	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemMesh3P"));
	Mesh3P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh3P->bReceivesDecals = false;
	//	Mesh3P->CastShadow = true;
	//Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
	//Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
	//	Mesh3P->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECR_Block);
	//	Mesh3P->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	//Mesh3P->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	RootComponent = Mesh3P;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bNetUseOwnerRelevancy = true;

}

// Called when the game starts or when spawned
void AGameItem::BeginPlay()
{
	Super::BeginPlay();

}


void AGameItem::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}


//////////////////////////////////////////////////////////////////////////
// Inventory

void AGameItem::OnEnterInventory(AReplicationCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
}

void AGameItem::OnLeaveInventory()
{
	if (Role == ROLE_Authority)
	{
		SetOwningPawn(NULL);
	}
}

//////////////////////////////////////////////////////////////////////////
// Item usage helpers

UAudioComponent* AGameItem::PlayItemSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound && MyPawn)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, MyPawn->GetRootComponent());
	}

	return AC;
}

float AGameItem::PlayItemAnimation(const FItemAnim& Animation)
{
	float Duration = 0.0f;
	if (MyPawn)
	{

		UAnimMontage* UseAnim = Animation.itemAnim;
		if (UseAnim)
		{
			Duration = MyPawn->PlayAnimMontage(UseAnim);
		}
	}

	return Duration;
}

void AGameItem::StopItemAnimation(const FItemAnim& Animation)
{
	if (MyPawn)
	{
		UAnimMontage* UseAnim = Animation.itemAnim;
		if (UseAnim)
		{
			MyPawn->StopAnimMontage(UseAnim);
		}
	}
}

void AGameItem::SetOwningPawn(AReplicationCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		Instigator = NewOwner;
		MyPawn = NewOwner;
		// net owner for RPC calls
		SetOwner(NewOwner);
	}
}

class AReplicationCharacter* AGameItem::GetPawnOwner() const
{
	return MyPawn;
}


////////////////////////////////////////////////////////////////////////////////////
// Rep notifies

void AGameItem::OnRep_MyPawn() {

	if (MyPawn)
	{
		OnEnterInventory(MyPawn);
	}
	else
	{
		OnLeaveInventory();
	}
}



void AGameItem::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGameItem, MyPawn);

}


USkeletalMeshComponent* AGameItem::GetItemMesh() const
{
	return  Mesh3P;
}


