// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseItem.h"
#include "InteractableItem.generated.h"

UCLASS()
class REPLICATION_API AInteractableItem : public ABaseItem
{
	GENERATED_BODY()
	
public:	

	AInteractableItem();

	UPROPERTY(EditAnywhere)
		UBoxComponent* BoxCollider;


	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	virtual void OnInteract();

protected:

	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Show(bool visible) override;
};
