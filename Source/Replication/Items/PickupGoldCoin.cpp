// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "PickupGoldCoin.h"

void APickupGoldCoin::OnPickup()
{

	Super::OnPickup();

	AReplicationCharacter* player = Cast<AReplicationCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
	if (player) {

		bool hasAdded;
		hasAdded = player->AddToInventory(this);

		if (hasAdded) {//If item was added, play pickup sound and hide item.

			if (ItemSound)
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), ItemSound, this->GetActorLocation());

			Show(false);
		}
	}
}
