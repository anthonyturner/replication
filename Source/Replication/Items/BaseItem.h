// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseItem.generated.h"

UCLASS()
class REPLICATION_API ABaseItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseItem();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
		FRotator RotationRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
		float RotationSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
		USceneComponent* SceneComponent;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
		UStaticMeshComponent* ItemMesh;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
		FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
		UTexture2D* Image;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
		USoundCue* ItemSound;

protected:

	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Show(bool visible);
		
};
