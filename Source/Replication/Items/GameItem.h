// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameItem.generated.h"



USTRUCT()
struct FItemData
{
	GENERATED_USTRUCT_BODY()

		
	/** failsafe reload duration if Item doesn't have any animation for it */
	UPROPERTY(EditDefaultsOnly, Category = ItemStat)
		FName ItemName;

	/** defaults */
	FItemData()
	{
		
	}
};


USTRUCT()
struct FItemAnim
{
	GENERATED_USTRUCT_BODY()

		/** animation played on pawn (3rd person view) */
		UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* itemAnim;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		float TimerHandlerDuration;

};


UCLASS()
class REPLICATION_API AGameItem : public AActor
{
	GENERATED_BODY()
	
public:	


	// Sets default values for this actor's properties
	AGameItem();	/** get pawn owner */

	UFUNCTION(BlueprintCallable, Category = "Game|Item")
		class AReplicationCharacter* GetPawnOwner() const;

	/** set the Item's owning pawn */
	void SetOwningPawn(AReplicationCharacter* ReplicationCharacter);

	/** get Item mesh (needs pawn owner to determine variant) */
	USkeletalMeshComponent* GetItemMesh() const;

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** [server] Item was added to pawn's inventory */
	virtual void OnEnterInventory(AReplicationCharacter* NewOwner);

	/** [server] Item was removed from pawn's inventory */
	virtual void OnLeaveInventory();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** perform initial setup */
	virtual void PostInitializeComponents() override;

	//////////////////////////////////////////////////////////////////////////
	// Input - server side

	/** pawn owner */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_MyPawn)
		class AReplicationCharacter* MyPawn;


	/** Item data */
	UPROPERTY(EditDefaultsOnly, Category = Config)
		FItemData ItemConfig;

	//////////////////////////////////////////////////////////////////////////
	// Replication & effects

	UFUNCTION()
	virtual void OnRep_MyPawn();

	//////////////////////////////////////////////////////////////////////////
	// Item usage

	/** [local] Item specific use implementation */
	virtual void UseItem() PURE_VIRTUAL(AGameItem::UseItem, );

	//////////////////////////////////////////////////////////////////////////
	// Item usage helpers

	/** play Item sounds */
	UAudioComponent* PlayItemSound(USoundCue* Sound);

	/** play Item animations */
	float PlayItemAnimation(const FItemAnim& Animation);

	/** stop playing Item animations */
	void StopItemAnimation(const FItemAnim& Animation);

	/** get the muzzle location of the Item */
	//FVector GetMuzzleLocation() const;
	FVector GetItemLocation() const;


	/** Item mesh: 3rd person view */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		USkeletalMeshComponent* Mesh3P;


};
