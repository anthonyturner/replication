// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/PickupItem.h"
#include "PickupHealth.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API APickupHealth : public APickupItem
{
	GENERATED_BODY()
	
	
protected:


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickup)
	float healthAmount;

	virtual void OnPickup() override;
	
};
