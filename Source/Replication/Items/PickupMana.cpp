// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "PickupMana.h"


void APickupMana::OnPickup()
{

	Super::OnPickup();

	AReplicationCharacter* player = Cast<AReplicationCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
	if (player) {

		player->AddMana(manaAmount);

		if (ItemSound)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ItemSound, this->GetActorLocation());

		DestroyPickup();
	}
}
