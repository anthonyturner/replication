// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "BaseProjectile.h"


// Sets default values
ABaseProjectile::ABaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	this->SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	this->SphereComponent->SetSphereRadius(50.f);
	this->SphereComponent->bGenerateOverlapEvents = true;
	//this->SphereComponent->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	this->SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABaseProjectile::OnOverlapBegin);
	

	this->RootComponent = SphereComponent;

	this->ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	
	//	this->ProjectileMovement->AttachToComponent(this->RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	this->FlightSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FlightSystem"));
	this->FlightSystem->AttachToComponent(this->RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	this->OnDestroyed.AddDynamic(this, &ABaseProjectile::OnProjectileDestroyed);//Bind delegate for the EventDestroyed for actors

	bReplicates = true;

}


void ABaseProjectile::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseProjectile, projectileSound);

}

// Called when the game starts or when spawned
void ABaseProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (projectileSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), projectileSound, this->GetActorLocation());
	
}

// Called every frame
void ABaseProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}




void ABaseProjectile::OnProjectileDestroyed(AActor* destroyedActor) {

	GEngine->AddOnScreenDebugMessage(1, 3, FColor::Red, "BaseProjectile.cpp: OnProjectileDestroyed destroyed");
	if (explosionParticle) {

		FTransform transform = this->GetTransform();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), explosionParticle, transform);

	}
}




void ABaseProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	// Other Actor is the actor that triggered the event. Check that is not ourself.  
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {

		ABaseCharacter* character = Cast<ABaseCharacter>(OtherActor);
		if (character != nullptr && character != Instigator) {


			// Create a damage event  
			TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
			FDamageEvent DamageEvent(ValidDamageTypeClass);
			AController* controller = character->GetController();
			if (controller) {
				character->TakeDamage(10.f, DamageEvent, controller, this);
				this->Destroy();//Triggers the event OnProjectileDestroyed

			}
			else {

				GEngine->AddOnScreenDebugMessage(1, 3, FColor::Red, "BaseProjectile.cpp: OverOverlapBegin no controller for take damage");
			}

		}
	}
}