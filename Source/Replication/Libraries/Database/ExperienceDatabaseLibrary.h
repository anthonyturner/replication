// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Data/FLevelUpData.h"

#include "ExperienceDatabaseLibrary.generated.h"


UCLASS()
class REPLICATION_API UExperienceDatabaseLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
		static int GetExpForNextLevel(UWorld* World, int Level);
	

		static FLevelUpData* GetDataForNextLevel(UWorld* World, int CurrentLevel);
	
};
