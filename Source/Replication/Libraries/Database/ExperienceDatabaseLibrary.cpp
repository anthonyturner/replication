// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "ExperienceDatabaseLibrary.h"


static const FString ContextString(TEXT("GENERAL"));


int UExperienceDatabaseLibrary::GetExpForNextLevel(UWorld* World, int CurrentLevel) {


	if (World) {
		AReplicationGameMode* RepGameMode = Cast<AReplicationGameMode>(World->GetAuthGameMode());
		if (RepGameMode) {

			// o-- Search using FindRow. It returns a handle to the row.
			// Access the variables like FLevelUpData->XPtoLvl
			UDataTable* LevelUpDataTable = RepGameMode->GetLevelUpData();
			if (LevelUpDataTable) {

				FLevelUpData*  LevelUpDataRow = LevelUpDataTable->FindRow<FLevelUpData>(*FString::Printf(TEXT("%d"), CurrentLevel), ContextString);
				if (LevelUpDataRow) {
					return LevelUpDataRow->XPtoLvl;
				}
			}
		}
	}

	return -1;

}


FLevelUpData* UExperienceDatabaseLibrary::GetDataForNextLevel(UWorld* World, int CurrentLevel) {


	if (World) {
		AReplicationGameMode* RepGameMode = Cast<AReplicationGameMode>(World->GetAuthGameMode());
		if (RepGameMode) {

			// o-- Search using FindRow. It returns a handle to the row.
			// Access the variables like FLevelUpData->XPtoLvl
			UDataTable* LevelUpDataTable = RepGameMode->GetLevelUpData();
			if (LevelUpDataTable) {

				FLevelUpData*  LevelUpDataRow = LevelUpDataTable->FindRow<FLevelUpData>(*FString::Printf(TEXT("%d"), CurrentLevel), ContextString);
				if (LevelUpDataRow) {
					return LevelUpDataRow;
				}
			}
		}
	}

	return nullptr;

}