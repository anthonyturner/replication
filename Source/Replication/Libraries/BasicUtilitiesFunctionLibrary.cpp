// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "BasicUtilitiesFunctionLibrary.h"
#include "Kismet/KismetStringLibrary.h"

static int32 StructUniqueID;


FString UBasicUtilitiesFunctionLibrary::GetStructUniqueID() {

	FString UniqueID = UKismetStringLibrary::Conv_IntToString(StructUniqueID);
	UniqueID.Append(TEXT("GoalInfo"));
	StructUniqueID++;
	

	FString StringID =  FMD5::HashAnsiString(*UniqueID);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Yellow, TEXT("UBasicUtilitiesFunctionLibrary::GetStructUniqueID() Unique id is ") + StringID);
	}

	return StringID;
}

int32 UBasicUtilitiesFunctionLibrary::GetCurrentUniqueID() {

	return StructUniqueID;
}