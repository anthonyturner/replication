// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BasicUtilitiesFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API UBasicUtilitiesFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
		static FString GetStructUniqueID();

	UFUNCTION(BlueprintCallable)
		static int32 GetCurrentUniqueID();
		
};
