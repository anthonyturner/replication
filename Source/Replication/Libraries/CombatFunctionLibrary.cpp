// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "CombatFunctionLibrary.h"
#include "SkillSystem/Elements/MasterElement.h"
#include "UI/Widgets/Common/FloatingWidget.h"

bool UCombatFunctionLibrary::IsEnemy(AActor* Actor) {

	return (Actor->GetClass() == AEnemyCharacter::StaticClass()) || (Actor->GetClass()->IsChildOf(AEnemyCharacter::StaticClass()));
}


bool UCombatFunctionLibrary::TraceSphere(AActor* ActorToIgnore, FVector Start, FVector End, float Radius, TArray<FHitResult>& OutHits, ECollisionChannel TraceChannel) {

	if (ActorToIgnore) {

		UWorld* World = ActorToIgnore->GetWorld();
		if (World) {
			
			const FName TraceTag("TraceSphereTag");
			FCollisionQueryParams TraceParams(TraceTag, true, ActorToIgnore);
			TraceParams.bTraceComplex = false;
			TraceParams.bReturnPhysicalMaterial = false;
			TraceParams.AddIgnoredActor(ActorToIgnore);

			World->DebugDrawTraceTag = TraceTag;
			return World->SweepMultiByChannel(OutHits, Start, End, FQuat(), TraceChannel, FCollisionShape::MakeSphere(Radius), TraceParams);
		}
	}
	return false;
}

bool UCombatFunctionLibrary::TraceLine(AActor* ActorToIgnore, FVector Start, FVector End, FHitResult& OutHit, ECollisionChannel TraceChannel) {

	if (ActorToIgnore) {

		UWorld* World = ActorToIgnore->GetWorld();
		if (World) {

			const FName TraceTag("TraceLineTag");
			FCollisionQueryParams TraceParams(TraceTag, true, ActorToIgnore);
			TraceParams.bTraceComplex = true;
			TraceParams.bReturnPhysicalMaterial = false;
			TraceParams.AddIgnoredActor(ActorToIgnore);

			World->DebugDrawTraceTag = TraceTag;
			
			FCollisionObjectQueryParams ObjectTypes;
			ObjectTypes.AddObjectTypesToQuery(TraceChannel);
			return World->LineTraceSingleByObjectType(OutHit, Start, End, ObjectTypes, TraceParams);
		}
	}
	return false;
}


void UCombatFunctionLibrary::ShowFloatingWidget(const FText& Msg, float value, FLinearColor ValueColor, AActor* ActorReference, TSubclassOf<UFloatingWidget> FloatingWidgetClass) {
	
	UWorld* World = ActorReference->GetWorld();
	UFloatingWidget* FloatingWidget = CreateWidget<UFloatingWidget>(World, FloatingWidgetClass);
	if (FloatingWidget) {
		FloatingWidget->SetActorReference(ActorReference);
		FloatingWidget->OnReceiveValue(Msg, value, ValueColor);
		FloatingWidget->AddToViewport();
	}
}

FLibDamage UCombatFunctionLibrary::CalculateFinalDamage(float BaseDamage, int CriticalChance, TSubclassOf<AMasterElement> AttackerElement, TSubclassOf<AMasterElement> DefenderElement) {

	float CurrentDamage = 0.f;
	bool bIsLocalCrit;
	EEffectiveness LocalEffectiveness = EEffectiveness::Effective;
	CurrentDamage = BaseDamage * FMath::RandRange(.9f, 1.1f);

	//Determine Effectiveness
	if (AttackerElement && DefenderElement) {

		AMasterElement* DefaultDefenderElement = Cast<AMasterElement>(DefenderElement->GetDefaultObject());
		if (DefaultDefenderElement->ElementInfo.Weaknesses.Contains(AttackerElement)) { //Weakness check
			LocalEffectiveness = EEffectiveness::SuperEffective;
		} else {//Resistance check

			if (DefaultDefenderElement->ElementInfo.Resistances.Contains(AttackerElement)) {

				LocalEffectiveness = EEffectiveness::NotEffective;
			}
			else {

				LocalEffectiveness = EEffectiveness::Effective;
			}
		}
	} else {

		LocalEffectiveness = EEffectiveness::Effective;
	}

	float EffectivenessMultiplier = 1.f;
	switch (LocalEffectiveness) {

	case EEffectiveness::SuperEffective:
		EffectivenessMultiplier = 1.5f;
		break;

	case EEffectiveness::Effective:
		EffectivenessMultiplier = 1.0f;
		break;

	case EEffectiveness::NotEffective:
		EffectivenessMultiplier = 0.75f;
		break;
	}

	CurrentDamage = CurrentDamage * EffectivenessMultiplier;

	//Determine critical chance
	int CritProbablity = FMath::RandRange(1, 100);
	bIsLocalCrit = (CritProbablity <= CriticalChance);

	CurrentDamage = bIsLocalCrit ? CurrentDamage * 2 : CurrentDamage * 1;

	FLibDamage DamageInfo;
	CurrentDamage = FMath::RoundToInt(CurrentDamage);
	DamageInfo.Damage = CurrentDamage;
	DamageInfo.bIsCritical = bIsLocalCrit;
	DamageInfo.Effectiveness = LocalEffectiveness;

	return DamageInfo;
}