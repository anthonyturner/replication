// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SkillSystem/Enums/EEffectiveness.h"
#include "CombatFunctionLibrary.generated.h"



USTRUCT(BlueprintType)
struct REPLICATION_API FLibDamage
{
	GENERATED_BODY()


		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsCritical;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EEffectiveness Effectiveness;

	FLibDamage() {


	}
};

UCLASS()
class REPLICATION_API UCombatFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	FLibDamage LibDamage;

public:
	UFUNCTION(BlueprintPure)
		static bool IsEnemy(AActor* Actor);

	UFUNCTION(BlueprintCallable)
		static bool TraceSphere(AActor* ActorToIgnore, FVector Start, FVector End, float Radius, TArray<FHitResult>& OutHits, ECollisionChannel TraceChannel);

	UFUNCTION(BlueprintCallable)
		static bool TraceLine(AActor* ActorToIgnore, FVector Start, FVector End, FHitResult& OutHit, ECollisionChannel TraceChannel);

	UFUNCTION(BlueprintPure)
		static FLibDamage CalculateFinalDamage(float BaseDamage, int CriticalChance, TSubclassOf<AMasterElement> AttackerElement, TSubclassOf<AMasterElement> DefenderElement);

	UFUNCTION(BlueprintCallable)
	static void ShowFloatingWidget(const FText& Msg, float value, FLinearColor ValueColor, AActor* ActorReference, TSubclassOf<UFloatingWidget> FloatingWidgetClass);
	
};
