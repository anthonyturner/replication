// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "MeleeWeapon.h"

AMeleeWeapon::AMeleeWeapon() {

}

void AMeleeWeapon::OnWeaponNotify() {

	Super::OnWeaponNotify();

	UWorld* World = GetWorld();
	if (World) {

		FVector Location = GetActorLocation();
		if (ParticleEffect) {
			UGameplayStatics::SpawnEmitterAtLocation(World, ParticleEffect, Location);
		}

		if (WeaponSound)
			UGameplayStatics::PlaySoundAtLocation(World, WeaponSound, Location);

		const FVector Start = GetActorLocation();
		FVector Orgin;
		FVector BoundsExtent;
		GetActorBounds(false, Orgin, BoundsExtent);
		const FVector End = Start + (RepPlayerReference->GetActorRotation().Vector() * GetCurrentStage().Range);
		FHitResult OutHit;

		bool HasHits = UCombatFunctionLibrary::TraceLine(this, Start, End, OutHit, ECollisionChannel::ECC_Pawn);
		if (HasHits) {

			AActor* HitActor = OutHit.GetActor();
			if (HitActor) {

				IDamageable* Damageable = Cast<IDamageable>(HitActor);
				if (Damageable) {

					FWeaponStage WeaponStage = GetCurrentStage();
					float AttackDamage = RepPlayerReference->BaseDamage * WeaponStage.Damage;
					Damageable->OnReceiveDamage(AttackDamage, WeaponStage.DamageType, WeaponInfo.Element, WeaponStage.CriticalChance, RepPlayerReference, nullptr);
				}
			}
		}
		
	}
}


//////////////////////////////////////////////////////////////////////////
// Weapon usage helpers




