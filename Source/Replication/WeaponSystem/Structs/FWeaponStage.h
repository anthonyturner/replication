#pragma once

#include "SkillSystem/Enums/EDamageType.h"
#include "FWeaponStage.generated.h"

class ASkill;
USTRUCT(BlueprintType)
struct REPLICATION_API FWeaponStage {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		TArray<ASkill*> RequiredSkills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		UTexture2D* OverrideIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		int RequiredLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float Range;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		EDamageType DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		int CriticalChance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float Cooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		UCurveFloat* CooldownCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float AttackingTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float AreaRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float MissileSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		int ManaCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float DmgPerTick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float TickInterval;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float Duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		int Accuracy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon, meta = (ToolTip = "Amount the player is slowed down when weapon is pickedup/put in inventory"))
		float EncumberAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon, meta = (ToolTip = "Amount the player is slowed down when weapon equipped"))
		float EquipEncumberAmount;

	FWeaponStage() {


	}
};
