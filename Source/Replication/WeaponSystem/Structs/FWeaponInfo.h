#pragma once

#include "FWeaponStage.h"
#include "SkillSystem/Enums/ETargetTypes.h"
#include "FWeaponInfo.generated.h"

class AMasterElement;
USTRUCT(BlueprintType)
struct REPLICATION_API FWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		FName WeaponAttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		FName WeaponUnEquipSocket;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		TSubclassOf<AMasterElement> Element;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		ETargetTypes Target;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		TArray<FWeaponStage> Stages;

	FWeaponInfo() {


	}
};
