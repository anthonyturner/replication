// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MasterWeapon.h"
#include "MeleeWeapon.generated.h"


UCLASS()
class REPLICATION_API AMeleeWeapon : public AMasterWeapon
{
	GENERATED_BODY()
	
	
public:

	AMeleeWeapon();
	virtual void  OnWeaponNotify();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float AttackTraceDistance;
	

};
