// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WeaponSystem/Structs/FWeaponInfo.h"
#include "GameFramework/Actor.h"
#include "MasterWeapon.generated.h"

class AReplicationCharacter;

class UTextBlock;
class UMaterialInstanceDynamic;
class UOverlay;

UCLASS()
class REPLICATION_API AMasterWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMasterWeapon();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Weapon)
		USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		FWeaponInfo WeaponInfo;
	
		int CurrentAnimationIndex;
		int CurrentStageIndex;
		bool bIsCoolDown;
		bool bIsCurrentlyAttacking;
		bool bIsEquipped;
		bool bPendingEquip;
		bool bPendingUnEquip;
		float EquipStartedTime;
		float EquipDuration;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon | Animations")
		TArray<UAnimMontage*>AttackAnimations;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon | Animations")
		UAnimMontage* WeaponAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon | Animations")
		UAnimMontage* EquipAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon | Animations")
		UAnimMontage* UnEquipAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		UParticleSystem* ParticleEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		USoundCue* WeaponSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		USoundCue* EquipSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		USoundCue* UnEquipSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		UMaterialInstanceDynamic* WeaponCooldownMat = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		UTextBlock* WeaponCooldownText = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		UOverlay* WeaponCooldownOverlay = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		AReplicationCharacter* RepPlayerReference;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		virtual void InitializeWeaponAttack();

	UFUNCTION(BlueprintCallable)
		virtual void OnTryAttack();

	UFUNCTION(BlueprintCallable)
		virtual void OnWeaponAttack();

	UFUNCTION(BlueprintCallable)
		virtual void OnWeaponNotify();

	UFUNCTION(BlueprintImplementableEvent)
		void OnInitComplete();

	UFUNCTION(BlueprintPure, Category = HelperFunctions)
		FWeaponStage GetCurrentStage();
	
	void ResetAttack();


	void InitializeCooldown(float cooldown);
	
	

	void ManaCheck();

	//Equip

	void OnEquip(AMasterWeapon* LastWeapon);
	void OnEquipFinished();
	void OnUnEquip();
	void OnUnEquipFinished();
	void OnEnterInventory(AReplicationCharacter* NewOwner);
	void OnLeaveInventory();

	void SheathWeapon();
	void DrawWeapon();
	void OnDrawWeaponComplete();
	void OnSheathWeaponComplete();

	bool IsAttachedToPawn() const;
	void AttachMeshToPawn(FName attachLocation, bool bIsHidden);
	void DetachMeshFromPawn(bool bIsHidden);
	UFUNCTION(BlueprintCallable)
		void OnAttackComboComplete();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	UPROPERTY()
		FTimerHandle CooldownTimerHandle;

	UPROPERTY()
		FTimerHandle WeaponAnimationHandler;

	UPROPERTY()
		FTimerHandle OnEquipFinishedHandler;

	UPROPERTY()
		FTimerHandle OnUnEquipFinishedHandler;

	UPROPERTY()
		FTimerHandle WeaponResetAnimationHandler;


	FOnTimelineFloat InterpFunction{};
	FOnTimelineEvent OnTimelineCooldownFinishedEvent;
	FOnTimelineEvent OnTimelineCooldownUpdateEvent;
	FOnTimelineFloat OnInterpFloatEvent;
	bool bIsComboEnabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		UTimelineComponent* CooldownTimelineComponent;
private:

	void OnPlayAnimation();
	
	UFUNCTION()
	void OnCooldownUpdated();

	UFUNCTION()
	void OnCooldownCompleted();

	UFUNCTION()
	void OnCooldownInterpFloat(float value);

	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
		void OnComboEnabled();


	float PlayAnimation(UAnimMontage* Animation, float InPlayRate = 1.0f);
	void PlayCooldownAnimation(float CooldownTime);
	int GetNextAnimationIndex();

	UFUNCTION()
		UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	UFUNCTION()
		void OnPlayAnimationCompleted();
};
