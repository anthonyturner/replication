// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "MasterWeapon.h"

#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/Overlay.h"

// Sets default values
AMasterWeapon::AMasterWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	WeaponMesh->bReceivesDecals = false;

	RootComponent = WeaponMesh;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	//bReplicates = true;
	//bNetUseOwnerRelevancy = true;

	CooldownTimelineComponent = CreateDefaultSubobject<UTimelineComponent>(TEXT("CooldownTimelineComponent"));

	OnTimelineCooldownFinishedEvent.BindDynamic(this, &AMasterWeapon::OnCooldownCompleted);
	OnTimelineCooldownUpdateEvent.BindDynamic(this, &AMasterWeapon::OnCooldownUpdated);
	OnInterpFloatEvent.BindUFunction(this, "OnCooldownInterpFloat");

}


// Called when the game starts or when spawned
void AMasterWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (RepPlayerReference && RepPlayerReference->SkillMainWidget) {
		WeaponCooldownMat = RepPlayerReference->SkillMainWidget->WeaponCooldownImage->GetDynamicMaterial();
		WeaponCooldownText = RepPlayerReference->SkillMainWidget->WeaponCooldownTextBlock;
		WeaponCooldownOverlay = RepPlayerReference->SkillMainWidget->WeaponCooldownStatusAreaOverlay;
	}
	else {

		UE_LOG(LogUI, Error, TEXT("AMasterWeapon::BeginPlay, RepPlayerRef && SkillMainWidget DNE - Could not create SkillMainWidget Weapon UI elements "));

	}


	FWeaponStage WeaponStage = GetCurrentStage();
	if (WeaponStage.CooldownCurve) {
		CooldownTimelineComponent->AddInterpFloat(WeaponStage.CooldownCurve, OnInterpFloatEvent, FName("Percentage_Complete"));
	}
	CooldownTimelineComponent->SetTimelineFinishedFunc(OnTimelineCooldownFinishedEvent);
	CooldownTimelineComponent->SetTimelinePostUpdateFunc(OnTimelineCooldownUpdateEvent);
}


// Called every frame
void AMasterWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//--------------------------------------------------------- Attack Behaviors --------------------------------------------------
void AMasterWeapon::OnTryAttack() {

	if (!bIsCoolDown) {

		if (RepPlayerReference->SelectedEnemy) {

			//When attacking, faces the enemy selected
			FVector ActorLocation = this->GetActorLocation();
			FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(RepPlayerReference->GetActorLocation(), RepPlayerReference->SelectedEnemy->GetActorLocation());
			const FRotator YawRotation(0, NewRotation.Yaw, 0);

			RepPlayerReference->SetActorRotation(YawRotation);
		}


		if (bIsCurrentlyAttacking) {
			bIsComboEnabled = true;
		} else {

			InitializeWeaponAttack();
		}
	}
}


void AMasterWeapon::InitializeWeaponAttack() {

	bIsCurrentlyAttacking = true;
	RepPlayerReference->BeginWeaponAttack(this);
	OnWeaponAttack();
}

void AMasterWeapon::OnWeaponAttack() {

	WeaponAnimation = AttackAnimations[CurrentAnimationIndex];
	if (WeaponAnimation) {

		RepPlayerReference->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);//Needed for Root Motion animations that move without capsule.
		float PlayRate = WeaponAnimation->SequenceLength / GetCurrentStage().AttackingTime;
		float sequenceLength = PlayAnimation(WeaponAnimation, PlayRate);
		CurrentAnimationIndex = GetNextAnimationIndex();
		GetWorld()->GetTimerManager().SetTimer(WeaponAnimationHandler, this, &AMasterWeapon::OnPlayAnimationCompleted, sequenceLength);
	}
}

void AMasterWeapon::OnComboEnabled() {

	if (bIsComboEnabled && !bIsCoolDown) {

		bIsComboEnabled = false;
		OnWeaponAttack();
	}
}

void AMasterWeapon::OnWeaponNotify() {


}


void AMasterWeapon::OnAttackComboComplete() {

	ResetAttack();
	float WeaponCooldownTime = GetCurrentStage().Cooldown;
	InitializeCooldown(WeaponCooldownTime);
}

void AMasterWeapon::ResetAttack() {

	CurrentAnimationIndex = 0;
	bIsCurrentlyAttacking = false;
	RepPlayerReference->EndWeaponAttack(this);
	bIsComboEnabled = false;
}


//--------------------------------------------------------- End Attack Behaviors --------------------------------------------------


FWeaponStage AMasterWeapon::GetCurrentStage()
{

	return WeaponInfo.Stages[CurrentStageIndex];

}




UAudioComponent* AMasterWeapon::PlayWeaponSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound && RepPlayerReference)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RepPlayerReference->GetRootComponent());
	}

	return AC;
}

//--------------------------------------------------------- Equip Behavior --------------------------------------------------

void AMasterWeapon::OnEquip(AMasterWeapon* LastWeapon)
{

	bPendingEquip = true;

	// Only play animation if last weapon is valid
	if (LastWeapon){
		
		LastWeapon->DetachMeshFromPawn(true);
		//LastWeapon->Destroy();
		//float PlayRate = EquipAnim->SequenceLength;
		//float Duration = PlayAnimation(EquipAnim, PlayRate);
		//GetWorldTimerManager().SetTimer(OnEquipFinishedHandler, this, &AMasterWeapon::OnEquipFinished, 0.338f, false);
	} 
		
	OnEquipFinished();
	

	if (RepPlayerReference && RepPlayerReference->IsLocallyControlled()) {
		PlayWeaponSound(EquipSound);
	}
}

void AMasterWeapon::OnEquipFinished()
{
	FName AttachPoint = WeaponInfo.WeaponAttachPoint;
	//DetachMeshFromPawn(true);
	AttachMeshToPawn(AttachPoint, false);
	bIsEquipped = true;
	bPendingEquip = false;
	OnPlayAnimationCompleted();
}


void AMasterWeapon::OnUnEquip()
{

	bPendingUnEquip = true;
	//float PlayRate = UnEquipAnim->SequenceLength;
	//float Duration = PlayAnimation(UnEquipAnim, PlayRate);
	//if (Duration <= 0.0f)
	//{
		// failsafe
		//Duration = 0.5f;
	//}
	//GetWorldTimerManager().SetTimer(OnUnEquipFinishedHandler, this, &AMasterWeapon::OnUnEquipFinished, 0.303f, false);
	
	OnUnEquipFinished();
	if (RepPlayerReference && RepPlayerReference->IsLocallyControlled()) {
		PlayWeaponSound(UnEquipSound);
	}
}

void AMasterWeapon::OnUnEquipFinished()
{
	
	DetachMeshFromPawn(true);
	//AttachMeshToPawn("SecondaryAttachPoint", false);
	bIsEquipped = false;
	bPendingUnEquip = false;
	bIsCurrentlyAttacking = false; //Reset to fix bug where player can't attack when unequipped weapon is reequipped due to quickly unequipping after an attack.
	OnPlayAnimationCompleted();
	if (RepPlayerReference && RepPlayerReference->IsLocallyControlled()) {
		PlayWeaponSound(UnEquipSound);
	}
}

void AMasterWeapon::SheathWeapon()
{

	bPendingUnEquip = true;
	float PlayRate = UnEquipAnim->SequenceLength;
	float Duration = PlayAnimation(UnEquipAnim, PlayRate);
	if (Duration <= 0.0f)
	{
		// failsafe
		Duration = 0.5f;
	}
	GetWorldTimerManager().SetTimer(OnUnEquipFinishedHandler, this, &AMasterWeapon::OnSheathWeaponComplete, 0.303f, false);

}

void AMasterWeapon::OnSheathWeaponComplete() {


	DetachMeshFromPawn(true);

	FName AttachPoint = WeaponInfo.WeaponUnEquipSocket;

	AttachMeshToPawn(AttachPoint, false);
	bIsEquipped = false;
	bPendingUnEquip = false;
	bIsCurrentlyAttacking = false; //Reset to fix bug where player can't attack when unequipped weapon is reequipped due to quickly unequipping after an attack.
	OnPlayAnimationCompleted();
	if (RepPlayerReference && RepPlayerReference->IsLocallyControlled()) {
		PlayWeaponSound(UnEquipSound);
	}
}

void AMasterWeapon::DrawWeapon()
{
	FName AttachPoint = WeaponInfo.WeaponAttachPoint;

	float PlayRate = EquipAnim->SequenceLength;
	float Duration = PlayAnimation(EquipAnim, PlayRate);
	GetWorldTimerManager().SetTimer(OnEquipFinishedHandler, this, &AMasterWeapon::OnDrawWeaponComplete, 0.338f, false);

}

void AMasterWeapon::OnDrawWeaponComplete() {


	FName AttachPoint = WeaponInfo.WeaponAttachPoint;
	DetachMeshFromPawn(true);
	AttachMeshToPawn(AttachPoint, false);
	bIsEquipped = true;
	bPendingEquip = false;
	OnPlayAnimationCompleted();
	if (RepPlayerReference && RepPlayerReference->IsLocallyControlled()) {
		PlayWeaponSound(EquipSound);
	}
}

//--------------------------------------------------------- End Equip Behavior --------------------------------------------------


//--------------------------------------------------------- Pawn attachments Behavior --------------------------------------------------

void AMasterWeapon::OnEnterInventory(AReplicationCharacter* NewOwner)
{
	SetOwner(NewOwner);
}

void AMasterWeapon::OnLeaveInventory()
{
	if (Role == ROLE_Authority)
	{
		SetOwner(NULL);
	}

	if (IsAttachedToPawn())
	{
		OnUnEquip();
	}
}


bool AMasterWeapon::IsAttachedToPawn() const
{
	return bIsEquipped || bPendingEquip;
}


void AMasterWeapon::AttachMeshToPawn(FName AttachPoint, bool bisHidden)
{
	if (RepPlayerReference){
		// Remove and hide both first and third person meshes
		//DetachMeshFromPawn();
		//FName AttachPoint = WeaponInfo.WeaponAttachPoint;
		USkeletalMeshComponent* UsePawnMesh = RepPlayerReference->GetMesh();
		WeaponMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, AttachPoint);
		WeaponMesh->SetHiddenInGame(bisHidden);
	}
}


void AMasterWeapon::DetachMeshFromPawn(bool isHidden)
{
	if (WeaponMesh) {

		if (RepPlayerReference) {
			USkeletalMeshComponent* UsePawnMesh = RepPlayerReference->GetMesh();
			if (UsePawnMesh) {

				WeaponMesh->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
				WeaponMesh->SetHiddenInGame(isHidden);
			//	WeaponMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::KeepRelativeTransform, FName("SecondaryAttachPoint"));
			}
		}
	}
}

//--------------------------------------------------------- End Pawn attachments Behavior --------------------------------------------------


//--------------------------------------------------------- Animations  --------------------------------------------------


float AMasterWeapon::PlayAnimation(UAnimMontage* Animation, float InPlayRate) {

	if (CurrentAnimationIndex != 0)//Hard-coded to dash animation. If not dash, because dash uses root-motion movement
		OnPlayAnimation();

	RepPlayerReference->PlayAnimMontage(Animation, InPlayRate);
	float  sequenceLength = Animation->SequenceLength / InPlayRate;
	return sequenceLength;
}

void AMasterWeapon::OnPlayAnimation() {

	RepPlayerReference->GetCharacterMovement()->DisableMovement();
	RepPlayerReference->GetCharacterMovement()->StopMovementImmediately();
}

void AMasterWeapon::OnPlayAnimationCompleted() {

	RepPlayerReference->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
}


int AMasterWeapon::GetNextAnimationIndex() {


	if ((CurrentAnimationIndex + 1) >= AttackAnimations.Num()) {

		return 0;
	}

	return (CurrentAnimationIndex + 1);
}


void AMasterWeapon::PlayCooldownAnimation(float CooldownTime) {

	CooldownTimelineComponent->SetTimelineLength(CooldownTime);
	CooldownTimelineComponent->PlayFromStart();
}

//--------------------------------------------------------- Cooldown events --------------------------------------------------

void AMasterWeapon::InitializeCooldown(float WeaponCooldownTime) {

	bIsCoolDown = true;
	WeaponCooldownOverlay->SetVisibility(ESlateVisibility::Visible);
	PlayCooldownAnimation(WeaponCooldownTime);
}

void AMasterWeapon::OnCooldownInterpFloat(float value) {


}

void AMasterWeapon::OnCooldownUpdated() {

	float CooldownPercent = (1 - (CooldownTimelineComponent->GetPlaybackPosition() / CooldownTimelineComponent->GetTimelineLength()));
	
	WeaponCooldownMat->SetScalarParameterValue(FName("Percent"), CooldownPercent);
	//Set UI cooldown text countdown value
	FText CooldownTextValue;
	float CooldownCountDownValue = CooldownTimelineComponent->GetTimelineLength() - CooldownTimelineComponent->GetPlaybackPosition();
	if (CooldownCountDownValue > 1) {//Round to either 1 or 0 digits if countdown is less than 1.0
		CooldownTextValue = UKismetTextLibrary::Conv_FloatToText(CooldownCountDownValue, ERoundingMode::HalfToEven, false, 1, 324, 0, 0);
	}
	else {

		CooldownTextValue = UKismetTextLibrary::Conv_FloatToText(CooldownCountDownValue, ERoundingMode::HalfToEven, false, 1, 324, 0, 1);
	}

	WeaponCooldownText->SetText(CooldownTextValue);
}


void AMasterWeapon::OnCooldownCompleted() {
	
	WeaponCooldownOverlay->SetVisibility(ESlateVisibility::Hidden);
	bIsCoolDown = false;	
	bIsCurrentlyAttacking = false;
}

