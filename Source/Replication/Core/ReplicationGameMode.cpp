// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*
File: ReplicationGameMode.h
Description: There are two main classes which handle information about the game being played: Game Mode and Game State.

Even the most open-ended game has an underpinning of rules, and these rules make up a Game Mode. On the most basic level, these rules include:

The number of players and spectators present, as well as the maximum number of players and spectators allowed.
How players enter the game, which can include rules for selecting spawn locations and other spawn/respawn behavior.
Whether or not the game can be paused, and how pausing the game is handled.
Transitions between levels, including whether or not the game should start in cinematic mode.

When rule-related events in the game happen and need to be tracked and shared with all players, that information is stored and synced through the Game State.

<GameModes>
While certain fundamentals, like the number of players required to play, or the method by which those players join the game,
are common to many types of games, limitless rule variations are possible depending on the specific game you are developing.
Regardless of what those rules are, Game Modes are designed to define and implement them.
There are currently two commonly-used base classes for Game Modes.

Engine version 4.14 introduces AGameModeBase,
which is the base class for all Game Modes and is a simplified and streamlined version of the classic AGameMode.
AGameMode, the Game Mode base class before version 4.14, still exists and functions as it did before, but is now a child of AGameModeBase.
AGameMode is more suited to standard game types like multiplayer shooters due to its implementation of the concept of match state.
AGameModeBase is the new default game mode included in new code projects due to its simplicity and efficiency.
*/

#include "Replication.h"
#include "Online/ReplicationPlayerState.h"
#include "Blueprint/UserWidget.h"


AReplicationGameMode::AReplicationGameMode()
{

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Pawns/PCs/BP_ReplicationCharacter"));
	DefaultPawnClass = PlayerPawnBPClass.Class;


	static ConstructorHelpers::FClassFinder<APlayerController> PlayerPawnControllerBPClass(TEXT("/Game/Blueprints/Pawns/PCs/BP_ReplicationController"));
	PlayerControllerClass = PlayerPawnControllerBPClass.Class;
	//PlayerControllerClass = AReplicationPlayerController::StaticClass();


	static ConstructorHelpers::FClassFinder<AHUD> HUDBP(TEXT("/Game/Blueprints/UI/HUD/BP_ReplicationHUD"));
	HUDClass = HUDBP.Class;
	//WidgetBlueprint'/Game/Blueprints/UI/Menu/Menu_WB.Menu_WB'
	//StartingWidgetClass = UserWidgetClass.Class;

	PlayerStateClass = AReplicationPlayerState::StaticClass();
	GameStateClass = AReplicationGameStateBase::StaticClass();


	// DataTable'/Game/Data/LevelUpInfo.LevelUpInfo'
	static ConstructorHelpers::FObjectFinder<UDataTable>LeveUpDataTable(TEXT("DataTable'/Game/Data/LevelUpInfoData.LevelUpInfoData'"));
	LevelUpDataTable = LeveUpDataTable.Object;

	
}

void AReplicationGameMode::BeginPlay()
{
	Super::BeginPlay();
}

UDataTable* AReplicationGameMode::GetLevelUpData() {

	
	return LevelUpDataTable;
}


/*
The InitGame event is called before any other scripts (including PreInitializeComponents), 
and is used by AGameModeBase to initialize parameters and spawn its helper classes.
This is called before any Actor runs PreInitializeComponents, including the Game Mode instance itself.
*/
void AReplicationGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage){

	Super::InitGame(MapName, Options, ErrorMessage);
	
}

/*
Called after a successful login. 
This is the first place it is safe to call replicated functions on the PlayerController. 
OnPostLogin can be implemented in Blueprint to add extra logic.

*/
void AReplicationGameMode::PostLogin(APlayerController * NewPlayer) {

	Super::PostLogin(NewPlayer);
	
	
}


/*

Tries to spawn the player's pawn at a specific location

*/
void AReplicationGameMode::RestartPlayerAtTransform(AController* NewPlayer, const FTransform & SpawnTransform) {
	Super::RestartPlayerAtTransform(NewPlayer, SpawnTransform);


}

/*
Called to login new players by creating a player controller, overridable by the game
*/
APlayerController* AReplicationGameMode::Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString & Portal, const FString & Options, const FUniqueNetIdRepl & UniqueId, FString & ErrorMessage) {
	return Super::Login(NewPlayer, InRemoteRole, Portal, Options, UniqueId, ErrorMessage);

}

/*
Called when a player leaves the game or is destroyed. OnLogout can be implemented to do Blueprint logic.
*/
void AReplicationGameMode::Logout(AController * Exiting) {
	Super::Logout(Exiting);

}
