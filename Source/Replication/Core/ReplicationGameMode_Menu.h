// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "ReplicationGameMode_Menu.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AReplicationGameMode_Menu : public AGameModeBase
{
	GENERATED_BODY()

		AReplicationGameMode_Menu();
public:
	
		//virtual void DrawHUD() override;
		/** Remove the current menu widget and create a new one from the specified class, if provided. */
		UFUNCTION(BlueprintCallable, Category = "UMG Game")
		void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

protected:
	/** The widget class we will use as our menu when the game starts. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Replication Game")
		TSubclassOf<UUserWidget> StartingWidgetClass;

	/** The widget instance that we are using as our menu. */
	UPROPERTY()
		UUserWidget* CurrentWidget;

	/** Called when the game starts. */
	virtual void BeginPlay() override;
};
