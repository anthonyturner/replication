// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "Player/ReplicationPlayerController_Menu.h"
#include "ReplicationGameMode_Menu.h"





AReplicationGameMode_Menu::AReplicationGameMode_Menu()
{
	
	//Set UI to show the main menu widget on start
	static ConstructorHelpers::FClassFinder<UUserWidget>UserWidgetClass(TEXT("/Game/Blueprints/UI/Widgets/Menus/Menu_WB"));
	StartingWidgetClass = UserWidgetClass.Class;

	//PlayerControllerClass = AReplicationPlayerController_Menu::StaticClass();
	
}

void AReplicationGameMode_Menu::BeginPlay()
{
	Super::BeginPlay();
}



void AReplicationGameMode_Menu::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}


	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}