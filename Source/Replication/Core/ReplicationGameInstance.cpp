// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"

UReplicationGameInstance::UReplicationGameInstance(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	/** Bind function for CREATING a Session */
	OnCreateSessionCompleteDelegate = FOnCreateSessionCompleteDelegate::CreateUObject(this, &UReplicationGameInstance::OnCreateSessionComplete);
	OnStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject(this, &UReplicationGameInstance::OnStartSessionComplete);
	OnFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject(this, &UReplicationGameInstance::OnFindSessionsComplete);
	OnJoinSessionCompleteDelegate = FOnJoinSessionCompleteDelegate::CreateUObject(this, &UReplicationGameInstance::OnJoinSessionComplete);
	OnDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &UReplicationGameInstance::OnDestroySessionComplete);

}

void UReplicationGameInstance::Init() {


}


void UReplicationGameInstance::LaunchLobby(int numPlayers, bool bIsLan, FName newServerName) {

	maxPlayers = numPlayers;
	serverName = newServerName;

	ShowLoadingScreen();
	ULocalPlayer* const Player = GetFirstGamePlayer();
	// Call our custom HostSession function.
	//HostSession(Player->GetPreferredUniqueNetId(), GameSessionName, bIsLan, true, maxPlayers);
	HostSession(Player->GetPreferredUniqueNetId(), newServerName, bIsLan, true, maxPlayers);
}

/**
*	Function to host a game!
*
*	@Param		UserID			User that started the request
*	@Param		SessionName		Name of the Session
*	@Param		bIsLAN			Is this is LAN Game?
*	@Param		bIsPresence		"Is the Session to create a presence Session"
*	@Param		MaxNumPlayers	        Number of Maximum allowed players on this "Session" (Server)
*/
bool UReplicationGameInstance::HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers)
{
	// Get the Online Subsystem to work with
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get the Session Interface, so we can call the "CreateSession" function on it
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid() && UserId.IsValid())
		{
			/*
			Fill in all the Session Settings that we want to use.

			There are more with SessionSettings.Set(...);
			For example the Map or the GameMode/Type.
			*/
			sessionSettings = MakeShareable(new FOnlineSessionSettings());
			
			sessionSettings->bIsLANMatch = bIsLAN;
			sessionSettings->bUsesPresence = bIsPresence;
			sessionSettings->NumPublicConnections = MaxNumPlayers;
			sessionSettings->NumPrivateConnections = 0;
			sessionSettings->bAllowInvites = true;
			sessionSettings->bAllowJoinInProgress = true;
			sessionSettings->bShouldAdvertise = true;
			sessionSettings->bAllowJoinViaPresence = true;
			sessionSettings->bAllowJoinViaPresenceFriendsOnly = false;
			sessionSettings->Set(SETTING_MAPNAME, FString("ReplicationLobby"), EOnlineDataAdvertisementType::ViaOnlineService);

			// Set the delegate to the Handle of the SessionInterface
			OnCreateSessionCompleteDelegateHandle = Sessions->AddOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegate);

			// Our delegate should get called when this is complete (doesn't need to be successful!)
			return Sessions->CreateSession(*UserId, SessionName, *sessionSettings);
		}
	}else{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("No OnlineSubsytem found!"));
	}

	return false;
}

/**
* Delegate fired when a session create request has completed
*
* @param SessionName the name of the session this callback is for
* @param bWasSuccessful true if the async action completed without error, false if there was an error
*/
void UReplicationGameInstance::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
{


	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, FString::Printf(TEXT("OnCreateSessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the OnlineSubsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to call the StartSession function
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the SessionComplete delegate handle, since we finished this call
			Sessions->ClearOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegateHandle);
			if (bWasSuccessful)
			{
				// Set the StartSession delegate handle
				OnStartSessionCompleteDelegateHandle = Sessions->AddOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegate);

				// Our StartSessionComplete delegate should get called after this
				Sessions->StartSession(SessionName);
			}
		}
	}
}



/**
* Delegate fired when a session start request has completed
*
* @param SessionName the name of the session this callback is for
* @param bWasSuccessful true if the async action completed without error, false if there was an error
*/
void UReplicationGameInstance::OnStartSessionComplete(FName SessionName, bool bWasSuccessful){

	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, FString::Printf(TEXT("OnStartSessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));
	// Get the Online Subsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to clear the Delegate
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			// Clear the delegate, since we are done with this call
			Sessions->ClearOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegateHandle);
		}
	}

	// If the start was successful, we can open a NewMap if we want. Make sure to use "listen" as a parameter!
	if (bWasSuccessful)
	{
		UGameplayStatics::OpenLevel(GetWorld(), "ReplicationLobby", true, "listen");
	}
}


void UReplicationGameInstance::FindOnlineGames(bool bIsLAN, bool bIsPresence)
{
	ULocalPlayer* const Player = GetFirstGamePlayer();

	FindSessions(Player->GetPreferredUniqueNetId(), bIsLAN, bIsPresence);
}

/**
*	Find an online session
*
*	@param UserId user that initiated the request
*	@param bIsLAN are we searching LAN matches
*	@param bIsPresence are we searching presence sessions
*/
void UReplicationGameInstance::FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence) {

	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get the SessionInterface from our OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			/*
			Fill in all the SearchSettings, like if we are searching for a LAN game and how many results we want to have!
			*/
			sessionSearch = MakeShareable(new FOnlineSessionSearch());

			sessionSearch->bIsLanQuery = bIsLAN;
			sessionSearch->MaxSearchResults = 20;
			sessionSearch->PingBucketSize = 50;

			// We only want to set this Query Setting if "bIsPresence" is true
			if (bIsPresence)
			{
				sessionSearch->QuerySettings.Set(SEARCH_PRESENCE, bIsPresence, EOnlineComparisonOp::Equals);
			}

			TSharedRef<FOnlineSessionSearch> SearchSettingsRef = sessionSearch.ToSharedRef();

			// Set the Delegate to the Delegate Handle of the FindSession function
			OnFindSessionsCompleteDelegateHandle = Sessions->AddOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegate);

			// Finally call the SessionInterface function. The Delegate gets called once this is finished
			Sessions->FindSessions(*UserId, SearchSettingsRef);
		}
	}
	else
	{
		// If something goes wrong, just call the Delegate Function directly with "false".
		OnFindSessionsComplete(false);
	}
}


/**
*	Delegate fired when a session search query has completed
*
*	@param bWasSuccessful true if the async action completed without error, false if there was an error
*/
void UReplicationGameInstance::OnFindSessionsComplete(bool bWasSuccessful) {

	sessionSearchList.Empty();
	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub){

		// Get SessionInterface of the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			// Clear the Delegate handle, since we finished this call
			Sessions->ClearOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegateHandle);
			
			TArray<FOnlineSessionSearchResult>SearchResults = sessionSearch->SearchResults;
			
			int32 searchResultsLength = SearchResults.Num();
			// If we have found at least 1 session, we just going to debug them. You could add them to a list of UMG Widgets, like it is done in the BP version!
			if (searchResultsLength > 0){
				
				ULocalPlayer* const Player = GetFirstGamePlayer();
				// "SessionSearch->SearchResults" is an Array that contains all the information. You can access the Session in this and get a lot of information.
				// This can be customized later on with your own classes to add more information that can be set and displayed
				for (int32 SearchIdx = 0; SearchIdx < searchResultsLength; SearchIdx++){

					// OwningUserName is just the SessionName for now. I guess you can create your own Host Settings class and GameSession Class and add a proper GameServer Name here.
					// This is something you can't do in Blueprint for example!
					FOnlineSessionSearchResult currentSessionSearch = SearchResults[SearchIdx];
					FOnlineSession session = currentSessionSearch.Session;
					
					if (session.OwningUserId != Player->GetPreferredUniqueNetId()) {
						
						FString SessionName = *session.OwningUserName;
						FString SessionId = *(session.GetSessionIdStr());
						int32 ping = currentSessionSearch.PingInMs;
						//Create session info for ui
						FOnlineSessionWrapper onlineSessionWrapper;
						onlineSessionWrapper.index = SearchIdx;//Used to retrieve FOnlineSessionSearchResult later from sessionSearch->SearchResults array
						onlineSessionWrapper.SessionId = SessionId;
						onlineSessionWrapper.SessionName = SessionName;
						onlineSessionWrapper.numPlayers = session.SessionSettings.NumPublicConnections - session.NumOpenPublicConnections;
						onlineSessionWrapper.maxPlayers = session.SessionSettings.NumPublicConnections;
						onlineSessionWrapper.ping = ping;
						sessionSearchList.Add(onlineSessionWrapper);
					}
				}
			}
			//Call Blueprint Implementable function
			UIOnFindSessionsComplete(sessionSearchList, searchResultsLength);
		}
	}
}

void UReplicationGameInstance::SetCurrentSearchResultIndex(int32 index) {

	currentSearchResultIndex = index;

}

void UReplicationGameInstance::JoinOnlineGame(int SessionSearchResultIndex) {

	FOnlineSessionSearchResult sessionSearchResult = sessionSearch->SearchResults[currentSearchResultIndex];
	JoinServer(sessionSearchResult);
}

void UReplicationGameInstance::JoinServer(const FOnlineSessionSearchResult& sessionSearchResult) {

	ULocalPlayer* const Player = GetFirstGamePlayer();
	ShowLoadingScreen();
	JoinASession(Player->GetPreferredUniqueNetId(), FName(*sessionSearchResult.Session.OwningUserName), sessionSearchResult);
}

/**
*	Joins a session via a search result
*
*	@param SessionName name of session
*	@param SearchResult Session to join
*
*	@return bool true if successful, false otherwise
*/
bool UReplicationGameInstance::JoinASession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult) {

	// Return bool
	bool bSuccessful = false;

	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			// Set the Handle again
			OnJoinSessionCompleteDelegateHandle = Sessions->AddOnJoinSessionCompleteDelegate_Handle(OnJoinSessionCompleteDelegate);

			// Call the "JoinSession" Function with the passed "SearchResult". The "SessionSearch->SearchResults" can be used to get such a
			// "FOnlineSessionSearchResult" and pass it. Pretty straight forward!
			bSuccessful = Sessions->JoinSession(*UserId, SessionName, SearchResult);
		}
	}

	return bSuccessful;
}


/**
*	Delegate fired when a session join request has completed
*
*	@param SessionName the name of the session this callback is for
*	//@param bWasSuccessful true if the async action completed without error, false if there was an error
*	@param EOnJoinSessionCompleteResult session join result, succesfull or non-successfull message types
*/
void UReplicationGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) {

	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		FString URL;
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid() && Sessions->GetResolvedConnectString(SessionName, URL))
		{
			APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (PC){
				
				PC->ClientTravel(URL, TRAVEL_Absolute);
			}
		} else {
			//UE_LOG(LogOnlineGame, Warning, TEXT("Failed to join session %s"), *SessionName.ToString());
		}
	}
#if !UE_BUILD_SHIPPING
	else
	{
		APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (PC)
		{
			FString LocalURL(TEXT("127.0.0.1"));
			PC->ClientTravel(LocalURL, TRAVEL_Absolute);
			
		}
	}
#endif //!UE_BUILD_SHIPPING

	
}

void UReplicationGameInstance::DestroySession() {

	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			Sessions->AddOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegate);

			Sessions->DestroySession(GameSessionName);
		}
	}
}

void UReplicationGameInstance::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnDestroySessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the Delegate
			Sessions->ClearOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegateHandle);

			// If it was successful, we just load another level (could be a MainMenu!)
			if (bWasSuccessful)
			{
				UGameplayStatics::OpenLevel(GetWorld(), "ReplicationMenu", true);
			}
		}
	}
}



void UReplicationGameInstance::NetworkError(EOnJoinSessionCompleteResult::Type Error) {

	switch (Error) {

	case EOnJoinSessionCompleteResult::AlreadyInSession:
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Already in Session");

		break;

	case EOnJoinSessionCompleteResult::CouldNotRetrieveAddress:
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Could not get Address");

		break;

	case EOnJoinSessionCompleteResult::SessionDoesNotExist:
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Session does not exist");

		break;

	case EOnJoinSessionCompleteResult::SessionIsFull:
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Session is full");

		break;

	default:
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red,  "Unknown error");
		break;
	}
}


void UReplicationGameInstance::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UReplicationGameInstance, maxPlayers);
	DOREPLIFETIME(UReplicationGameInstance, serverName);

}

//-----------------------------------------------------------------------------------------------------------
//UI Menus




//Shows animated loading screen
void UReplicationGameInstance::ShowLoadingScreen() {

	
	if (CurrentWidget_WB) {//Removes Server/Host widget from screen

		CurrentWidget_WB->RemoveFromParent();
	}


	if (LoadingScreenWB) {

		LoadingScreenWB->AddToViewport();

	}
	else {
		if (LoadingScreenWidgetClass) {
			LoadingScreenWB = CreateWidget<UUserWidget>(UGameplayStatics::GetPlayerController(GetWorld(), 0), LoadingScreenWidgetClass);
			if (LoadingScreenWB) {

				LoadingScreenWB->AddToViewport();

			}
		}

	}

}

void UReplicationGameInstance::ShowMultiplayerMenu() {

	if (MultiplayerMenuWB) {

		MultiplayerMenuWB->AddToViewport();

	}else {
		if (MultiplayerWidgetClass) {
			MultiplayerMenuWB = CreateWidget<UUserWidget>(UGameplayStatics::GetPlayerController(GetWorld(), 0), MultiplayerWidgetClass);
			if (MultiplayerMenuWB) {

				MultiplayerMenuWB->AddToViewport();

			}
		}
		
	}
}

void UReplicationGameInstance::ShowOptionMenu() {

	if (OptionMenuWB) {

		OptionMenuWB->AddToViewport();

	}
	else {
		if (OptionMenuWidgetClass) {
			OptionMenuWB = CreateWidget<UUserWidget>(UGameplayStatics::GetPlayerController(GetWorld(), 0), OptionMenuWidgetClass);
			if (OptionMenuWB) {

				OptionMenuWB->AddToViewport();

			}
		}
	}
}


void UReplicationGameInstance::ShowHostMenu() {

	if (HostMenuWB) {

		HostMenuWB->AddToViewport();

	}
	else {
		if (HostMenuWidgetClass) {
			APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			HostMenuWB = CreateWidget<UUserWidget>(pc, HostMenuWidgetClass);

			if (HostMenuWB) {

				HostMenuWB->AddToViewport();

			}
		}

	}
}


//Shows server menu - created each time it's called
void UReplicationGameInstance::ShowServerMenu() {


	if (ServerMenuWidgetClass) {
		APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		UUserWidget* ServerMenuWB = CreateWidget<UUserWidget>(pc, ServerMenuWidgetClass);

		if (ServerMenuWB) {

			ServerMenuWB->AddToViewport();
		}
	}

	
}



//Show main menu and enable cursor
void UReplicationGameInstance::ShowMainMenu() {

	if (MainMenuWB) {

		MainMenuWB->AddToViewport();

	}
	else {

		if (MainMenuWidgetClass) {

			APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), 0);

			MainMenuWB = CreateWidget<UUserWidget>(pc, MainMenuWidgetClass);
			pc->bShowMouseCursor = true;
			pc->SetInputMode(FInputModeGameAndUI());
			if (MainMenuWB) {

				MainMenuWB->AddToViewport();
			}
		}
	}
}