// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "GameFramework/GameModeBase.h"
#include "ReplicationGameMode.generated.h"



UCLASS(minimalapi)
class AReplicationGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AReplicationGameMode();

	

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void RestartPlayerAtTransform(AController* NewPlayer, const FTransform & SpawnTransform) override;
	virtual APlayerController* Login(UPlayer* NewPlayer, ENetRole InRemoteRole, const FString & Portal, const FString & Options, const FUniqueNetIdRepl & UniqueId, FString & ErrorMessage) override;
	virtual void Logout(AController * Exiting) override;


	UDataTable* GetLevelUpData();
protected:


	/** Called when the game starts. */
	virtual void BeginPlay() override;

private:

	UPROPERTY()
		UDataTable* LevelUpDataTable;

};



