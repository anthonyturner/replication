// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "ReplicationLobbyGameMode.h"





AReplicationLobbyGameMode::AReplicationLobbyGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Pawns/PCs/BP_ReplicationCharacter"));
	DefaultPawnClass = PlayerPawnBPClass.Class;

	static ConstructorHelpers::FClassFinder<AHUD> HUDBP(TEXT("/Game/Blueprints/UI/HUD/BP_ReplicationHUD"));
	HUDClass = HUDBP.Class;

	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/Pawns/PCs/BP_LobbyPlayerController"));
	PlayerControllerClass = PlayerControllerBPClass.Class;


}



void AReplicationLobbyGameMode::LaunchGame_Implementation() {

	//bReplicates = true;
}

void AReplicationLobbyGameMode::ServerSwapCharacter_Implementation(APlayerController* pc, class ACharacter* characterClass, bool changedStatus) {


}

bool AReplicationLobbyGameMode::ServerSwapCharacter_Validate(APlayerController* pc, class ACharacter* characterClass, bool changedStatus) {

	return true;
}

void AReplicationLobbyGameMode::ServerUpdatePlayers_Implementation() {


}

bool AReplicationLobbyGameMode::ServerUpdatePlayers_Validate() {

	return true;
}

void AReplicationLobbyGameMode::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AReplicationLobbyGameMode, spawnPointz);
}

/** Spawn the player as base character when arrive in lobby. Tells everyone to update there lobby */
void AReplicationLobbyGameMode::ServerRespawnPlayer_Implementation(APlayerController* playerController) {
	GEngine->AddOnScreenDebugMessage(-1, 8.f, FColor::Green, "Server_RespawnPlayer_Implementation");


	APawn* pawn = playerController->GetPawn();
	int randCharSelect = FMath::RandRange(0, 3);
	if (randCharSelect == 0) {

		currentCharacterClass = yasuoClass;

	}
	else if (randCharSelect == 1) {

		currentCharacterClass = warriorClass;

	}
	else {

		currentCharacterClass = barbarianClass;

	}

	///currentCharacterClass = yasuoClass;
	if (pawn && !pawn->IsPendingKill()) { //Destroy existing character and spawn again

										  //currentCharacterClass = warriorClass;
		pawn->Destroy();

	}

	UWorld* world = playerController->GetWorld();
	if (world && currentCharacterClass) {
		//spawn character
		int randPoint = FMath::RandRange(0, spawnPointz.Num() - 1);
		APlayerStart* randomSpawnPoint = spawnPointz[randPoint];
		ACharacter* spawnedActor = world->SpawnActor<ACharacter>(currentCharacterClass, randomSpawnPoint->GetActorTransform());
		APawn* spawnedPawnCharacter = Cast<APawn>(spawnedActor);
		if (spawnedPawnCharacter) {
			playerController->Possess(spawnedPawnCharacter);
			//Server_RespawnPlayer(playerController);
		}

	}
}

bool AReplicationLobbyGameMode::ServerRespawnPlayer_Validate(APlayerController* playerController) {


	return true;
}

