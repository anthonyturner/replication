// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "ReplicationLobbyGameMode.generated.h"

UCLASS()
class REPLICATION_API AReplicationLobbyGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	AReplicationLobbyGameMode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LobbyGameMode)
		bool canStartGame;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerInfo)
		TSubclassOf<ACharacter> barbarianClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerInfo)
		TSubclassOf<ACharacter> warriorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerInfo)
		TSubclassOf<ACharacter> yasuoClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerInfo)
		TSubclassOf<ACharacter> currentCharacterClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
		TArray<APlayerStart*> spawnPointz;
	
	//-------------------------------------------------------------------
	//Functions

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = LobbyGameMode)
		void LaunchGame();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerSwapCharacter(APlayerController* pc, class ACharacter* characterClass, bool changedStatus);

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerRespawnPlayer(APlayerController* playerController);

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerUpdatePlayers();

	//void Server_RespawnPlayer_Implementation(APlayerController* playerController);
//	bool Server_RespawnPlayer_Validate(APlayerController* playerController);


};
