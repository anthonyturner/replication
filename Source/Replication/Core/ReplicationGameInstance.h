// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "OnlineIdentityInterface.h"
#include "OnlineSubsystem.h"
#include "Blueprint/UserWidget.h"
#include "Online/FOnlineSessionWrapper.h"
#include "Engine/GameInstance.h"
#include "ReplicationGameInstance.generated.h"

/** Setting describing the max number of players on server */
#define SETTING_MAXPLAYERS FName(TEXT("MAXPLAYERS"))

UCLASS()
class REPLICATION_API UReplicationGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UReplicationGameInstance(const FObjectInitializer& ObjectInitializer);

		//Functions
		virtual void Init() override;

		void ShowLoadingScreen();

	UFUNCTION(BlueprintCallable, Category = Menu)
		void ShowMultiplayerMenu();

	UFUNCTION(BlueprintCallable, Category = Menu)
		void ShowOptionMenu();

	UFUNCTION(BlueprintCallable, Category = Menu)
		void ShowHostMenu();

	UFUNCTION(BlueprintCallable, Category = Menu)
		void ShowServerMenu();
	
	UFUNCTION(BlueprintCallable, Category = Menu)
		void ShowMainMenu();

	UFUNCTION()
		void LaunchLobby(int numPlayers, bool bIsLan, FName ServerName);


	void JoinServer(const FOnlineSessionSearchResult& sessionSearchResult);

	UFUNCTION(BlueprintCallable, Category = "Network|Steam")
		void DestroySession();

	UFUNCTION(BlueprintCallable, Category = "Network|Steam")
		void JoinOnlineGame(int sessionRowIndex);

	UFUNCTION(BlueprintCallable, Category = "Network|Steam")
		void FindOnlineGames(bool bIsLAN, bool bIsPresence);

	UFUNCTION(BlueprintCallable, Category = "Network|Steam")
		void SetCurrentSearchResultIndex(int32 index);

	int currentSearchResultIndex;
	bool hasChosenCharacter = false;

	bool HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers);
	void FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence);
	bool JoinASession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);

	void NetworkError(EOnJoinSessionCompleteResult::Type error );
	//variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WidgetsClass)
		TSubclassOf<UUserWidget>MultiplayerWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WidgetsClass)
		TSubclassOf<UUserWidget>LoadingScreenWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WidgetsClass)
		TSubclassOf<UUserWidget> OptionMenuWidgetClass;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WidgetsClass)
		TSubclassOf<UUserWidget> MainMenuWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WidgetsClass)
		TSubclassOf<UUserWidget> HostMenuWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WidgetsClass)
		TSubclassOf<UUserWidget> ServerMenuWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		UUserWidget* CurrentWidget_WB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		UUserWidget* MultiplayerMenuWB;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		UUserWidget* LoadingScreenWB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		UUserWidget* OptionMenuWB;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		UUserWidget* MainMenuWB;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		UUserWidget* HostMenuWB;

	UPROPERTY(EditAnywhere, Replicated, Category = ServerSettings)
	int maxPlayers;

	UPROPERTY(EditAnywhere, Replicated, Category = ServerSettings)
		FName serverName;


	TArray<FOnlineSessionWrapper> sessionSearchList;
	TSharedPtr<class FOnlineSessionSettings> sessionSettings;
	TSharedPtr<class FOnlineSessionSearch> sessionSearch;

	UFUNCTION(BlueprintImplementableEvent)
		void UIOnFindSessionsComplete(const TArray<FOnlineSessionWrapper>& SessionNames, int32 numSesssionsFound);

	/** Delegate for creating a new session */
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	/** Delegate after starting a session */
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;
	/** Delegate for destroying a session */
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;
	/** Delegate for searching for sessions */
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;
	/** Delegate after joining a session */
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;

	/** Handles to various registered delegates */
	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;

	void OnStartSessionComplete(FName InSessionName, bool bWasSuccessful);
	void OnCreateSessionComplete(FName InSessionName, bool bWasSuccessful);
	void OnFindSessionsComplete(bool bWasSuccessful);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	virtual void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

};
