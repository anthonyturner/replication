// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "ReplicationPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AReplicationPlayerState : public APlayerState
{
	GENERATED_BODY()
	
	
public:

	AReplicationPlayerState();

	virtual void ClientInitialize(AController* C) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, replicated, category = PlayerHealth)
		float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, replicated, category = PlayerHealth)
		float MaxHealth;
	
	UPROPERTY(Replicated)
		uint8 Deaths;


};
