#pragma once

#include "FOnlineSessionWrapper.generated.h"

USTRUCT(BlueprintType)
struct FOnlineSessionWrapper
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Network|Steam")
	FString SessionName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Network|Steam")
	FString SessionId;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Network|Steam")
	int32 index; //Index of the session in FOnlineSessionSearchResult

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Network|Steam")
	int32 numPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Network|Steam")
		int32 maxPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Network|Steam")
		int32 ping;

		//default constructor
		FOnlineSessionWrapper()
		{
			ping = 0;
			numPlayers = 0;
			maxPlayers = 0;
			SessionName = "";
			SessionId = "";
			index = -1;
		}

};