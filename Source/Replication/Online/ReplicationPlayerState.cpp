// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "ReplicationPlayerState.h"


AReplicationPlayerState::AReplicationPlayerState() {

	MaxHealth = 100.f;
	Health = MaxHealth;
	Deaths = 0;
}

void AReplicationPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>&OutLifetimeProps) const{

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AReplicationPlayerState, Health);
	DOREPLIFETIME(AReplicationPlayerState, MaxHealth);

	DOREPLIFETIME(AReplicationPlayerState, Deaths);
}

void AReplicationPlayerState::ClientInitialize(AController* InController) {


	Super::ClientInitialize(InController);

}