// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "ReplicationPlayerController_Menu.h"


AReplicationPlayerController_Menu::AReplicationPlayerController_Menu() {

	//static ConstructorHelpers::FClassFinder<UUserWidget>UserWidgetClass(TEXT("/Game/Blueprints/UI/Menu/Menu_WB"));
//	StartingWidgetClass = UserWidgetClass.Class;
}

void AReplicationPlayerController_Menu::BeginPlay()
{
	Super::BeginPlay();

	//ChangeMenuWidget(StartingWidgetClass);
	//GameAndUIMode.
	bShowMouseCursor = true;
	SetInputMode(FInputModeGameAndUI());

}




void AReplicationPlayerController_Menu::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}


	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}
