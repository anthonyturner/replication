// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "BaseCharacter.h"
#include "Ui/FloatingBarWidgetComponent.h"
#include "UI/Widgets/Common/FloatingBarWidget.h"
#include "UI/Widgets/Common/FloatingWidget.h"

#include "Kismet/KismetTextLibrary.h"


// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	bReplicates = true;
	bIsAlive = true;
	
	FloatingBarWidgetComponent = CreateDefaultSubobject<UFloatingBarWidgetComponent>(FName("FloatingBarWidgetComponent"));

	//Attach it to root comp
	FloatingBarWidgetComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	FloatingBarWidgetComponent->SetRelativeLocation(FVector(0, 0, 100));

	static ConstructorHelpers::FClassFinder<UUserWidget> FloatingStatBarClass(TEXT("/Game/Blueprints/UI/Widgets/Common/FloatingStatBar_WB"));
	if (FloatingStatBarClass.Succeeded()) {
		FloatingBarWidgetComponent->SetWidgetClass(FloatingStatBarClass.Class);
	}
	else {
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Red, "BaseCharacter::ABaseCharacter() FloatingStatBarClass does not exists");
	}

	static ConstructorHelpers::FClassFinder<UUserWidget>FloatingWidgetBPClass(TEXT("/Game/Blueprints/UI/Widgets/Common/FloatingText_WB"));
	if (FloatingWidgetBPClass.Succeeded()) {
		FloatingWidgetClass = FloatingWidgetBPClass.Class;
	}
	else {
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Red, "BaseCharacter::ABaseCharacter() FloatingWidgetClass does not exists");
	}

	WidgetVisibilitySphere = CreateDefaultSubobject<USphereComponent>(FName("WidgetSphere"));
	WidgetVisibilitySphere->bGenerateOverlapEvents = true;
	WidgetVisibilitySphere->SetSphereRadius(1500.f);
	WidgetVisibilitySphere->SetRelativeLocation(FVector(0, 0, 100));
	WidgetVisibilitySphere->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	
}


void ABaseCharacter::OnWidgetBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {


}

void ABaseCharacter::OnWidgetEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {


}
void ABaseCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABaseCharacter, Health);
	DOREPLIFETIME(ABaseCharacter, bIsAlive);
	DOREPLIFETIME(ABaseCharacter, CurrentExp);
	DOREPLIFETIME(ABaseCharacter, CurrentLevel);

	
}


// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	UUserWidget* UserWidget = FloatingBarWidgetComponent->GetUserWidgetObject();
	bool HasFloatingWidget = false;

	if (UserWidget) {
		
		 FloatingHealthWidget = Cast<UFloatingBarWidget>(UserWidget);
		if (FloatingHealthWidget) {
			HasFloatingWidget = true;

			UpdateFloatingTextLevel();
		}
	}

	if (!HasFloatingWidget) {

		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Red, "BaseCharacter::BeginPlay() FloatingBarWidget does not exists");
	}
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseCharacter::PostInitializeComponents() {

	Super::PostInitializeComponents();

	Super::PostInitializeComponents();

	
	SimulateRespawn();
}


void ABaseCharacter::UpdateHealth(float value) {


}

void ABaseCharacter::UpdateFloatingTextLevel() {

	FText level = UKismetTextLibrary::Conv_IntToText(CurrentLevel);
	FloatingHealthWidget->UpdateFloatingBar(Name, level, bIsAggressive);
}

//Health
float ABaseCharacter::GetCurrentHealth() const
{
	return Health;
}

void ABaseCharacter::ServerTakeDamage_Implementation(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) {

	TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

bool ABaseCharacter::ServerTakeDamage_Validate(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) {

	return true;
}

float ABaseCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) {

	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);


	return DamageAmount;
}

void ABaseCharacter::MulticastUpdateHealth_Implementation() {

	//OnUpdateHealth.Broadcast();
}


bool ABaseCharacter::IsAlive() const
{
	return bIsAlive;
}

void ABaseCharacter::Server_AddHealth_Implementation(float amount) {

	AddHealth(amount);
}

bool ABaseCharacter::Server_AddHealth_Validate(float amount) {

	return true;
}

void ABaseCharacter::AddHealth(float amount) {

	
	if (Role == ROLE_Authority) {

		Health += amount;
		if (Health <= 0.f) {
			Health = 0.f;
		}
		else if (Health > 100.f) {

			Health = 100.f;
		}
		//OnUpdateHealth.Broadcast(); //Update server's health

	} else {

		Server_AddHealth(amount);
	}
}

void ABaseCharacter::RemoveHealth_Implementation(float amount) {

	if (Role == ROLE_Authority) {

		Health -= amount;
		if (Health <= 0.f) {
			Health = 0.f;
		}
	//	OnUpdateHealth.Broadcast();//Update server's health
	} else {


		Server_RemoveHealth(amount);
	}
}

void ABaseCharacter::Server_RemoveHealth_Implementation(float amount) {

	RemoveHealth(amount);
}

bool ABaseCharacter::Server_RemoveHealth_Validate(float amount) {

	return true;
}

void ABaseCharacter::ClientSetHealth_Implementation(float amount) {

	Health = amount;
	//OnUpdateHealth.Broadcast();

}

void ABaseCharacter::OnRep_Health_Implementation() {

	//Create a string that will display the health and bomb count values
	FString NewText = FString("Health: ") + FString::SanitizeFloat(Health);

	//Set the created string to the text render comp
	if (Health <= 0) {

		if (Role == ROLE_Authority) {

			Die();
		}
		SimulateDie();
	}else {
		SimulateHit();
	}
	
	//OnUpdateHealth.Broadcast();
}

void ABaseCharacter::OnRep_IsAlive_Implementation() {
	//OnCharacterDie//
	SimulateDie();
}

void ABaseCharacter::SimulateDie() {


	if( DieAnimMontage)
		PlayAnimMontage(DieAnimMontage);

	if(DieSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), DieSound, this->GetActorLocation());

}

void ABaseCharacter::MulticastHit_Implementation() {

	SimulateHit();
}


float ABaseCharacter::SimulateHit() {

	float duration = 0.f;
	if (HitAnimMontage)
		duration = PlayAnimMontage(HitAnimMontage) - 0.50f;

	if (HitSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, this->GetActorLocation());

	return duration;

}

void ABaseCharacter::SimulateRespawn() {

	// Playable character - play respawn effects
	if (GetNetMode() != NM_DedicatedServer)
	{
		if (RespawnFX)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, RespawnFX, GetActorLocation(), GetActorRotation());
		}

		if (RespawnSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, RespawnSound, GetActorLocation());
		}
	}
}

void ABaseCharacter::Client_SimulateDie_Implementation() {

	SimulateDie();
}

void ABaseCharacter::Client_SimulateHit_Implementation() {

	SimulateHit();
	
}


void ABaseCharacter::MultiCastRagdoll_Implementation() {

	GetMesh()->SetPhysicsBlendWeight(1.0f);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->SetCollisionProfileName("Ragdoll");
	
}

void ABaseCharacter::Die() {

	
}


void ABaseCharacter::Server_Die_Implementation() {

	Die();
}

bool ABaseCharacter::Server_Die_Validate() {

	return true;
}

void ABaseCharacter::DieTimerExpired_Implementation() {

	//bIsAlive = false;
	if (bIsRespawning) {

		SetActorHiddenInGame(true);
		GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &ABaseCharacter::Respawn, RespawnDelayTime);

	} else {

		Destroy();
	}
	
	bTimerExpired = true;
}

void ABaseCharacter::OnDeath(AActor* Killer) {

	if (Role == ROLE_Authority) {
		
		StopAnimMontage();

		bIsAlive = false; //Calls rep notify for all clients to play die animation
		GetCharacterMovement()->DisableMovement();
		GetCharacterMovement()->StopMovementImmediately();

		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		Client_SimulateDie(); //Alternative method to rep notify for netmulticast/broadcasting to play die animation for all clients

		
		float dieTimeOffset = 3.f;
		float duration = 2.2f;
		//MultiCastRagdoll(); //Call for server to play die animation
	} else {

		Server_OnDeath(Killer);
	}
}


void ABaseCharacter::Server_OnDeath_Implementation(AActor* Killer) {

	OnDeath(Killer);
}


bool ABaseCharacter::Server_OnDeath_Validate(AActor* Killer) {

	return true;
}


void ABaseCharacter::OnDieTimerHandled() {


}


void ABaseCharacter::OnWaitRespawnTimerHandled() {


}

void ABaseCharacter::Respawn() {

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	bIsAlive = true;
	Health = MaxHealth;
	SimulateRespawn();

}
