// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*
Player characters derive from Replication Character
*/
#include "Replication.h"
#include "Items/PickupItem.h"

#include "Interfaces/IInteractable.h"
#include "Data/FLevelUpData.h"

#include "QuestSystem/Actors/QuestManager.h"
#include "QuestSystem/Widgets/QuestMainWidget.h"

#include "SkillSystem/Widgets/SkillMainWidget.h"
#include "SkillSystem/Widgets/SkillHotKeyWidget.h"
#include "SkillSystem/Widgets/BuffWidget.h"

#include "SkillSystem/Skills/Skill.h"
#include "SkillSystem/Elements/MasterElement.h"
#include "SkillSystem/Skills/Buffs/BuffSkill.h"

#include "UI/Widgets/GameHud.h"


#include "WeaponSystem/MasterWeapon.h"
#include"Kismet/KismetMathLibrary.h"


//////////////////////////////////////////////////////////////////////////
// AReplicationCharacter

AReplicationCharacter::AReplicationCharacter()
{
	
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the Controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the Controller

	//Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the Controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	bWantsToAttack = false;
	bWantsToRun = false;
	MaxInventorySize = 10;
	NextExpMultiplier = 1.5;
	CurrentLevel = 1;
	ExpForNextLevel = 150;
	WalkSpeed = MaxWalkSpeed;
	RunSpeed = MaxRunSpeed;

	
	LevelUpEffectParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("LevelUpEffectParticle"));
	LevelUpEffectParticle->bAutoActivate = false;
	LevelUpEffectParticle->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	LevelUpEffectParticle->SetOnlyOwnerSee(false);

	InteractCapsule = CreateDefaultSubobject<UCapsuleComponent>(FName("InteractCapsule"));
	FRotator newRotation;
	newRotation.Pitch = 90.f;
	InteractCapsule->SetRelativeRotation(newRotation);

	FVector scale3D;
	scale3D.X = 1.f;
	scale3D.Y = 4.f;
	scale3D.Z = 4.f;
	InteractCapsule->SetRelativeScale3D(scale3D);
	InteractCapsule->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	InteractCapsule->OnComponentBeginOverlap.AddDynamic(this, &AReplicationCharacter::OnBeginOverlap);
	InteractCapsule->OnComponentEndOverlap.AddDynamic(this, &AReplicationCharacter::OnEndOverlap);
	InteractCapsule->bGenerateOverlapEvents = true;
	
	
}



// Called to bind functionality to input
void AReplicationCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("MeleeAttack", IE_Pressed, this, &AReplicationCharacter::OnAttack);
	
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AReplicationCharacter::Interact);

	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &AReplicationCharacter::OnNextWeapon);
	PlayerInputComponent->BindAction("PrevWeapon", IE_Pressed, this, &AReplicationCharacter::OnPrevWeapon);

	PlayerInputComponent->BindAction("EquipWeapon", IE_Pressed, this, &AReplicationCharacter::ToggleWeapon);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("ToggleBaseStats", IE_Pressed, this, &AReplicationCharacter::ToggleBaseStats);
	PlayerInputComponent->BindAction("ToggleAllStats", IE_Pressed, this, &AReplicationCharacter::ToggleAllStats);
	
	PlayerInputComponent->BindAction("ToggleInventory", IE_Pressed, this, &AReplicationCharacter::ToggleInventory);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AReplicationCharacter::OnStartRunning);
	PlayerInputComponent->BindAction("RunToggle", IE_Pressed, this, &AReplicationCharacter::OnStartRunningToggle);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AReplicationCharacter::OnStopRunning);
	
}


//Generic
void AReplicationCharacter::BeginPlay() {

	Super::BeginPlay();
	RepPlayerController = Cast<AReplicationPlayerController>(GetController());
	BPIE_InitializeUI();//Sets up Stats bar widgets and UI references. Needed for next call to UpdateStat.
	
	int NextLevelUp = 0;
	if (CurrentLevel == 1) {

		NextLevelUp = CurrentLevel + 1;
	}
	else {

		NextLevelUp = CurrentLevel;
	}


	int32 Experience = UExperienceDatabaseLibrary::GetExpForNextLevel(World, NextLevelUp );
	if (Experience != -1) {

		ExpForNextLevel = Experience;
	}

	InitStats();
	GenerateStartingSkills();
	GenerateStartingWeapons();

	
}


void AReplicationCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	UpdatePawnMeshes();
	World = GetWorld();
	if (World) {

		CreateSkillWidget();
		CreateQuestManager();
		CreateQuestWidget();
		CreateGameHud();
		
	}
}

void AReplicationCharacter::InitStats() {

	GetStat(EStats::Health)->StatName = FString("Health");
	GetStat(EStats::Mana)->StatName = FString("Mana");
	GetStat(EStats::Experience)->StatName = FString("Exp");
	GetStat(EStats::Experience)->MaxValue = ExpForNextLevel;

	UpdateStat(EStats::Health);
	UpdateStat(EStats::Mana);
	UpdateStat(EStats::Experience);
}

void AReplicationCharacter::CreateQuestManager() {

	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	if (QuestManagerClass) {
		QuestManager = World->SpawnActor<AQuestManager>(QuestManagerClass, spawnParams);

		if (QuestManager) {
			QuestManager->SetCharacterRef(this);
		}
		else {
			UE_LOG(LogReplication, Warning, TEXT("ReplicationCharacter::BeginPlay, Could not create QuestManager "));
		}
	}
	else
	{

		UE_LOG(LogReplication, Warning, TEXT("ReplicationCharacter::BeginPlay, QuestManagerClass not set "));
	}

}


void AReplicationCharacter::CreateSkillWidget() {

	SkillMainWidget = CreateWidget<USkillMainWidget>(World, SkillMainWidgetClass);

	if (SkillMainWidget) {
		//TODO Set QuestManager in QuestMainWidget

		SkillMainWidget->SetOwningPlayer(Cast<APlayerController>(Controller));
		SkillMainWidget->SetHotKeys(HotKeys);
		SkillMainWidget->HotKeysPerRow = HotKeysPerRow;
		SkillMainWidget->AddToViewport();
	}
}

void AReplicationCharacter::CreateQuestWidget() {

	QuestMainWidget = CreateWidget<UQuestMainWidget>(World, QuestMainWidgetClass);

	if (QuestMainWidget) {

		if( QuestManager)
		QuestMainWidget->SetQuestManager(QuestManager);

		QuestMainWidget->SetOwningPlayer(Cast<APlayerController>(Controller));
		QuestManager->SetQuestMainWidgetRef(QuestMainWidget);
		QuestMainWidget->AddToViewport();
		UE_LOG(LogUI, Warning, TEXT("ReplicationCharacter::BeginPlay, createed QuestMainWidget "));
	}
}

void AReplicationCharacter::CreateGameHud() {

	if (GameHudClass)
	{
		GameHud = CreateWidget<UGameHud>(World, GameHudClass);
		GameHud->SetOwningPlayer(Cast<APlayerController>(Controller));
		GameHud->AddToViewport();
	}
}

void AReplicationCharacter::SpawnDefaultInventory() {

}

void AReplicationCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// only to local owner: weapon change requests are locally instigated, other clients don't need it
	DOREPLIFETIME_CONDITION(AReplicationCharacter, WeaponInventory, COND_OwnerOnly);
	DOREPLIFETIME(AReplicationCharacter, CurrentWeapon);
	
	DOREPLIFETIME_CONDITION(AReplicationCharacter, bWantsToRun, COND_SkipOwner);
	DOREPLIFETIME(AReplicationCharacter, Mana);
}


void AReplicationCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	RepPlayerState = Cast<AReplicationPlayerState>(PlayerState);

	if (Role == ROLE_Authority && RepPlayerState != nullptr)
	{
		RepPlayerState->Health = 100.0f;
	}
}


class AReplicationPlayerState* AReplicationCharacter::GetRepPlayerState() {

	if (RepPlayerState)
	{
		return RepPlayerState;
	}
	else
	{
		RepPlayerState = Cast<AReplicationPlayerState>(PlayerState);
		return RepPlayerState;
	}
}

void AReplicationCharacter::SetRepPlayerState(AReplicationPlayerState* newPS)
{
	// Ensure PS is valid and only set on server
	if (newPS && Role == ROLE_Authority)
	{
		RepPlayerState = newPS;
		PlayerState = newPS;
	}
}

//Player
void AReplicationCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AReplicationCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AReplicationCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
		OnPlayerMove.Broadcast();

	}
}

void AReplicationCharacter::MoveRight(float Value)
{
	if (Controller && (Value != 0.0f)){


		//Third person movement
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
		OnPlayerMove.Broadcast();

	}
}


void AReplicationCharacter::GenerateStartingSkills() {


	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = this;

	if (World) {

		for (int i = 0; i < StartingSkills.Num(); i++) {

			TSubclassOf<ASkill> SkillClass = StartingSkills[i];
			if (SkillClass) {

				ASkill* skill = World->SpawnActorDeferred<ASkill>(SkillClass, GetActorTransform(), this, this, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
				if (skill) {

					for (int j = 0; j < SkillMainWidget->AllHotKeySlots.Num(); j++) {
						///SkillMainWidget->All

						USkillHotKeyWidget* HotKeyWidget = (SkillMainWidget->AllHotKeySlots[j]);
						if (HotKeyWidget) {
							ASkill* CheckedAssignedSpell = HotKeyWidget->AssignedSpell;
							if (CheckedAssignedSpell == nullptr) {//Available slot we can use

								//CheckedAssignedSpell = skill;
								HotKeyWidget->AssignSkill(skill);
								break;
							}
						}
					}
					skill->RepPlayerReference = this;
					skill->FinishSpawning(GetActorTransform());
					skill->OnInitComplete();
				}
			}
		}
	}
}

void AReplicationCharacter::GenerateStartingWeapons() {

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = this;

	if (World) {

		for (int i = 0; i < StartingWeapons.Num(); i++) {

			TSubclassOf<AMasterWeapon> WeaponClass = StartingWeapons[i];
			
			//Spawn deferred to set Weapon references before its BeginPlay function is called
			//It needs RepPlayerReference
			AMasterWeapon* Weapon = World->SpawnActorDeferred<AMasterWeapon>(WeaponClass, GetActorTransform(), this, this, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
			if (Weapon) {
				Weapon->RepPlayerReference = this;
				Weapon->FinishSpawning(GetActorTransform());
				Weapon->OnInitComplete();
				//Weapon->SetActorHiddenInGame(true);
				AddWeapon(Weapon);
			}
		}
	}

	// equip first weapon in inventory
	if (WeaponInventory.Num() > 0){

		AMasterWeapon* weapon = FindWeapon(AMasterWeapon::StaticClass());
		if (weapon) {
			//weapon->SetActorHiddenInGame(false);
			EquipWeapon(weapon);
		}
	}
}

void AReplicationCharacter::OnReceiveDamage(float Damage, EDamageType Type, TSubclassOf<AMasterElement> AttackerElement, int CritChance, ABaseCharacter* Attacker, ASkill* Spell) {

	if (UCombatFunctionLibrary::IsEnemy(Attacker)) {

		if (CurrentWeapon) {//On being attacked, Reset attack to prevent bug for player not being able to attack after getting hit

			CurrentWeapon->ResetAttack();
			float CooldownTime = FMath::RandRange(0.f, 1.f);
			CurrentWeapon->InitializeCooldown(CooldownTime);
		}

		FLibDamage DamageInfo = UCombatFunctionLibrary::CalculateFinalDamage(Damage, CritChance, AttackerElement, Element);
		float AppliedDamage = DamageInfo.Damage* -1;
	
		FLinearColor FloatingTextColor;

		switch (Type) {

			case EDamageType::Healing:
				FloatingTextColor = FLinearColor::Yellow;
				break;

			default:
				FloatingTextColor = FLinearColor::Red;
				break;
		}

		FText FloatingMessage;
		if (DamageInfo.bIsCritical) {
			FloatingMessage = FText::FromString(("Critical Hit!"));
			if (HitCritCameraShakeClass)
				RepPlayerController->ClientPlayCameraShake(HitCritCameraShakeClass);

		}
		else {

			if (HitCameraShakeClass)
				RepPlayerController->ClientPlayCameraShake(HitCameraShakeClass);
		}

		UCombatFunctionLibrary::ShowFloatingWidget(FloatingMessage, AppliedDamage, FloatingTextColor, this, FloatingWidgetClass);

		
		UpdateHealth(AppliedDamage);
		SimulateHit();

		if (Health <= 0.f) {
			OnDeath(Attacker);
		}
	}
}


//----------------------------------------------------------------- Casting spells  ---------------------------------------------------
void AReplicationCharacter::BeginSpellCast(ASkill* CastedSpell) {

	CurrentSpell = CastedSpell;
	bIsCasting = true;
	SkillMainWidget->DisableOtherKeys(CastedSpell);
}


void AReplicationCharacter::EndSpellCast(ASkill* CastedSpell) {

	CurrentSpell = nullptr;
	bIsCasting = false;
	SkillMainWidget->EnableOtherKeys(CastedSpell);
}


//---------------------------------------------------------------- Buffs  ------------------------------------------------
bool AReplicationCharacter::HasBuff(class ABuffSkill* BuffClass) {


	for (int32 i = 0; i < CurrentBuffs.Num(); i++) {

		ABuffSkill* TempBuff = CurrentBuffs[i];
		if (TempBuff && TempBuff->GetClass()->IsChildOf(BuffClass->GetClass())) {

			return true;
		}
	}
	return false;
}

ABuffSkill* AReplicationCharacter::GetBuff(const TSubclassOf<ABuffSkill>& BuffClass) {

	ABuffSkill* CurrentBuff = nullptr;

	for (ABuffSkill* Buff : CurrentBuffs) {

		if (Buff && (Buff->GetClass() == BuffClass) || (Buff->IsA(BuffClass))) {

			CurrentBuff = Buff;
			break;
		}
	}

	return CurrentBuff;
}

UBuffWidget* AReplicationCharacter::AddBuff(ABuffSkill* Buff)
{

	if (CurrentBuffs.Contains(Buff)) {	return nullptr;	}

	CurrentBuffs.Add(Buff);
	UBuffWidget* BuffWidget = CreateWidget<UBuffWidget>(World, BuffWidgetClass);
	BuffWidget->SetImageIcon(Buff->GetBuffIcon());
	SkillMainWidget->OnAddBuffWidget(BuffWidget);

	return BuffWidget;
}

void AReplicationCharacter::RemoveBuff(ABuffSkill* Buff)
{

	if (!CurrentBuffs.Contains(Buff)) { return; }

	CurrentBuffs.Remove(Buff);
	UBuffWidget* BuffWidgetRef = Buff->GetBuffWidgetRef();
	BuffWidgetRef->RemoveFromParent();
	
}


//------------------------------------------------------------------ End Buffs  ------------------------------------------------


void AReplicationCharacter::UpdateHealth(float value) {

	ModifyStat(EStats::Health, value, false);
	float NewHealth = GetStat(EStats::Health)->CurrentValue;
	Health = FMath::Clamp(NewHealth, 0.f, MaxHealth);
}

void AReplicationCharacter::UpdatExperience(int newExp) {

	ModifyStat(EStats::Experience, newExp, false);
	CurrentExp = GetStat(EStats::Experience)->CurrentValue;

	if (CurrentExp >= ExpForNextLevel) {

		BNE_OnLevelUp();
	}
}


void AReplicationCharacter::BNE_OnLevelUp_Implementation() {

	CurrentLevel++;
	//int32 Experience = 4.f;
	FLevelUpData* LevelUpData = UExperienceDatabaseLibrary::GetDataForNextLevel(World, CurrentLevel);


	if (LevelUpData) {
		

		//Update Experience

		FStatData* ExpStatData = GetStat(EStats::Experience);

		ExpForNextLevel = LevelUpData->XPtoLvl * NextExpMultiplier;
		ExpStatData->MaxValue = ExpForNextLevel;

		//float NewExperience = ExpStatData->CurrentValue - ExpForNextLevel;
		UpdateStat(EStats::Experience);
	}
	
	BaseDamage += .1;

	//Update Health
	FStatData* HealthStatData = GetStat(EStats::Health);

	float NextLevelUpHp = LevelUpData->AdditionalHP;
	HealthStatData->MaxValue = HealthStatData->MaxValue + NextLevelUpHp;
	UpdateHealth(HealthStatData->MaxValue);
	
	
	BPIE_UpdateLevel();//Todo make this server call
	UpdateFloatingTextLevel();
	SimulateLevelUp();
}

void AReplicationCharacter::UpdateStat(EStats StatType) {

	FStatData* StatData = Stats.Find(StatType);

	float DisplayedValue = StatData->DisplayedValue;
	float MaxValue = (float)StatData->MaxValue;
	float statPercent = DisplayedValue / (MaxValue);
	statPercent = FMath::Clamp(statPercent, 0.f, 1.f);
	StatData->BarWidget->OnUpdateStatBar(statPercent);

	int32 IntDisplayedValue = UKismetMathLibrary::FTrunc(DisplayedValue);
	int32 IntMaxValue = UKismetMathLibrary::FTrunc(MaxValue);

	FString DisplayedValueText = UKismetStringLibrary::Conv_IntToString(IntDisplayedValue);
	FString MaxValueText = UKismetStringLibrary::Conv_IntToString(UKismetMathLibrary::FTrunc(IntMaxValue));

	FText value = UKismetTextLibrary::Conv_StringToText(DisplayedValueText.Append("/" + MaxValueText));
	StatData->BarWidget->OnUpdateStatValue(value);

	if (StatType == EStats::Health) {
		FloatingHealthWidget->OnUpdateHealthBar(statPercent);
	}
	
}


void AReplicationCharacter::SimulateLevelUp() {
	
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), LevelUpSound, GetActorLocation());
	LevelUpEffectParticle->Activate(true);
}

void AReplicationCharacter::ToggleBaseStats_Implementation() {

	GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, "Toogle stats");


}

void AReplicationCharacter::ToggleAllStats_Implementation() {

	GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, "Toogle stats");


}

//--------------------------------------------------------------- Mana -----------------------------------

void AReplicationCharacter::AddMana(float amount) {

	if (Role == ROLE_Authority) {

		Mana += amount;
		if (Mana <= 0.f) {
			Mana = 0.f;
		} else if (Mana > MaxMana) {

			Mana = MaxMana;
		}
		OnUpdateMana.Broadcast();
	}
	else {

		ServerAddMana(amount);
	}

}

void AReplicationCharacter::ServerAddMana_Implementation(float amount) {

	AddMana(amount);
}



bool AReplicationCharacter::ServerAddMana_Validate(float amount) {

	return true;
}


float AReplicationCharacter::GetMana() {

	return Mana;
}


//--------------------------------------------------------------- End Mana -----------------------------------

// Meshes

void AReplicationCharacter::UpdatePawnMeshes()
{

	GetMesh()->MeshComponentUpdateFlag = false ? EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered : EMeshComponentUpdateFlag::AlwaysTickPoseAndRefreshBones;
	GetMesh()->SetOwnerNoSee(false);
}

void AReplicationCharacter::ToggleInventory() {

	RepPlayerController->ToggleInventory();
	
}

bool AReplicationCharacter::AddToInventory(APickupItem * item)
{

	if (_inventory.Num() < MaxInventorySize) {
		
		_inventory.Add(item);
		UpdateInventory();
		return true;

	}

	return false;

}

void AReplicationCharacter::UpdateInventory()
{
	
	OnUpdateInventory.Broadcast(_inventory);
}


void AReplicationCharacter::AddItem(AGameItem* Item)
{
	if (Item && Role == ROLE_Authority)
	{
		Item->OnEnterInventory(this);
		Inventory.AddUnique(Item);
	}
}



AGameItem* AReplicationCharacter::FindItem(TSubclassOf<AGameItem>ItemClass)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] && Inventory[i]->IsA(ItemClass))
		{
			return Inventory[i];
		}
	}

	return NULL;
}

void AReplicationCharacter::RemoveItem(AGameItem* Item)
{
	if (Item && Role == ROLE_Authority)
	{
		Item->OnLeaveInventory();
		Inventory.RemoveSingle(Item);
	}
}


//Player and Enemy
void AReplicationCharacter::AddWeapon(AMasterWeapon* Weapon)
{
	//if (Weapon && Role == ROLE_Authority)
	//{
		Weapon->OnEnterInventory(this);
		WeaponInventory.AddUnique(Weapon);
	//}
}

//Player and Enemy
AMasterWeapon* AReplicationCharacter::FindWeapon(TSubclassOf<AMasterWeapon> WeaponClass)
{
	for (int32 i = 0; i < WeaponInventory.Num(); i++)
	{
		if (WeaponInventory[i] && WeaponInventory[i]->IsA(WeaponClass))
		{
			return Cast<AMasterWeapon>(WeaponInventory[i]);
		}
	}

	return NULL;
}


//Player and Enemy
void AReplicationCharacter::RemoveWeapon(AMasterWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		//Weapon->OnLeaveInventory();
		WeaponInventory.RemoveSingle(Weapon);
	}
}



void AReplicationCharacter::ToggleWeapon() {

	if (CurrentWeapon) {

		if (CurrentWeapon->bIsEquipped)
			SheathWeapon();
		else
			DrawWeapon();
	}
}


void AReplicationCharacter::OnUnEquipWeapon() {

	UnEquipWeapon(CurrentWeapon);
}

void AReplicationCharacter::EquipWeapon(AMasterWeapon* Weapon)
{
	if (Weapon)
	{
		if (Role == ROLE_Authority){

			Weapon->OnEquip(CurrentWeapon);
			CurrentWeapon = Weapon;
			Weapon->SetOwner(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
			bIsWeaponEquipped = true;

			WalkSpeed = MaxCombatWalkSpeed - (CurrentWeapon->GetCurrentStage()).EquipEncumberAmount;
			RunSpeed = MaxCombatRunSpeed - (CurrentWeapon->GetCurrentStage()).EquipEncumberAmount;
			GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;

		} else {
			ServerEquipWeapon(Weapon);
		}
	}
}


void AReplicationCharacter::SheathWeapon() {

	if (CurrentWeapon)
	{
		
		WalkSpeed = MaxWalkSpeed;
		RunSpeed = MaxRunSpeed;
		GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
		CurrentWeapon->SheathWeapon();
		bIsWeaponEquipped = false;
	}
}

void AReplicationCharacter::DrawWeapon() {

	if (CurrentWeapon)
	{
		
		CurrentWeapon->DrawWeapon();
		//CurrentWeapon = Weapon;
		//Weapon->SetOwner(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
		bIsWeaponEquipped = true;

		WalkSpeed = MaxCombatWalkSpeed - (CurrentWeapon->GetCurrentStage()).EquipEncumberAmount;
		RunSpeed = MaxCombatRunSpeed - (CurrentWeapon->GetCurrentStage()).EquipEncumberAmount;
		GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;

	}
}

void AReplicationCharacter::UnEquipWeapon(AMasterWeapon* Weapon)
{
	if (CurrentWeapon)
	{
		if (Role == ROLE_Authority)
		{
			
			WalkSpeed = MaxWalkSpeed;
			RunSpeed = MaxRunSpeed;
			GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
			//CurrentWeapon = Weapon;
			//Weapon->SetOwner(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
			CurrentWeapon->OnUnEquip(); 
			bIsWeaponEquipped = false;
		}
		else
		{
			ServerUnEquipWeapon(Weapon);
		}
	}
}

bool AReplicationCharacter::ServerUnEquipWeapon_Validate(AMasterWeapon* Weapon)
{
	return true;
}

void AReplicationCharacter::ServerUnEquipWeapon_Implementation(AMasterWeapon* Weapon)
{
	UnEquipWeapon(Weapon);
}

bool AReplicationCharacter::ServerEquipWeapon_Validate(AMasterWeapon* Weapon)
{
	return true;
}

void AReplicationCharacter::ServerEquipWeapon_Implementation(AMasterWeapon* Weapon)
{
	EquipWeapon(Weapon);
}

bool AReplicationCharacter::HasWeaponEquipped() const {

	return bIsWeaponEquipped;
}

//////////////////////////////////////////////////////////////////////////
// Movement

void AReplicationCharacter::SetRunning(bool bNewRunning, bool bToggle)
{
	bWantsToRun = bNewRunning;
	bWantsToRunToggled = bNewRunning && bToggle;
	
	if (Role < ROLE_Authority)
	{
		ServerSetRunning(bNewRunning, bToggle);
	}
}

bool AReplicationCharacter::ServerSetRunning_Validate(bool bNewRunning, bool bToggle)
{
	return true;
}

void AReplicationCharacter::ServerSetRunning_Implementation(bool bNewRunning, bool bToggle)
{
	SetRunning(bNewRunning, bToggle);
}


bool AReplicationCharacter::IsRunning() const
{
	if (!GetCharacterMovement())
	{
		return false;
	}

	return (bWantsToRun || bWantsToRunToggled) && !GetVelocity().IsZero() && (GetVelocity().GetSafeNormal2D() | GetActorForwardVector()) > -0.1;
}

void AReplicationCharacter::OnStartRunningToggle()
{
	APlayerController* MyPC = Cast<APlayerController>(Controller);
	if (MyPC)
	{
		SetRunning(true, true);
	}
}

void AReplicationCharacter::OnStopRunning()
{

	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	SetRunning(false, false);
}


void AReplicationCharacter::OnStartRunning()
{
	APlayerController* MyPC = Cast<APlayerController>(Controller);
	if (MyPC)
	{

		GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
		SetRunning(true, false);
	}
}


bool AReplicationCharacter::CanAttack() const
{
	return IsAlive();
}

void AReplicationCharacter::OnNextWeapon()
{
	if (!bWantsToAttack) { //Guard against switching weapons in middle of attack
		APlayerController* MyPC = Cast<APlayerController>(Controller);
		if (MyPC)
		{
			if (WeaponInventory.Num() > 1 && (CurrentWeapon != NULL))
			{

				const int32 CurrentWeaponIdx = WeaponInventory.IndexOfByKey(CurrentWeapon);
				for (int i = CurrentWeaponIdx + 1; i < WeaponInventory.Num(); i++) {

					AMasterWeapon* NextItem = WeaponInventory[i];
					if (NextItem->IsA(AMasterWeapon::StaticClass())) {
						
						EquipWeapon(Cast<AMasterWeapon>(NextItem));
						break;
					}
				}
			}
		}
	}
}

void AReplicationCharacter::OnPrevWeapon()
{

	if (!bWantsToAttack) {

		APlayerController* MyPC = Cast<APlayerController>(Controller);
		if (MyPC)
		{

			if (WeaponInventory.Num() >= 2 && (CurrentWeapon != NULL))
			{


				const int32 CurrentWeaponIdx = WeaponInventory.IndexOfByKey(CurrentWeapon);
				for (int i = CurrentWeaponIdx - 1; i >= 0; i--) {

					AMasterWeapon* NextItem = WeaponInventory[i];
					if (NextItem->IsA(AMasterWeapon::StaticClass())) {
						//UnEquipWeapon(CurrentWeapon);
						EquipWeapon(Cast<AMasterWeapon>(NextItem));
						break;
					}
				}

				//const int32 CurrentWeaponIdx = Inventory.IndexOfByKey(CurrentWeapon);
				//	AGameWeapon* PrevWeapon = Inventory[(CurrentWeaponIdx - 1 + Inventory.Num()) % Inventory.Num()];
				//	EquipWeapon(PrevWeapon);
			}
		}
	}
}



bool AReplicationCharacter::ServerAttack_Validate(const FVector pos, const FVector dir){
	if (pos != FVector(ForceInit) && dir != FVector(ForceInit))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void AReplicationCharacter::ServerAttack_Implementation(const FVector pos, const FVector dir)
{
	Attack(pos, dir);
	//MultiCastAttackEffects();
}


void AReplicationCharacter::Attack(const FVector pos, const FVector dir)
{
	//Attack(pos, dir);
	//MultiCastAttackEffects();

}

void AReplicationCharacter::MultiCastAttackEffects_Implementation() {

}


void AReplicationCharacter::OnRep_CurrentWeapon(AMasterWeapon* LastWeapon) {

	SetCurrentWeapon(CurrentWeapon, LastWeapon);
}


void AReplicationCharacter::OnRep_Mana() {

	OnUpdateMana.Broadcast();

}



void AReplicationCharacter::SetCurrentWeapon(AMasterWeapon* NewWeapon, AMasterWeapon* LastWeapon)
{
	AMasterWeapon* LocalLastWeapon = NULL;

	if (LastWeapon != NULL)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	CurrentNewWeapon = NewWeapon;
	CurrentLastWeapon = LastWeapon;

	// unequip previous
	if (LocalLastWeapon)
	{
		//LocalLastWeapon->OnUnEquip();
	}
	else {

		OnUnEquipFinished();
	}
}

void AReplicationCharacter::OnUnEquipFinished() {

	CurrentWeapon = CurrentNewWeapon;

	// equip new one
	if (CurrentWeapon)
	{
		CurrentNewWeapon->SetOwner(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!

		//CurrentNewWeapon->OnEquip(CurrentLastWeapon);
	}

}

//--------------------------------------------------------------------------------------------------------


AMasterWeapon* AReplicationCharacter::GetWeapon() const {

	return CurrentWeapon;
}

FName AReplicationCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}


bool AReplicationCharacter::IsAttacking() const
{
	return bWantsToAttack;
};


USkeletalMeshComponent* AReplicationCharacter::GetSpecifcPawnMesh(bool WantFirstPerson) const
{
	return WantFirstPerson == true ? GetMesh() : GetMesh();
}

USkeletalMeshComponent* AReplicationCharacter::GetPawnMesh() const
{
	return GetMesh();
}


//////////////////////////////////////////////////////////////////////////
// Animations

float AReplicationCharacter::PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();

	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance)
	{
		return UseMesh->AnimScriptInstance->Montage_Play(AnimMontage, InPlayRate);
	}

	return 0.0f;
}

void AReplicationCharacter::StopAnimMontage(class UAnimMontage* AnimMontage)
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance &&
		UseMesh->AnimScriptInstance->Montage_IsPlaying(AnimMontage))
	{
		UseMesh->AnimScriptInstance->Montage_Stop(AnimMontage->BlendOut.GetBlendTime(), AnimMontage);
	}
}

void AReplicationCharacter::StopAllAnimMontages()
{
	USkeletalMeshComponent* UseMesh = GetPawnMesh();
	if (UseMesh && UseMesh->AnimScriptInstance)
	{
		UseMesh->AnimScriptInstance->Montage_Stop(0.0f);
	}
}


//Stat System

void AReplicationCharacter::ModifyStat(EStats stat, int amount, bool isAnimated)
{
	
	if (!isAnimated) {

		FStatData*statData = GetStat(stat);
		statData->CurrentValue = FMath::Clamp(statData->CurrentValue + amount, statData->MinValue, statData->MaxValue);
		statData->DisplayedValue = statData->CurrentValue;
		UpdateStat(stat);

	}
}

void AReplicationCharacter::SetStat(EStats stat, FStatData value)
{

	Stats.Add(stat, value);
}

FStatData* AReplicationCharacter::GetStat(EStats stat)
{

	return Stats.Find(stat);
}




void AReplicationCharacter::Interact() {

	TArray<AActor*>interactingActors;
	InteractCapsule->GetOverlappingActors(interactingActors);
	for (int i = 0; i < interactingActors.Num(); i++) {

		AActor* overlappedActor = interactingActors[i];
		IIInteractable* interactable = Cast<IIInteractable>(overlappedActor);
		if (interactable) {

			interactable->OnInteract(this);
			break;
		}
	}
}

void AReplicationCharacter::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {


	if (OtherActor != nullptr && OtherActor != this) {
		

		IIInteractable* interactable = Cast<IIInteractable>(OtherActor);
		if (interactable) {

			interactable->OnEnterPlayerRadius(this);
		}
	}

}

void AReplicationCharacter::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	if (OtherActor != nullptr && OtherActor != this) {

		IIInteractable*interactable = Cast<IIInteractable>(OtherActor);
		if (interactable) {

			interactable->OnLeavePlayerRadius(this);
		}
		
	}
}


float AReplicationCharacter::SimulateHit() {

	float duration = Super::SimulateHit();
	//GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->StopMovementImmediately();
	GetWorld()->GetTimerManager().SetTimer(SimulateHitTimerHandle, this, &AReplicationCharacter::OnSimulateHitCompleted, duration);
	return duration;
}



void AReplicationCharacter::OnSimulateHitCompleted() {


	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	GetCharacterMovement()->SetMovementMode(MOVE_Walking);
}

void AReplicationCharacter::OnDeath(AActor* Killer) {

	Super::OnDeath(Killer);
	OnPlayerDied.Broadcast();
	OnPlayerDied.Clear();

	SkillMainWidget->DisableAllKeys();
	if (World) {

		World->GetTimerManager().SetTimer(DieTimerHandle, this, &AReplicationCharacter::OnDieTimerHandled, DeadBodyTime);
		bTimerExpired = false;
	}
}


void AReplicationCharacter::OnDieTimerHandled() {

	GetMesh()->GlobalAnimRateScale = 0.f; //Pauses animation
										  //bIsAlive = false;
	if (bIsRespawning) {
		
		GetWorld()->GetTimerManager().SetTimer(WaitRespawnTimerHandle, this, &AReplicationCharacter::OnWaitRespawnTimerHandled, WaitRespawnDelayTime);
	} else {
		Destroy();
	}
	bTimerExpired = true;
	//DieTimerExpired();
}


void AReplicationCharacter::OnWaitRespawnTimerHandled() {

	SetActorHiddenInGame(true);
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &AReplicationCharacter::Respawn, RespawnDelayTime);
}


void AReplicationCharacter::Respawn() {

	Super::Respawn();
	GetMesh()->GlobalAnimRateScale = 1.f;
	SetActorHiddenInGame(false);
	SkillMainWidget->EnableAllKeys();


	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
	GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	ModifyStat(EStats::Health, MaxHealth, false);
	ModifyStat(EStats::Mana, MaxMana, false);
	//UIUpdateHealthBar();
	UpdateStat(EStats::Health);
	UpdateStat(EStats::Mana);

}

//------------------------------------------------------------------ Weapon System -----------------------------------------------------


void AReplicationCharacter::OnAttack() {

	if (CurrentWeapon && bIsWeaponEquipped ) {

		CurrentWeapon->OnTryAttack();
	}
}

void AReplicationCharacter::BeginWeaponAttack(AMasterWeapon* AttackingWeapon){

	bIsAttacking = true;
}

void AReplicationCharacter::EndWeaponAttack(AMasterWeapon* AttackingWeapon){

	bIsAttacking = false;
}
