// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Data/FPlayerInfo.h"
#include "ReplicationLobbyPlayerController.generated.h"

class ULobbyMenu;

UCLASS()
class REPLICATION_API AReplicationLobbyPlayerController : public APlayerController
{
	GENERATED_BODY()


public:

	AReplicationLobbyPlayerController();

	void UpdateNumberOfPlayers(int currentPlayers, int MaxPlayers);
	void Kicked();

	void Client_UpdateLobbySettings_Implementation(UTexture2D * mapImage, const FText & mapName, const FText & mapTime);

	UFUNCTION(BlueprintCallable, Reliable, Client, Category = LobbyPCInfo)
		void Client_SetupLobbyMenu(FName ServerName);

	UFUNCTION(BlueprintCallable, Reliable, Client, Category = LobbyPCInfo)
		void Client_InitialSetup();

	UFUNCTION(Reliable, Client, Category = LobbyPCInfo)
		void Client_UpdateLobbySettings(UTexture2D* mapImage, const FText& mapName, const FText& mapTime);

	UFUNCTION(Reliable, Client, Category = LobbyPCInfo)
		void Client_AddPlayerInfo(const TArray<FPlayerInfo>& connectedPlayersInfo);


	UFUNCTION(Reliable, Server, WithValidation, Category = LobbyPCInfo)
		void Server_CallUpdate(FPlayerInfo playerSetings, bool changedStatus);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = LobbyPCInfo)
		void SaveGameCheck();

	UFUNCTION(BlueprintCallable, Category = LobbyPCInfo)
		void LoadGame();

	UFUNCTION(BlueprintCallable, Category = LobbyPCInfo)
		void SaveGame();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = LobbyPCInfo)
		void UISaveGame();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = LobbyPCInfo)
		void UILoadGame();
	
	
	//Properties
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LobbyPCInfo)
		bool foundSaveGame;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LobbyPCInfo)
		TSubclassOf<USaveGame> SaveGameClass;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LobbyPCInfo)
		USaveGame* playerSaveGame;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerInfo)
		FString playerSettingsSave = "playerSettingsSave";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = PlayerInfo)
		FPlayerInfo playerSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = PlayerInfo)
		TArray<FPlayerInfo> allConnectedPlayers;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		ULobbyMenu* lobbyMenuWB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		TSubclassOf<ULobbyMenu> ReplicationLobbyMenuClass;

};
