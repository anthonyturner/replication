// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "Core/ReplicationLobbyGameMode.h"
#include "ReplicationLobbyPlayerController.h"
#include "UI/Lobby/LobbyMenu.h"


AReplicationLobbyPlayerController::AReplicationLobbyPlayerController() {
	bReplicates = true;
}

void AReplicationLobbyPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AReplicationLobbyPlayerController, playerSettings);
	DOREPLIFETIME(AReplicationLobbyPlayerController, allConnectedPlayers);
}


//Update display of connected players
void AReplicationLobbyPlayerController::UpdateNumberOfPlayers(int currentPlayers, int MaxPlayers) {

	if (lobbyMenuWB) {

		//lobbyMenuWB->SetPlayersDisplay( currentPlayers + "  of "  + MaxPlayers );

	}
}

//void EndPlay(){}
//End Play function from game instance

void AReplicationLobbyPlayerController::Client_SetupLobbyMenu_Implementation(FName ServerName) {

	bShowMouseCursor = true;

	lobbyMenuWB = CreateWidget<ULobbyMenu>(this, ReplicationLobbyMenuClass);
	//Pass server name to LobbyMenu
	lobbyMenuWB->AddToViewport();
	SetInputMode(FInputModeGameAndUI());//Hide cursor during capture for InputMode
	
}

void AReplicationLobbyPlayerController::Kicked() {

	UReplicationGameInstance* gameInstance = Cast<UReplicationGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	gameInstance->ShowMainMenu();
	gameInstance->DestroySession();
}

//Check for save game info and tell server to update
void AReplicationLobbyPlayerController::Client_InitialSetup_Implementation() {

	SaveGameCheck();
	Server_CallUpdate(playerSettings, false);
}


//Update player list window - Called when player's join or change there characters
void AReplicationLobbyPlayerController::Client_AddPlayerInfo_Implementation(const TArray<FPlayerInfo>&  connectedPlayersInfo) {

	/*if (lobbyMenuWB) {

		ClearPlayerList();
		for (int i = 0; i < allConnectedPlayers.Num(); i++) {

			//Excecutes on owning CLIENT
			lobbyMenuWB->UpdatePlayerWindow(allConnectedPlayers[i]);
		}
	}*/
}


//Update lobby info on the client
void AReplicationLobbyPlayerController::Client_UpdateLobbySettings_Implementation(UTexture2D* mapImage, const FText& mapName, const FText& mapTime) {

	lobbyMenuWB->SetMapImage(mapImage);
	lobbyMenuWB->SetMapName(mapName);
	lobbyMenuWB->SetMapTime(mapTime);

	
}




void AReplicationLobbyPlayerController::SaveGameCheck_Implementation() {


	foundSaveGame = UGameplayStatics::DoesSaveGameExist(playerSettingsSave, 0);
	if (foundSaveGame) {

		LoadGame();
		SaveGame();

	}
	else {

		SaveGame();
	}
}

//Assigns the updated character selection and tells everyone to update thier lobby menus
void AReplicationLobbyPlayerController::Server_CallUpdate_Implementation(FPlayerInfo newPlayerSettings, bool changedStatus) {

	playerSettings = newPlayerSettings;
	AGameModeBase* gamemode = UGameplayStatics::GetGameMode(GetWorld());
	if (gamemode) {
		AReplicationLobbyGameMode* lobbyGamemode = Cast<AReplicationLobbyGameMode>(gamemode);
		if (lobbyGamemode) {

			lobbyGamemode->ServerSwapCharacter(this, playerSettings.playerCharacter, changedStatus);
			lobbyGamemode->ServerUpdatePlayers();
		}
	}
}



bool AReplicationLobbyPlayerController::Server_CallUpdate_Validate(FPlayerInfo newPlayerSettings, bool changedStatus) {

	return true;
}





void AReplicationLobbyPlayerController::LoadGame() {

	playerSaveGame = UGameplayStatics::LoadGameFromSlot(playerSettingsSave, 0);
	//Call Blueprints Load game because the playerInfo we need is in the Blueprint SaveGameWidget and not in C++
	UILoadGame();

}


void AReplicationLobbyPlayerController::SaveGame() {

	if (playerSaveGame) {
		//Call Blueprints Save game because playerInfo is in Blueprint SaveGameWidget and not in C++
		UISaveGame();
		//playerSaveGame->Set
	}
	else {

		if (SaveGameClass) {
			playerSaveGame = UGameplayStatics::CreateSaveGameObject(SaveGameClass);
			UISaveGame();

		}
	}

}

