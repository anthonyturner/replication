// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "BaseCharacter.h"
#include "SkillSystem/Enums/EStats.h"
#include "SkillSystem/Structs/FStatData.h"
#include "SkillSystem/Interfaces/Damageable.h"
#include "ReplicationCharacter.generated.h"


//TODO change to Multicast delegate because Dynamic is slower
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateInventoryDelegate, const TArray<APickupItem*>&, InventoryItems);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUpdateManaDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerMoveDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathDelegate);

class AReplicationPlayerController;
class APickupItem;
class ABaseProjectile;
class ABaseWeapon;
class AQuestManager;
class ASkill;
class AMasterWeapon;
class AMasterElement;
class AEnemyCharacter;
class ABuffSkill; 
class UGameHud;
class USkillMainWidget;
class UTakeDamageComponent;
class UQuestMainWidget;
class UFloatingWidget;

UCLASS(config=Game)
class AReplicationCharacter : public ABaseCharacter, public IDamageable
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	

	AReplicationCharacter();
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	virtual void PossessedBy(AController* NewController) override;

	virtual void OnDeath(AActor* Killer) override;
	virtual void Respawn() override;
	virtual void OnDieTimerHandled() override;
	virtual void OnWaitRespawnTimerHandled() override;
	virtual float SimulateHit() override;
	void OnSimulateHitCompleted();


	UFUNCTION()
	virtual void OnReceiveDamage(float Damage, EDamageType Type, TSubclassOf<AMasterElement> ElementClass, int CritChance, ABaseCharacter* Attacker, ASkill* Spell) override;


	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
		void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintImplementableEvent)
		void OnInitCompleted();

	bool bWantsToAttack;

	/** current weapon rep handler */
	UFUNCTION(BlueprintCallable)
		void OnRep_CurrentWeapon(class AMasterWeapon* LastWeapon);

	/** updates current weapon */
	void SetCurrentWeapon(class AMasterWeapon* NewWeapon, class AMasterWeapon* LastWeapon = NULL);
	void OnUnEquipFinished();

	UFUNCTION(BlueprintPure)
	bool HasWeaponEquipped() const;


	UPROPERTY(EditAnywhere, Category = "Sounds | ExperienceSystem")
		class USoundBase* LevelUpSound;

	/** Sound to play each time we attack */
	UPROPERTY(EditAnywhere, Category = Sounds)
		class USoundBase* AttackSound;

	/** Sound to play each time we get hit */
	UPROPERTY(EditAnywhere, Category = Sounds)
		class USoundBase* PainSound;


	/** particle system for 3rd person weapon attack effect (Probably belongs on weapon as weapon trail*/
	UPROPERTY(EditAnywhere, Category = VisualEffects)
		class UParticleSystemComponent* WeaponAttackParticle;

	UPROPERTY(EditAnywhere, Category = VisualEffects)
		class UParticleSystemComponent* LevelUpEffectParticle;

	UPROPERTY(EditAnywhere, Category = VisualEffects)
		class UForceFeedbackEffect* HitSuccessFeedback;

	UPROPERTY(EditAnywhere, Category = VisualEffects)
		TSubclassOf<UCameraShake>HitCameraShakeClass;

	UPROPERTY(EditAnywhere, Category = VisualEffects)
		TSubclassOf<UCameraShake>HitCritCameraShakeClass;

	void OnLevelUp_Implementation();

	void SimulateLevelUp();


	class AReplicationPlayerState* GetRepPlayerState();
	void SetRepPlayerState(class AReplicationPlayerState* newPS);


	//------------------------------------------------------------------ Experience/Quest ------------------------------------------------------
	UFUNCTION(BlueprintCallable, Category = QuestSystem)
		void UpdatExperience(int newExp);

	UFUNCTION(BlueprintNativeEvent, Category = QuestSystem)
		void BNE_OnLevelUp();

	UFUNCTION(BlueprintImplementableEvent, Category = QuestSystem)
		void BPIE_UpdateLevel();

	UPROPERTY(BlueprintReadWrite, Category = QuestSystem)
		UQuestMainWidget* QuestMainWidget;

	UPROPERTY(EditAnywhere, Category = QuestSystem)
		TSubclassOf<UQuestMainWidget> QuestMainWidgetClass;

	UPROPERTY(BlueprintReadWrite, Category = QuestSystem)
		AQuestManager* QuestManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = QuestSystem)
		TSubclassOf<AQuestManager>QuestManagerClass;

	UFUNCTION(BlueprintImplementableEvent)
		void InitUIQuestManager();

	//------------------------------------------------------------------ Weapon System -----------------------------------------------------

	void OnAttack();
	void BeginWeaponAttack(AMasterWeapon* AttackingWeapon);
	void EndWeaponAttack(AMasterWeapon* AttackingWeapon);
	void GenerateStartingWeapons();

		bool bIsAttacking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponSystem)
		TArray<TSubclassOf<AMasterWeapon>> StartingWeapons;

	UPROPERTY()
		AMasterWeapon* CurrentWeapon;
	//------------------------------------------------------------------ Stats system ------------------------------------------------------

	UPROPERTY()
		AEnemyCharacter* SelectedEnemy;

	UPROPERTY()
		AReplicationPlayerController* RepPlayerController;

	UPROPERTY()
		USkillMainWidget* SkillMainWidget;

	UPROPERTY(EditAnywhere, Category = SkillSystem)
		TSubclassOf<USkillMainWidget>SkillMainWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SkillSystem)
		TMap<EStats, FStatData> Stats;

	UFUNCTION(BlueprintImplementableEvent, Category = SKillSystem)
	void InitUIStatBar();
	
	UFUNCTION(BlueprintImplementableEvent, Category = SKillSystem)
	void UIUpdateEnemySelected(const FText& EnemyName, int Level);
	
	
	void UpdateStat(EStats stat);
	void UpdateHealth(float value);

	UFUNCTION(BlueprintCallable, Category = SkillSystem)
	void ModifyStat(EStats stat, int By, bool isAnimated);

	void SetStat(EStats stat, FStatData value);
	 
	FStatData* GetStat(EStats stat);

	UFUNCTION(BlueprintCallable, Category = SkillSystem)
		void GenerateStartingSkills();

	UFUNCTION(BlueprintImplementableEvent, Category = SkillSystem)
		void UIGenerateStartingSkills();

	void BeginSpellCast(ASkill* CastedSpell);
	void EndSpellCast(ASkill* CastedSpell);

	UFUNCTION(BlueprintPure, Category = SkillSystem)
		bool HasBuff(class ABuffSkill* BuffClass);
	
	UFUNCTION(BlueprintCallable, Category = SkillSystem)
		ABuffSkill* GetBuff(const TSubclassOf<ABuffSkill>& Buff);

	UFUNCTION(BlueprintCallable, Category = SkillSystem)
		UBuffWidget* AddBuff(ABuffSkill* Buff);

	UFUNCTION(BlueprintCallable, Category = SkillSystem)
		void RemoveBuff(ABuffSkill* Buff);

	//ABuffSkill* Current

	 UPROPERTY()
	 int HotKeysPerRow;

	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SkillSystem)
		 TArray<FKey> HotKeys;

	 UPROPERTY()
		 bool bIsCasting;

	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SkillSystem)
		 TArray<TSubclassOf<ASkill>> StartingSkills;


	 UPROPERTY(EditAnywhere, Category = SkillSystem)
		 TSubclassOf<UBuffWidget> BuffWidgetClass;


	 UPROPERTY()
		 TArray<ABuffSkill*> CurrentBuffs;

	 UPROPERTY()
		 ASkill* CurrentSpell;

	//----------------------------------------------------------------- Interaction ------------------------------------------------------
	UFUNCTION()
	void Interact();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interaction)
		UCapsuleComponent* InteractCapsule;


	//------------------------------------------------------------------ Mana ------------------------------------------------------
	UPROPERTY(EditAnywhere, BlueprintReadOnly, replicated, ReplicatedUsing = OnRep_Mana, Category = Stats)
	float Mana;

	UFUNCTION()
		void OnRep_Mana();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
	float MaxMana;

	UFUNCTION()
	void AddMana(float amount);

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
	void ServerAddMana(float amount);

	UFUNCTION(BlueprintCallable)
	float GetMana();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;


	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }


	/** handle mesh visibility and updates */
	void UpdatePawnMeshes();

	/********************************************************* inventory list ***********************************************************************************/
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
		TArray<TSubclassOf<class AGameItem> > DefaultInventoryClasses;

	/** [server] spawns default inventory */
	void SpawnDefaultInventory();

	/**
	* get weapon from inventory at index. Index validity is not checked.
	*
	* @param Index Inventory index
	*/
	class AMasterWeapon* GetInventoryWeapon(int32 index) const;
	class AGameItem* GetInventoryItem(int32 index) const;

	/** get total number of inventory items */
	int32 GetInventoryCount() const;

	UFUNCTION(BlueprintCallable)
		void UpdateInventory();

	UPROPERTY(BlueprintAssignable, Category = Pickup)
		FUpdateInventoryDelegate OnUpdateInventory;

	UPROPERTY(BlueprintAssignable, Category = Stats)
		FUpdateManaDelegate OnUpdateMana;


	UPROPERTY(BlueprintAssignable)
		FOnDeathDelegate OnPlayerDied;

	UPROPERTY(BlueprintAssignable, Category = Movement)
		FOnPlayerMoveDelegate OnPlayerMove;

	bool AddToInventory(APickupItem* item);

	UFUNCTION(BlueprintCallable)
		void ToggleInventory();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Inventory)
		int MaxInventorySize;

	void OnStopAttack();


	/** player pressed run action */
	void OnStartRunning();

	/** player pressed toggled run action */
	void OnStartRunningToggle();

	/** player released run action */
	void OnStopRunning();

	/** player pressed next weapon action */
	void OnNextWeapon();
	/** player pressed prev weapon action */
	void OnPrevWeapon();

	void ToggleWeapon();
	void SheathWeapon();
	void DrawWeapon();

	void OnUnEquipWeapon();

	//////////////////////////////////////////////////////////////////////////
	// Animations

	/** play anim montage */
	virtual float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) override;

	/** stop playing montage */
	virtual void StopAnimMontage(class UAnimMontage* AnimMontage) override;

	/** stop playing all montages */
	void StopAllAnimMontages();

	//////////////////////////////////////////////////////////////////////////
	// Reading data




	/** get currently equipped weapon */

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
		class AMasterWeapon* GetWeapon() const;

	/** get weapon attach point */
	FName GetWeaponAttachPoint() const;



	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
		FName WeaponAttachPoint;

	/** get attacking state */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
		bool IsAttacking() const;


	/** get mesh component */
	USkeletalMeshComponent* GetPawnMesh() const;

	/*
	* Get either first or third person mesh.
	*
	* @param	WantFirstPerson		If true returns the first peron mesh, else returns the third
	*/
	USkeletalMeshComponent* GetSpecifcPawnMesh(bool WantFirstPerson) const;


	bool CanAttack() const;

	/** equip weapon */
	UFUNCTION(reliable, server, WithValidation)
		void ServerEquipWeapon(class AMasterWeapon* NewWeapon);

	/** unequip weapon */
	UFUNCTION(reliable, server, WithValidation)
		void ServerUnEquipWeapon(class AMasterWeapon* NewWeapon);

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/**
	* [server] add weapon to inventory
	*
	* @param Weapon	Weapon to add.
	*/
	void AddWeapon(class AMasterWeapon* Weapon);
	void AddItem(class AGameItem* Item);
	/**
	* [server] remove weapon from inventory
	*
	* @param Weapon	Weapon to remove.
	*/
	void RemoveWeapon(class AMasterWeapon* Weapon);
	void RemoveItem(class AGameItem* Item);

	/** Find in inventory
	*
	* @param WeaponClass	Class of weapon to find.
	*/
	class AMasterWeapon* FindWeapon(TSubclassOf<class AMasterWeapon> WeaponClass);
	class AGameItem* FindItem(TSubclassOf<class AGameItem> ItemClass);
	/**
	* [server + local] equips weapon from inventory
	*
	* @param Weapon	Weapon to equip
	*/
	void EquipWeapon(class AMasterWeapon* Weapon);

	void UnEquipWeapon(class AMasterWeapon* Weapon);



protected:


	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	UFUNCTION(BlueprintNativeEvent, Category = UI)
		void ToggleBaseStats();

	UFUNCTION(BlueprintNativeEvent, Category = UI)
		void ToggleAllStats();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	/** current running state */
	UPROPERTY(Transient, Replicated)
	bool bWantsToRun;

	bool bWantsToRunToggled;

	/** update targeting state */
	UFUNCTION(reliable, server, WithValidation)
		void ServerSetRunning(bool bNewRunning, bool bToggle);


	/** [server + local] change running state */
	void SetRunning(bool bNewRunning, bool bToggle);

	/** get running state */
	UFUNCTION(BlueprintCallable, Category = Pawn)
		bool IsRunning() const;

	

	/** REMOTE PROCEDURE CALLS */
private:
	// Perform attack action on the server
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerAttack(const FVector pos, const FVector dir);

	void Attack(const FVector pos, const FVector dir);

	// Multicast so all clients run attack effects
	UFUNCTION(NetMultiCast, unreliable)
		void MultiCastAttackEffects();

	void CreateQuestWidget();
	void CreateSkillWidget();
	void CreateQuestManager();
	void CreateGameHud();
	
	void InitStats();

	UPROPERTY(BlueprintReadOnly, Category = "GameHud", meta = (AllowPrivateAccess = "true"))
		UGameHud* GameHud;

	UPROPERTY(EditAnywhere, Category = UMG, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<UGameHud>GameHudClass;

	UPROPERTY(Transient)
		class AMasterWeapon* CurrentNewWeapon;

	UPROPERTY(Transient)
		class AMasterWeapon* CurrentLastWeapon;

	class UMaterialInstanceDynamic* DynamicMat;
	class AReplicationPlayerState* RepPlayerState;


	bool bIsWeaponEquipped;


	/** Items in inventory */
	UPROPERTY(Transient, Replicated)
		TArray<class AGameItem*> Inventory;

	/** weapons in inventory */
	UPROPERTY(Transient, Replicated)
		TArray<class AMasterWeapon*> WeaponInventory;

	TArray<APickupItem*> _inventory;

	UPROPERTY()
	UWorld* World;


};
