// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Character.h"
#include "SkillSystem/Enums/EDamageType.h"
#include "SkillSystem/Enums/EStats.h"
#include "BaseCharacter.generated.h"

//Delegates
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthUpdatedDelegate, float, health);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterDieDelegate);

class UFloatingBarWidgetComponent;
class UFloatingBarWidget;
UCLASS()
class REPLICATION_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		FText Name;

	UPROPERTY(replicated, EditAnywhere, BlueprintReadWrite, ReplicatedUsing = OnRep_Health, Category = BaseInfo)
		float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float MaxHealth;

	UPROPERTY(replicated, EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		int CurrentLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float DeadBodyTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float RespawnDelayTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float WaitRespawnDelayTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		bool bIsRespawning;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		UFloatingBarWidgetComponent* FloatingBarWidgetComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		TSubclassOf<UUserWidget>FloatingBarWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VisualEffects)
		UFloatingBarWidget* FloatingHealthWidget; 

	UPROPERTY(replicated, EditAnywhere, BlueprintReadWrite, ReplicatedUsing = OnRep_IsAlive, Category = BaseInfo)
		bool bIsAlive;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		bool bIsAggressive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		USphereComponent* WidgetVisibilitySphere;


	/** effect played on respawn */
	UPROPERTY(EditDefaultsOnly, Category = VisualEffects)
		UParticleSystem* RespawnFX;

	/** sound played on respawn */
	UPROPERTY(EditDefaultsOnly, Category = Sounds)
		USoundCue* RespawnSound;



	UPROPERTY(replicated, EditAnywhere, BlueprintReadWrite,  Category = ExperienceSystem)
		int CurrentExp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ExperienceSystem)
		int ExpForNextLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ExperienceSystem)
		float NextExpMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
	float NextHealthMultiplier;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = Stats)
		FOnHealthUpdatedDelegate OnUpdateHealth;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = Stats)
		FOnCharacterDieDelegate OnCharacterDie;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		float BaseDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		EDamageType DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		int CriticalChance;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations )
		UAnimMontage* DieAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
		UAnimMontage* HitAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sounds)
		USoundCue* HitSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sounds)
		USoundCue* DieSound;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SkillSystem)
		TSubclassOf<AMasterElement> Element;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float WalkSpeed = 200.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float RunSpeed = 600.f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float MaxWalkSpeed = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float MaxRunSpeed = 600.f;;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float MaxCombatWalkSpeed = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseInfo)
		float MaxCombatRunSpeed = 300.f;


	UFUNCTION(BlueprintNativeEvent)
		void OnRep_Health();

	UFUNCTION(BlueprintNativeEvent)
		void OnRep_IsAlive();


	UFUNCTION(NetMulticast, Reliable)
		void ClientSetHealth(float amount);

	// Called on death for all clients for hilarious death
	UFUNCTION(NetMultiCast, unreliable)
		void MultiCastRagdoll();


	UFUNCTION(NetMulticast, Reliable)
	void MulticastUpdateHealth( );

	void AddHealth(float amount);

	UFUNCTION(Client, Reliable)
	void RemoveHealth(float amount);

	virtual void UpdateHealth(float value);

	void UpdateFloatingTextLevel();

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
		void Server_AddHealth(float amount);

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
		void Server_RemoveHealth(float amount);


	UFUNCTION(Server, Reliable, WithValidation)
	void ServerTakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser);

	/** get current health */
	float GetCurrentHealth() const;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

	UFUNCTION()
	void SimulateDie();
	
	// Play hit anim on owning client when hit
	//UFUNCTION(Client, Reliable)
	virtual float SimulateHit();

	virtual void SimulateRespawn();


	UFUNCTION(NetMulticast, UnReliable)
	void MulticastHit();

	UFUNCTION(NetMulticast, UnReliable)
	void Client_SimulateDie();

	UFUNCTION(NetMulticast, UnReliable)
	void Client_SimulateHit();

	UFUNCTION()
		virtual void Die();

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_Die();

	UFUNCTION()
		virtual void OnDeath(AActor* Killer);
	
	UFUNCTION(Server, Reliable, WithValidation)
		void Server_OnDeath(AActor* Killer);


	UFUNCTION()
		virtual void Respawn();


	UFUNCTION(BlueprintPure, Category = BaseInfo)
	bool IsAlive() const;

	UFUNCTION()
	virtual void OnWidgetBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	virtual void OnWidgetEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintImplementableEvent)
		void BPIE_InitializeUI();

	UFUNCTION(BlueprintImplementableEvent)
		void BPIE_InitWidgetText();

	UFUNCTION(BlueprintImplementableEvent)
		void BPIE_UpdateHealthBar();

	virtual void OnWaitRespawnTimerHandled();

protected:
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	virtual void OnDieTimerHandled();

	bool bTimerExpired;
	FTimerHandle DieTimerHandle;
	FTimerHandle RespawnTimerHandle;
	FTimerHandle WaitRespawnTimerHandle;
	FTimerHandle SimulateHitTimerHandle;

	UFUNCTION(NetMulticast, Reliable)
		void DieTimerExpired();

	UPROPERTY(EditAnywhere, Category = UMG, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<UFloatingWidget>FloatingWidgetClass;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private: 

	

};
