// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/PlayerController.h"
#include "ReplicationPlayerController.generated.h"


class AMissileSkill;
class AGoalDecal;
UCLASS()
class REPLICATION_API AReplicationPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
		
	AReplicationPlayerController();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Inventory)
		bool bIsShowingInventory;
	
	UFUNCTION(BlueprintCallable)
	void ToggleInventory();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<AGoalDecal> GoalDecalClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		AMissileSkill* MissileSpell;

	UFUNCTION()
	AReplicationCharacter* GetControlledCharacter();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = SkillSystem)
		AActor* SelectedActor;


	void CancelMovementCommand();

protected:
		
		/** Called when the game starts. */
		virtual void BeginPlay() override;
		virtual void Tick(float DeltaSeconds) override;
		virtual void SetupInputComponent() override;
		
private:

	UFUNCTION()
	void LookUp(float value);
	
	UFUNCTION()
		void Turn(float value);

	UFUNCTION()
	void MoveForward(float Value);

	UFUNCTION()
	void MoveRight(float Value);

	UFUNCTION()
	void MoveToClick();

	UFUNCTION()
	void EnableTurning();
	
	UFUNCTION()
	void DisableTurning();
	
	void ProcessMouse();


	UPROPERTY()
	AReplicationCharacter* RepCharacter;

	UPROPERTY()
	AGoalDecal* CurrentGoalDecal;

	bool CanTurn;
	bool HasMouseMovementCommand;


};