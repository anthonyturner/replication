// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "Kismet/KismetMathLibrary.h"
#include "UI/ReplicationHUD.h"
#include "SkillSystem/GoalDecal.h"
#include "SkillSystem/Interfaces/Selectable.h"

AReplicationPlayerController::AReplicationPlayerController() {

	bIsShowingInventory = false;
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
	bEnableTouchEvents = true;
	CanTurn = false;
}


void AReplicationPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("EnableTurning", IE_Pressed, this, &AReplicationPlayerController::EnableTurning);
	InputComponent->BindAction("EnableTurning", IE_Released, this, &AReplicationPlayerController::DisableTurning);
	InputComponent->BindAction("MoveToClick", IE_Pressed, this, &AReplicationPlayerController::MoveToClick);

	InputComponent->BindAxis("LookUp", this, &AReplicationPlayerController::LookUp);
	InputComponent->BindAxis("Turn", this, &AReplicationPlayerController::Turn);
	InputComponent->BindAxis("MoveForward", this, &AReplicationPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AReplicationPlayerController::MoveRight);

}


void AReplicationPlayerController::BeginPlay()
{
	Super::BeginPlay();

	 RepCharacter = Cast<AReplicationCharacter>(GetPawn());
	
	
}

void AReplicationPlayerController::Tick(float DeltaSeconds) {

	//ProcessMouse();
}


void AReplicationPlayerController::EnableTurning() {
	
	CanTurn = true;
}


AReplicationCharacter* AReplicationPlayerController::GetControlledCharacter() {

	return RepCharacter;
}

void AReplicationPlayerController::MoveToClick() {

	CancelMovementCommand();

	if (GoalDecalClass) {

		//FParams
		FActorSpawnParameters spawnParams;
		
		spawnParams.Owner = this;
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		FHitResult MouseHitResult;
		bool HasHit = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, MouseHitResult);
		//GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, MouseHitResult);
		if (HasHit) {

			AActor* HitActor = MouseHitResult.GetActor();
			ISelectable* NewSelectedActor = Cast<ISelectable>(HitActor);
			if (NewSelectedActor) {

				if (SelectedActor && (SelectedActor != HitActor)) {

					ISelectable* ISelectedActor = Cast<ISelectable>(SelectedActor);
					ISelectedActor->OnSelectionEnd(RepCharacter);
				}

				SelectedActor = HitActor;
				NewSelectedActor->OnSelected(RepCharacter);

			} else {

				const FRotator YawRotation(0, 0, 0);
				CurrentGoalDecal = GetWorld()->SpawnActor<AGoalDecal>(GoalDecalClass, MouseHitResult.Location, YawRotation, spawnParams);
				HasMouseMovementCommand = true;
				UNavigationSystem::SimpleMoveToLocation(this, MouseHitResult.Location);
				
			}
				
		}
		
	}

}

void AReplicationPlayerController::CancelMovementCommand(){

	if (SelectedActor) {

		ISelectable* ISelectedActor = Cast<ISelectable>(SelectedActor);
		ISelectedActor->OnSelectionEnd(RepCharacter);
		SelectedActor = nullptr;

	} 
	
	if (CurrentGoalDecal) {

		RepCharacter->GetCharacterMovement()->StopMovementImmediately();
		
		CurrentGoalDecal->Destroy();
		HasMouseMovementCommand = false;
		CurrentGoalDecal = nullptr;
	}

}


void AReplicationPlayerController::MoveForward(float Value)
{
	if (Value != 0.0f){

		USpringArmComponent* CameraArm = RepCharacter->GetCameraBoom();
		FTransform RelativeTransfrom = CameraArm->GetRelativeTransform();
		FQuat Rotation = RelativeTransfrom.GetRotation();

		FRotator Rotator = Rotation.Rotator();
		const FRotator YawRotation(0, Rotator.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		if (HasMouseMovementCommand) {
			//Cancel any clicks that move to a target location
			CancelMovementCommand();

		}
		
		RepCharacter->AddMovementInput(Direction, Value);
	}
}



void AReplicationPlayerController::MoveRight(float Value)
{
	if (Value != 0.0f)
	{


		USpringArmComponent* CameraArm = RepCharacter->GetCameraBoom();
		FTransform RelativeTransfrom = CameraArm->GetRelativeTransform();
		FQuat Rotation = RelativeTransfrom.GetRotation();

		FRotator Rotator = Rotation.Rotator();
		const FRotator YawRotation(0, Rotator.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		if (HasMouseMovementCommand) {

			CancelMovementCommand();

		}

		RepCharacter->AddMovementInput(Direction, Value);
	

	}
}


void AReplicationPlayerController::LookUp(float value) {

	if (CanTurn && value != 0) {

		USpringArmComponent* CameraArm = RepCharacter->GetCameraBoom();
		FTransform RelativeTransfrom = CameraArm->GetRelativeTransform();
		FQuat Rotation = RelativeTransfrom.GetRotation();
		FRotator Rotator = Rotation.Rotator();

		//Clamp camera so it does not go through map
		Rotator.Pitch = FMath::Clamp(Rotator.Pitch + (value*1.5f), -40.f, 0.f);
		
		CameraArm->SetRelativeRotation(Rotator);
	}
}


void AReplicationPlayerController::Turn(float value) {

	if (CanTurn && value != 0) {

		USpringArmComponent* CameraArm = RepCharacter->GetCameraBoom();
		FTransform RelativeTransfrom = CameraArm->GetRelativeTransform();
		FQuat Rotation = RelativeTransfrom.GetRotation();
		FRotator Rotator = Rotation.Rotator();

		//Clamp camera so it does not go through map
		Rotator.Yaw = Rotator.Yaw + (value*2.f);

		CameraArm->SetRelativeRotation(Rotator);
	}
}


void AReplicationPlayerController::DisableTurning() {

	CanTurn = false;

}

void AReplicationPlayerController::ToggleInventory() {


	AReplicationHUD* hud = Cast<AReplicationHUD>(GetHUD());
	if (hud) {

		if (bIsShowingInventory) {

			hud->HideInventory();
			bIsShowingInventory = false;
			bShowMouseCursor = false;
			bEnableClickEvents = false;
			bEnableMouseOverEvents = false;
		} else {

			hud->ShowInventory();

			bIsShowingInventory = true;
			bShowMouseCursor = true;
			bEnableClickEvents = true;
			bEnableMouseOverEvents = true;
		}
	}

}


//Capture mouse clicks for rotating the player's pawn in the direction of the click.
void AReplicationPlayerController::ProcessMouse() {


	FHitResult cursourHitResults;

	bool hasHitResult = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, cursourHitResults);
	if (hasHitResult) {

		APawn* pawn = GetPawn();
		if (pawn) {

			
			
			FVector location = cursourHitResults.Location;
			FVector direction = UKismetMathLibrary::GetDirectionUnitVector(pawn->GetActorLocation(), location);
			FRotator rotation = UKismetMathLibrary::MakeRotFromX(direction);
			rotation.Roll = 0.f;
			rotation.Pitch = 0.f;

			pawn->SetActorRotation(rotation);
		}
	}


}
