// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "MasterElement.h"


// Sets default values
AMasterElement::AMasterElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMasterElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMasterElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

