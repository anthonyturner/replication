// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SkillSystem/Structs/FElementInfo.h"
#include "MasterElement.generated.h"


UCLASS()
class REPLICATION_API AMasterElement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMasterElement();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Element)
	FElementInfo ElementInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
