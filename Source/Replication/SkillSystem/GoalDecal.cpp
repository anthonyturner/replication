// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "GoalDecal.h"


// Sets default values
AGoalDecal::AGoalDecal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	Decal = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComponent"));
	Decal->DecalSize = FVector(70.f, 35.f, 35.f);
	
	Decal->SetRelativeRotation(FQuat(FRotator(0.f, -180.0f, 0.f)));
	//Decal->SetActor(FQuat(FRotator(0.f, -90.0f, 0.f)));

	Decal->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);



	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionComponent"));
	BoxCollision->AttachToComponent(Decal, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &AGoalDecal::OnBeginOverlap);
	BoxCollision->OnComponentEndOverlap.AddDynamic(this, &AGoalDecal::OnEndOverlap);
	
	FVector InBoxExtent(75.f, 40.f, 50.f);
	BoxCollision->SetBoxExtent(InBoxExtent);
	
}

// Called when the game starts or when spawned
void AGoalDecal::BeginPlay()
{
	Super::BeginPlay();
	//Decal->GetRelativeTransform().SetRotation(FQuat(0.f, -90.f, 0.f, 0.f));

	AReplicationPlayerController* controller = Cast<AReplicationPlayerController>(GetOwner());
	if (controller) {
		RepPlayerController = controller;
		GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Blue, "Spawned controller for rep");
	}
	else {

		GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, "Couldnt spawn controller");
	}
	
}

// Called every frame
void AGoalDecal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGoalDecal::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {


	if (OtherActor && OtherActor != this) {

		if (OtherActor == RepPlayerController->GetControlledCharacter()) {

			RepPlayerController->CancelMovementCommand();
		}
	}

}



void AGoalDecal::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex){


}

