#pragma once

#include "FStatLerp.generated.h"

USTRUCT(BlueprintType)
struct REPLICATION_API FStatLerp
{
	GENERATED_BODY()

		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float OriginalValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ValueToLerpTo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isPositive;

	FStatLerp() {

		
	}
};