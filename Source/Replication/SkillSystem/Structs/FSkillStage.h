#pragma once

#include "SkillSystem/Enums/EDamageType.h"
#include "FSkillStage.generated.h"

class ASkill;
USTRUCT(BlueprintType)
struct REPLICATION_API FSkillStage {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		TArray<ASkill*> RequiredSkills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		UTexture2D* OverrideIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		int RequiredLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float Range;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		EDamageType DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		int CriticalChance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float Cooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		UCurveFloat* CooldownCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		UCurveFloat* BuffDurationCurve;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		UCurveFloat* CastingCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float CastingTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float AreaRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float MissileSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		int ManaCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float DmgPerTick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float TickInterval;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		float Duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		int Accuracy;

	

	FSkillStage() {


	}
};