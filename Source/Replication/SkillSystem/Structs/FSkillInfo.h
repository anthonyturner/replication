#pragma once
#include "FSkillStage.h"
#include "Skillsystem/Enums/ETargetTypes.h"
#include "FSkillInfo.generated.h"

class AMasterElement;
USTRUCT(BlueprintType)
struct REPLICATION_API FSkillInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		TSubclassOf<AMasterElement> Element;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		ETargetTypes Target;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		TArray<FSkillStage> Stages;

	FSkillInfo() {

		
	}
};