#pragma once

#include "FElementInfo.generated.h"

class AMasterElement;
USTRUCT(BlueprintType)
struct REPLICATION_API FElementInfo
{
	GENERATED_BODY()


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor Color = FLinearColor::White;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<TSubclassOf<AMasterElement>> Weaknesses;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<TSubclassOf<AMasterElement>> Resistances;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MinLerpTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxLerpTime;




	FElementInfo() {

	
	}
};