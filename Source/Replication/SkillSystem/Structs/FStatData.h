#pragma once

#include "SkillSystem/Widgets/StatBarWidget.h"
#include "FStatData.generated.h"

USTRUCT(BlueprintType)
struct REPLICATION_API FStatData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString StatName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int MinValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int MaxValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CurrentValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DisplayedValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MinLerpTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxLerpTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStatBarWidget* BarWidget;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsAnimated;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bHasRegen;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float TimeToRegenMaxValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RegInterval;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FTimerHandle RegenTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString RegFunctionName;


	FStatData() {

		MinLerpTime = 0.2f;
		MaxLerpTime = 1.5f;
		TimeToRegenMaxValue = 45.f;
		RegInterval = .4;
	}
};