// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BuffWidget.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API UBuffWidget : public UUserWidget
{
	GENERATED_BODY()
	
	
public:

	UFUNCTION(BlueprintCallable)
		void SetImageIcon(UTexture2D* Icon);

	UFUNCTION(BlueprintPure)
		UTexture2D* GetImageIcon() const;

	UFUNCTION(BlueprintImplementableEvent)
	void OnBuffProgressUpdated(float value, float percent);

	UFUNCTION(BlueprintImplementableEvent)
	void OnBuffActivated();


private:
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuffWidget")
	UPROPERTY()
	UTexture2D* ImageIcon;
	
	
};
