// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "BuffWidget.h"



void UBuffWidget::SetImageIcon(UTexture2D* Icon) {

	ImageIcon = Icon;
}


UTexture2D* UBuffWidget::GetImageIcon() const {

	return ImageIcon;
}