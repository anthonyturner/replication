// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "StatBarWidget.h"
#include "Runtime/UMG/Public/Components/ProgressBar.h"

void UStatBarWidget::NativeConstruct() {

	StatsBar = NewObject<UProgressBar>(this);
	StatsBar->SetFillColorAndOpacity(StatsBarColor);
}

UProgressBar* UStatBarWidget::GetStatsBar() {

	return StatsBar;

}


UTextBlock* UStatBarWidget::GetStatsBarName()
{
	return StatsNameTextBlock;
}

UTextBlock* UStatBarWidget::GetStatsBarValue()
{
	return StatsValueTextBlock;
}

void UStatBarWidget::SetStatsBarValue(float percent) {
	
	StatsBar->SetPercent(percent);

}
