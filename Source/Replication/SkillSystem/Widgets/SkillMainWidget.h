// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SkillMainWidget.generated.h"

class USkillHotKeyWidget;
class UTextBlock;
class UProgressBar;
class UOverlay;
class UImage;
class UHorizontalBox;
class UBuffWidget;

UCLASS()
class REPLICATION_API USkillMainWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<USkillHotKeyWidget*>AllHotKeySlots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FKey>HotKeys;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int HotKeysPerRow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTextBlock* CastingSpellNameText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTextBlock* CastingSpellTimeText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UProgressBar* CastingSpellBar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UOverlay* CastingSpellOverlay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UOverlay* WeaponCooldownStatusAreaOverlay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTextBlock* WeaponCooldownTextBlock;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UImage* WeaponCooldownImage;

public:
	void SetHotKeys(TArray<FKey> keys);
	void DisableOtherKeys(ASkill* ExceptionSkill);
	void DisableAllKeys();
	void EnableOtherKeys(ASkill* ExceptionSkill);
	void EnableAllKeys();

	UFUNCTION(BlueprintPure)
	UHorizontalBox* GetBuffBox();

	UFUNCTION(BlueprintCallable)
	void SetBuffBox(UHorizontalBox* Bufferbox);

	UFUNCTION(BlueprintImplementableEvent)
	void OnAddBuffWidget(const UBuffWidget* Buff);

private:

	UPROPERTY()
	UHorizontalBox* BuffBox;


};
