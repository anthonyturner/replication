// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "SkillHotKeyWidget.h"


void USkillHotKeyWidget::AssignSkill(ASkill* Spell) {

	AssignedSpell = Spell;
	AssignedSpell->HotKey = this;
	SetUIElements(true);
}

void USkillHotKeyWidget::ClearAssignedSpell(){

	if (AssignedSpell) {

		AssignedSpell->HotKey = nullptr;
		AssignedSpell = nullptr;
		SetUIElements(false);

	}
}

void USkillHotKeyWidget::EnableHotkey() {

	bIsDeactived = false;
	if (!AssignedSpell->bIsCoolDown) {

		ActivateHotKeyElements();
	}

}


void USkillHotKeyWidget::DisableHotkey() {

	bIsDeactived = true;
	if (!AssignedSpell->bIsCoolDown) {

		DeactivateHotKeyElements();
	}

}
