// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "SkillSystem/Structs/FSkillStage.h"

#include "SkillHotKeyWidget.generated.h"

class ASkill;
UCLASS()
class REPLICATION_API USkillHotKeyWidget : public UUserWidget
{
	GENERATED_BODY()
	
	
public:


	void EnableHotkey();
	void DisableHotkey();

	UFUNCTION(BlueprintImplementableEvent)
		void DisableCastingElements(FSkillStage SkillStage);

	UFUNCTION(BlueprintImplementableEvent)
		void EnableCastingElements();

	UFUNCTION(BlueprintCallable)
	void AssignSkill(ASkill* AssignedSpell);

	UFUNCTION(BlueprintImplementableEvent)
	void SetUIElements(bool isEnabled);

	UFUNCTION(BlueprintImplementableEvent)
	void ActivateHotKeyElements();

	UFUNCTION(BlueprintImplementableEvent)
	void DeactivateHotKeyElements();

	UFUNCTION(BlueprintCallable)
		void ClearAssignedSpell();


public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ASkill* AssignedSpell;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FKey HotKey;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInstanceDynamic* CooldownMat;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTextBlock* CooldownTextBlock;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsDeactived;
};
