// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "SkillMainWidget.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/ProgressBar.h"
#include "Runtime/UMG/Public/Components/Overlay.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/UMG/Public/Components/HorizontalBox.h"

void USkillMainWidget::SetHotKeys(TArray<FKey> keys) {

	HotKeys = keys;
}

void USkillMainWidget::DisableOtherKeys(ASkill* ExceptionSkill) {

	//Get each key and its assigned spell
	//Disable all keys except for the one that has the ExceptionSkill(Spell)
	for (int j = 0; j < AllHotKeySlots.Num(); j++) {

		USkillHotKeyWidget* HotKeyWidget = (AllHotKeySlots[j]);
		ASkill* CheckedAssignedSpell = HotKeyWidget->AssignedSpell;

		if (CheckedAssignedSpell && CheckedAssignedSpell != ExceptionSkill) {//Disable other skill buttons

			HotKeyWidget->DisableHotkey();
		}
	}
}

void USkillMainWidget::DisableAllKeys() {

	//Get each key and its assigned spell
	//Disable all keys except for the one that has the ExceptionSkill(Spell)
	for (int j = 0; j < AllHotKeySlots.Num(); j++) {

		USkillHotKeyWidget* HotKeyWidget = (AllHotKeySlots[j]);
		ASkill* CheckedAssignedSpell = HotKeyWidget->AssignedSpell;

		if (CheckedAssignedSpell) {//Disable other skill buttons

			HotKeyWidget->DisableHotkey();
		}
	}
}


void USkillMainWidget::EnableAllKeys() {

	//Get each key and its assigned spell
	//Disable all keys except for the one that has the ExceptionSkill(Spell)
	for (int j = 0; j < AllHotKeySlots.Num(); j++) {

		USkillHotKeyWidget* HotKeyWidget = (AllHotKeySlots[j]);
		ASkill* CheckedAssignedSpell = HotKeyWidget->AssignedSpell;

		if (CheckedAssignedSpell) {//Disable other skill buttons

			HotKeyWidget->EnableHotkey();
		}
	}
}

UHorizontalBox* USkillMainWidget::GetBuffBox()
{
	return BuffBox;
}

void USkillMainWidget::SetBuffBox(UHorizontalBox * Bufferbox)
{
	BuffBox = Bufferbox;
}


void USkillMainWidget::EnableOtherKeys(ASkill* ExceptionSkill) {

	//Get each key and its assigned spell
	//Enable all keys except for the one that has the ExceptionSkill(Spell)
	for (int j = 0; j < AllHotKeySlots.Num(); j++) {

		USkillHotKeyWidget* HotKeyWidget = (AllHotKeySlots[j]);
		ASkill* CheckedAssignedSpell = HotKeyWidget->AssignedSpell;

		if (CheckedAssignedSpell && CheckedAssignedSpell != ExceptionSkill) {//Renable other skill buttons

			HotKeyWidget->EnableHotkey();
		}
	}
}