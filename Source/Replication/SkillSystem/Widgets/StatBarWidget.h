// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Blueprint/UserWidget.h"
#include "StatBarWidget.generated.h"

class UProgressBar;
class UTextBlock;

UCLASS()
class REPLICATION_API UStatBarWidget : public UUserWidget
{
	GENERATED_BODY()
	

public:

	UFUNCTION(BlueprintImplementableEvent, Category = "StatsBar")
		void OnUpdateStatBar(float percent);

	UFUNCTION(BlueprintImplementableEvent, Category = "StatsBar")
		void OnUpdateStatValue(const FText& value);

	UFUNCTION(BlueprintCallable, Category = "StatsBar")
	UProgressBar* GetStatsBar();

	UFUNCTION(BlueprintCallable, Category = "StatsBar")
	void SetStatsBarValue(float percent);

	UFUNCTION(BlueprintPure, Category = "StatsBar")
		UTextBlock* GetStatsBarName();

	UFUNCTION(BlueprintPure, Category = "StatsBar")
		UTextBlock* GetStatsBarValue();


protected:

	virtual void NativeConstruct() override;
	
private:


	//UPROPERTY(BlueprintReadWrite, Category = "StatsBar", meta = (AllowPrivateAccess = "true"))
	UPROPERTY()
	UProgressBar* StatsBar;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "StatsBar", meta = (AllowPrivateAccess = "true"))
	FLinearColor StatsBarColor;

	UPROPERTY()
		UTextBlock* StatsNameTextBlock;

	UPROPERTY()
		UTextBlock* StatsValueTextBlock;
	
};
