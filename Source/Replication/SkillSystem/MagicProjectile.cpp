// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "MagicProjectile.h"
#include "SkillSystem/Skills/Missiles/MissileSkill.h"


// Sets default values
AMagicProjectile::AMagicProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	SphereCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollisionComponent"));
	SphereCollisionComponent->SetSphereRadius( 55.0f );
	SphereCollisionComponent->SetEnableGravity(false);
	//Sphere>SetCol
	SphereCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
//	Sphere->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
	SphereCollisionComponent->SetNotifyRigidBodyCollision(true);//Simulation Generates Hit Events
	SphereCollisionComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Block);
	SphereCollisionComponent->OnComponentHit.AddDynamic(this, &AMagicProjectile::OnMagicProjectileHit);
	//RootComponent = SphereCollisionComponent;
	SphereCollisionComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	//RootComponent = Sphere;
	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
	ParticleSystemComponent->AttachToComponent(SphereCollisionComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	
	MagicProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MagicProjectileMovementComponent"));
	MagicProjectileMovementComponent->InitialSpeed = 3000.f;
	MagicProjectileMovementComponent->MaxSpeed = 3000.f;
	MagicProjectileMovementComponent->bRotationFollowsVelocity = false;
	MagicProjectileMovementComponent->ProjectileGravityScale = 0.f;
	MagicProjectileMovementComponent->Bounciness = 0.f;
	MagicProjectileMovementComponent->Friction = .3f;
	MagicProjectileMovementComponent->SetVelocityInLocalSpace(FVector::ZeroVector);
	//MagicProjectileMovementComponent->bIsHomingProjectile = true;
	//MagicProjectileMovementComponent->HomingAccelerationMagnitude = 1000000.f;
	


}

// Called when the game starts or when spawned
void AMagicProjectile::BeginPlay()
{
	Super::BeginPlay();
	float ProjectileMissleSpeed = 3000.f; //Use default speed if skill does not exist
	if (Skill) {
		ProjectileMissleSpeed = Skill->GetCurrentStage().MissileSpeed;

	}
	MagicProjectileMovementComponent->InitialSpeed = ProjectileMissleSpeed;
	MagicProjectileMovementComponent->MaxSpeed = ProjectileMissleSpeed;

	ParticleSystemComponent->SetTemplate(MissleEffectParticle);
	if( Target )
		MagicProjectileMovementComponent->HomingTargetComponent = Target->HitArrowComponent;
}

// Called every frame
void AMagicProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AMagicProjectile::OnMagicProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {


	AEnemyCharacter* OtherEnemyCharacter = Cast<AEnemyCharacter>(OtherActor);
	if (OtherEnemyCharacter && OtherEnemyCharacter == Target) {

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffectParticle, GetActorLocation());

		FSkillStage SkillStage = Skill->GetCurrentStage();

		//Get the owner whom launched this projectile and use there base damage
		float BaseDamage = 1.f;//Default value for no-owner.
		AActor* ProjectileOwner = GetOwner();
		if (ProjectileOwner) {
			ABaseCharacter* DamageOwner = Cast<ABaseCharacter>(ProjectileOwner);
			if (DamageOwner) {

				BaseDamage = DamageOwner->BaseDamage;
			}
		}
		
		float Damage = SkillStage.Damage * BaseDamage;
		EDamageType DamageType = SkillStage.DamageType;
		float CriticalChance = SkillStage.CriticalChance;
		
		Target->OnReceiveDamage(Damage, DamageType, Skill->SkillInfo.Element, CriticalChance, Skill->RepPlayerReference, Skill);
		Destroy();
	}

}

