#pragma once

#include "EStats.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EStats : uint8
{
	EMPTY 	UMETA(DisplayName = "EMPTY"),
	Health 	UMETA(DisplayName = "Health"),
	Mana	UMETA(DisplayName = "Mana"),
	Experience 	UMETA(DisplayName = "Experience")
};
