#pragma once

#include "EDamageType.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EDamageType : uint8
{
	Healing 	UMETA(DisplayName = "Healing"),
	Magic 	UMETA(DisplayName = "Magic"),
	Physical 	UMETA(DisplayName = "Physical"),
	AbsoluteDamage	UMETA(DisplayName = "AbsouteDamage")
};
