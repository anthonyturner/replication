#pragma once

#include "EEffectiveness.generated.h"

//How elements will interact with each other
UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EEffectiveness : uint8
{
	Effective 	UMETA(DisplayName = "Effective"),
	SuperEffective 	UMETA(DisplayName = "SuperEffective"),
	NotEffective	UMETA(DisplayName = "NotEffective")
};
