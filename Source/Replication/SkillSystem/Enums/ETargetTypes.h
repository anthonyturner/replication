#pragma once

#include "ETargetTypes.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ETargetTypes : uint8
{
	Default 	UMETA(DisplayName = "Default"),
	ForSelf 	UMETA(DisplayName = "ForSelf"),
	SelectedEnemy	UMETA(DisplayName = "SelectedEnemy"),
	SelectedArea 	UMETA(DisplayName = "SelectedArea"),
	AreaAroundSelf 	UMETA(DisplayName = "AreaAroundSelf"),
	Missile 	UMETA(DisplayName = "Missile")
};
