// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillSystem/Skills/Skill.h"
#include "BuffSkill.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API ABuffSkill : public ASkill
{
	GENERATED_BODY()
	
public:

	ABuffSkill();

	UPROPERTY()
	ABuffSkill* CurrentBuff;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTimelineComponent* BuffDurationTimeline;

	UFUNCTION(BlueprintCallable)
		UTexture2D* GetBuffIcon();

	UFUNCTION(BlueprintPure)
		UBuffWidget* GetBuffWidgetRef();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Buffs")
		float BuffAmount;

	void ResetBuff();
	void OnActivated();
	void OnDeactivated();

	virtual void BeginPlay() override;

protected:

	virtual void OnSkillNotify() override;


	//UFUNCTION(BlueprintNativeEvent)
	virtual void ApplyEffect();

	//UFUNCTION(BlueprintNativeEvent)
	virtual void RemoveEffect();

	FTimerHandle ApplyEffectTimerHandle;

private:

	UFUNCTION()
		void OnInterpBuffFloatEvent(float Value);

	UFUNCTION()
		void OnBufferDurationUpdated();

	UFUNCTION()
		void OnBufferDurationFinished();

	UPROPERTY(EditAnywhere, Category = "Buffs", meta = (AllowPrivateAccess = "true"))
		UTexture2D* BuffIcon;

	UPROPERTY(EditAnywhere, Category = "Buffs", meta = (AllowPrivateAccess = "true"))
		UBuffWidget* BuffWidgetRef;

	float Duration;
	float PassedTime;

	FOnTimelineEvent OnBufferDurationUpdateEvent;
	FOnTimelineFloat progressFunction{};


};
