// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "BuffSkill.h"




ABuffSkill::ABuffSkill() {

	BuffDurationTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("BuffDurationTimelineComponent"));

	FOnTimelineEvent OnBufferDurationFinishedEvent;
	OnBufferDurationFinishedEvent.BindDynamic(this, &ABuffSkill::OnBufferDurationFinished);
	BuffDurationTimeline->SetTimelineFinishedFunc(OnBufferDurationFinishedEvent);

	OnBufferDurationUpdateEvent.BindDynamic(this, &ABuffSkill::OnBufferDurationUpdated);
	progressFunction.BindUFunction(this, "OnInterpBuffFloatEvent"); // The function BufferDurationProgress gets called
}

void ABuffSkill::BeginPlay() {

	Super::BeginPlay();

	FSkillStage SkillStage = GetCurrentStage();
	if (SkillStage.BuffDurationCurve) {

		BuffDurationTimeline->AddInterpFloat(SkillStage.BuffDurationCurve, progressFunction, FName{ TEXT("BuffDuration") });
	}

	BuffDurationTimeline->SetTimelinePostUpdateFunc(OnBufferDurationUpdateEvent);
}


void ABuffSkill::OnBufferDurationUpdated() {

	//GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Yellow, "OnBufferDurationUpdated" );
	float PlaybackPosition = BuffDurationTimeline->GetPlaybackPosition();
	PassedTime = PlaybackPosition;
	float DurationValue = Duration - PassedTime;
	float DurationPercent = PassedTime / Duration;
	if( BuffWidgetRef )
	BuffWidgetRef->OnBuffProgressUpdated(DurationValue, DurationPercent);
}

void ABuffSkill::OnInterpBuffFloatEvent(float Value) {


	//GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Yellow, "OnInterpBuffFloatEvent" + FString::SanitizeFloat(Value));

}

void ABuffSkill::OnSkillNotify() {

	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Yellow, "ABuffSkill::OnSkillNotify");

	Super::OnSkillNotify();
	bool HasCurrentBuff = RepPlayerReference->HasBuff(this);
	if (HasCurrentBuff) {

		CurrentBuff = RepPlayerReference->GetBuff(this->GetClass());
		CurrentBuff->ResetBuff();
	}
	else {
		OnActivated();
	}
}

void ABuffSkill::OnBufferDurationFinished() {

	OnDeactivated();
}

void ABuffSkill::ResetBuff() {

	FSkillStage Stage = GetCurrentStage();
	Duration = Stage.Duration;
	PassedTime = 0.f;

	BuffDurationTimeline->SetTimelineLength(Duration);
	BuffDurationTimeline->PlayFromStart();
}

void ABuffSkill::OnActivated() {

	ApplyEffect();
	UBuffWidget* BuffWidget = RepPlayerReference->AddBuff(this);
	BuffWidgetRef = BuffWidget;
	Duration = GetCurrentStage().Duration;
	PassedTime = 0.f;

	BuffDurationTimeline->SetTimelineLength(Duration);
	BuffDurationTimeline->PlayFromStart();
	BuffWidgetRef->OnBuffActivated();
}

void  ABuffSkill::OnDeactivated() {

	RemoveEffect();
	RepPlayerReference->RemoveBuff(this);
	BuffWidgetRef = nullptr;
}



void ABuffSkill::ApplyEffect() {


}


void ABuffSkill::RemoveEffect() {

}



UBuffWidget* ABuffSkill::GetBuffWidgetRef() {

	return BuffWidgetRef;
}

UTexture2D* ABuffSkill::GetBuffIcon() {
	return BuffIcon;
}
