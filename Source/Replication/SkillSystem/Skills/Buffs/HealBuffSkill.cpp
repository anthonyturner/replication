// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "HealBuffSkill.h"


void AHealBuffSkill::OnSkillNotify() {

	Super::OnSkillNotify();
}


void AHealBuffSkill::ApplyEffect() {

	UWorld* World = GetWorld();
	if (World) {

		World->GetTimerManager().SetTimer(ApplyEffectTimerHandle, this, &AHealBuffSkill::HealRestore, 2.0f, true);
	}
}


void AHealBuffSkill::RemoveEffect() {

	UWorld* World = GetWorld();
	if (World) {

		World->GetTimerManager().ClearTimer(ApplyEffectTimerHandle);
		//ApplyEffectTimerHandle.Invalidate();

	}
}


void AHealBuffSkill::HealRestore() {

	RepPlayerReference->ModifyStat(EStats::Health, BuffAmount, false);
}

