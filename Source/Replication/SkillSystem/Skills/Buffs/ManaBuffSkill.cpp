// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "ManaBuffSkill.h"


void AManaBuffSkill::ApplyEffect() {
	GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, " AManaBuffSkill::ApplyEffect()");

	UWorld* World = GetWorld();
	if (World) {

		World->GetTimerManager().SetTimer(ApplyEffectTimerHandle, this, &AManaBuffSkill::ManaRestore, 2.0f, true);
	}
}


void AManaBuffSkill::RemoveEffect() {
	GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, " AManaBuffSkill::RemoveEffect()");

	UWorld* World = GetWorld();
	if (World) {

		World->GetTimerManager().ClearTimer(ApplyEffectTimerHandle);
		//ApplyEffectTimerHandle.Invalidate();

	}
}


void AManaBuffSkill::ManaRestore() {
	GEngine->AddOnScreenDebugMessage(-1, 4.f, FColor::Green, " AManaBuffSkill::ManaRestore()");

	RepPlayerReference->ModifyStat(EStats::Mana, BuffAmount, false);
}

