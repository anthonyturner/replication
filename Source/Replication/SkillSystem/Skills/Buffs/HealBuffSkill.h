// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillSystem/Skills/Buffs/BuffSkill.h"
#include "HealBuffSkill.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AHealBuffSkill : public ABuffSkill
{
	GENERATED_BODY()
	

protected:

	virtual void ApplyEffect() override;
	virtual void RemoveEffect() override;
	virtual void OnSkillNotify() override;
private:

	UFUNCTION()
		void HealRestore();
};
