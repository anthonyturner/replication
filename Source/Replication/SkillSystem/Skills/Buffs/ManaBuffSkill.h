// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillSystem/Skills/Buffs/BuffSkill.h"
#include "ManaBuffSkill.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AManaBuffSkill : public ABuffSkill
{
	GENERATED_BODY()
	
protected:

	virtual void ApplyEffect() override;
	virtual void RemoveEffect() override;

private:

	UFUNCTION()
		void ManaRestore();
};
