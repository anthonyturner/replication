// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SkillSystem/Structs/FSkillInfo.h"
#include "GameFramework/Actor.h"
#include "Skill.generated.h"

class USkillHotKeyWidget;

UCLASS()
class REPLICATION_API ASkill : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASkill(); 
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		FSkillInfo SkillInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		int CurrentStageIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		bool bIsCoolDown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		bool bIsCurrentlyCasted;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		UAnimMontage* SkillAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		UParticleSystem* SkillLaunchEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		USoundCue* SkillSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		TSubclassOf<USkillHotKeyWidget> SkillHotKeyWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		USkillHotKeyWidget* HotKey;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		AReplicationCharacter* RepPlayerReference;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		UTimelineComponent* CooldownTimelineComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Runtimes)
		UTimelineComponent* CastingTimelineComponent;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = HelperFunctions)
		FSkillStage GetCurrentStage();

	UFUNCTION(BlueprintImplementableEvent)
		void OnInitComplete();

	UFUNCTION(BlueprintImplementableEvent)
		void UIUpdateCastedSpellText(float castingPlayRate);

	void UpdateCastedSpellText(float CastingPlayRate);

	//UFUNCTION(BlueprintImplementableEvent)
	UFUNCTION()
		void PlayCooldownAnimation();

	UFUNCTION(BlueprintCallable)
		void PlayAnimation(UAnimMontage* Animation, float InPlayRate = 1.0f);

	void OnSpellCastCompleted();

	//Cooldown events
	UFUNCTION(BlueprintCallable)
		void OnCooldownCompleted();

	UFUNCTION()
		void OnTimelineCooldownUpdate();

	UFUNCTION()
		void OnTimelineCooldownFinished();

	UFUNCTION(BlueprintCallable)
		void UpdateRemaingCooldown();

	//Casting Events

	UFUNCTION(BlueprintCallable)
		void OnCastingCompleted();

	UFUNCTION()
		void OnTimelineCastingUpdate();

	UFUNCTION()
		void OnTimelineCastingFinished();

	UFUNCTION(BlueprintCallable)
		void UpdateRemaingCast();

	void UpdateCastingBar();
	
	void UpdateCastingTimeText();

	UFUNCTION(BlueprintImplementableEvent)
		void UpdateUIRemaingCooldown();

	void ManaCheck();


	UPROPERTY()
		TSubclassOf<UCurveFloat> DefaultCurveClass;

	FOnTimelineFloat InterpFunction{};
	FOnTimelineEvent OnTimelineCooldownFinishedEvent;
	FOnTimelineEvent OnTimelineCooldownUpdateEvent;
	FOnTimelineFloat OnInterpFloatEvent;
	UFUNCTION()
		void OnTimelineCooldownInterpFloat(float val);

	FOnTimelineEvent OnTimelineCastingFinishedEvent;
	FOnTimelineEvent OnTimelineCastingUpdateEvent;
	FOnTimelineFloat OnInterpCastingFloatEvent;

	UFUNCTION()
		void OnTimelineCastingInterpFloat(float val);


	FOnTimelineEvent TimelineCooldownUpdateEvent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		virtual void OnTryCastSpell();

	UFUNCTION(BlueprintCallable)
		virtual void OnSpellCast();


	UFUNCTION(BlueprintCallable)
		virtual void OnSkillNotify();

	UFUNCTION()
		virtual void InitializeSpellCast();

	virtual void SpawnEffects(UWorld* World, FVector Location);

private:

	UPROPERTY()
		FTimerHandle CooldownTimerHandle;

	UPROPERTY()
		FTimerHandle SkillAnimationHandler;

private:

	void OnPlayAnimation();

	UFUNCTION()
		void OnPlayAnimationCompleted();



};
