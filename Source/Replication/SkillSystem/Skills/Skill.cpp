// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "Skill.h"
#include "Runtime/UMG/Public/Components/Overlay.h"
#include "Runtime/UMG/Public/Components/ProgressBar.h"


// Sets default values
ASkill::ASkill()
{
	PrimaryActorTick.bCanEverTick = false;
	CooldownTimelineComponent = CreateDefaultSubobject<UTimelineComponent>(TEXT("CooldownTimelineComponent"));

	//Timeline cooldown
	OnTimelineCooldownFinishedEvent.BindDynamic(this, &ASkill::OnTimelineCooldownFinished);
	OnTimelineCooldownUpdateEvent.BindDynamic(this, &ASkill::OnTimelineCooldownUpdate);
	OnInterpFloatEvent.BindUFunction(this, "OnTimelineCooldownInterpFloat");


	//Timeline casting
	CastingTimelineComponent = CreateDefaultSubobject<UTimelineComponent>(TEXT("CastingTimelineComponent"));
	OnTimelineCastingFinishedEvent.BindDynamic(this, &ASkill::OnTimelineCastingFinished);
	OnTimelineCastingUpdateEvent.BindDynamic(this, &ASkill::OnTimelineCastingUpdate);
	OnInterpCastingFloatEvent.BindUFunction(this, "OnTimelineCastingInterpFloat");
	
	CurrentStageIndex = 0;
	
}

// Called when the game starts or when spawned
void ASkill::BeginPlay()
{
	Super::BeginPlay();

	
	FSkillStage SkillStage = GetCurrentStage();
	if (SkillStage.CooldownCurve) {
		CooldownTimelineComponent->AddInterpFloat(SkillStage.CooldownCurve, OnInterpFloatEvent, FName("Percentage_Complete"));
	}
	CooldownTimelineComponent->SetTimelineFinishedFunc(OnTimelineCooldownFinishedEvent);
	CooldownTimelineComponent->SetTimelinePostUpdateFunc(OnTimelineCooldownUpdateEvent);


	if (SkillStage.CastingCurve) {
		CastingTimelineComponent->AddInterpFloat(SkillStage.CastingCurve, OnInterpCastingFloatEvent, FName("Percentage_Complete"));
	}
	CastingTimelineComponent->SetTimelineFinishedFunc(OnTimelineCastingFinishedEvent);
	CastingTimelineComponent->SetTimelinePostUpdateFunc(OnTimelineCastingUpdateEvent);
	
}



// Called every frame
void ASkill::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FSkillStage ASkill::GetCurrentStage()
{
	return SkillInfo.Stages[CurrentStageIndex];
}

//---------------------------------------------------- Casting -----------------------------------------------------


void ASkill::OnTryCastSpell(){

	if (!bIsCoolDown && !bIsCurrentlyCasted && !(RepPlayerReference->bIsCasting)) {

		InitializeSpellCast();
	}
}


void ASkill::InitializeSpellCast() {

	ManaCheck();
}


void ASkill::ManaCheck() {

	FStatData* PlayerManaStatData = RepPlayerReference->GetStat(EStats::Mana);
	FSkillStage SkillStage = GetCurrentStage();
	float ManaCost = SkillStage.ManaCost;

	if (PlayerManaStatData->CurrentValue >= ManaCost) {

		bIsCurrentlyCasted = true;

		RepPlayerReference->BeginSpellCast(this);
		RepPlayerReference->ModifyStat(EStats::Mana, (ManaCost*-1), false);

		OnSpellCast();

	}
	else {

		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Red, "Not enough mana to cast this spell!");
	}
}



void ASkill::OnSpellCast() {

	if (SkillAnimation) {

		float PlayRate = SkillAnimation->SequenceLength / GetCurrentStage().CastingTime;
		UpdateCastedSpellText(GetCurrentStage().CastingTime);

		PlayAnimation(SkillAnimation, PlayRate);
	}

}


void ASkill::OnSpellCastCompleted() {

	RepPlayerReference->EndSpellCast(this);
	bIsCoolDown = true;

	bIsCurrentlyCasted = false;

	if (HotKey) {

		HotKey->DisableCastingElements(GetCurrentStage());
	}

	PlayCooldownAnimation();
}

//---------------------------------------------------- Timeline Animation/Cooldown -----------------------------------------------------


void ASkill::PlayCooldownAnimation() {

	//CooldownTimelineComponent->SetTimelineLength(GetCurrentStage().Cooldown);
	CooldownTimelineComponent->PlayFromStart();
}


void ASkill::OnTimelineCooldownInterpFloat(float val) {

}

void ASkill::OnTimelineCooldownUpdate() {

	UpdateRemaingCooldown();
}

void ASkill::UpdateRemaingCooldown() {

	if (HotKey) {
		//Set UI cooldown material percent
		 float CooldownPercent = (1 - (CooldownTimelineComponent->GetPlaybackPosition() / CooldownTimelineComponent->GetTimelineLength()));
		 HotKey->CooldownMat->SetScalarParameterValue(FName("Percent"), CooldownPercent);

		 //Set UI cooldown text countdown value
		 FText CooldownTextValue;
		 float CooldownCountDownValue =  CooldownTimelineComponent->GetTimelineLength() - CooldownTimelineComponent->GetPlaybackPosition();
		 if (CooldownCountDownValue > 1) {//Round to either 1 or 0 digits if countdown is less than 1.0
			 CooldownTextValue = UKismetTextLibrary::Conv_FloatToText(CooldownCountDownValue, ERoundingMode::HalfToEven, false, 1, 324, 0, 0);
		 } else {

			 CooldownTextValue = UKismetTextLibrary::Conv_FloatToText(CooldownCountDownValue, ERoundingMode::HalfToEven, false, 1, 324, 0, 1);
		 }

		 if(HotKey->CooldownTextBlock)
			HotKey->CooldownTextBlock->SetText(CooldownTextValue);
	}
}

void ASkill::OnTimelineCooldownFinished() {

	OnCooldownCompleted();
}


void ASkill::OnCooldownCompleted() {

	bIsCoolDown = false;
	if (HotKey) {

		HotKey->EnableCastingElements();
	}
}
//---------------------------------------------------- Timeline Animation/Casting -----------------------------------------------------


void ASkill::OnTimelineCastingInterpFloat(float val) {

}


void ASkill::OnTimelineCastingUpdate() {

	UpdateCastingBar();
	UpdateCastingTimeText();
}


void ASkill::UpdateCastedSpellText(float CastingTime) {


	USkillMainWidget* SkillWidget = RepPlayerReference->SkillMainWidget;
	if (SkillWidget) {

		SkillWidget->CastingSpellNameText->SetText(SkillInfo.Name);
		UpdateCastingBar();
		UpdateCastingTimeText();
		CastingTimelineComponent->SetTimelineLength(CastingTime);
		SkillWidget->CastingSpellOverlay->SetVisibility(ESlateVisibility::HitTestInvisible);
		CastingTimelineComponent->PlayFromStart();
	}

}

void ASkill::UpdateCastingBar() {

	float CastingTimePercentage = CastingTimelineComponent->GetPlaybackPosition() / GetCurrentStage().CastingTime;
	RepPlayerReference->SkillMainWidget->CastingSpellBar->SetPercent(CastingTimePercentage);

}

void ASkill::UpdateCastingTimeText() {

	float PlayBackPosition = CastingTimelineComponent->GetPlaybackPosition();
	float CastingTime = GetCurrentStage().CastingTime;


	FString CurrentTimeText = UKismetTextLibrary::Conv_FloatToText(PlayBackPosition, ERoundingMode::HalfToEven, false, 1, 324, 1, 1).ToString();
	FString CurrentCastingTimeText = UKismetTextLibrary::Conv_FloatToText(CastingTime, ERoundingMode::HalfToEven, false, 1, 324, 1, 1).ToString();

	FText CastingTimeText = UKismetTextLibrary::Conv_StringToText(CurrentTimeText.Append(" / " + CurrentCastingTimeText));
	RepPlayerReference->SkillMainWidget->CastingSpellTimeText->SetText(CastingTimeText);
}


void ASkill::UpdateRemaingCast() {

	if (HotKey) {
		//Set UI cooldown material percent
		float CooldownPercent = (1 - (CooldownTimelineComponent->GetPlaybackPosition() / CooldownTimelineComponent->GetTimelineLength()));
		HotKey->CooldownMat->SetScalarParameterValue(FName("Percent"), CooldownPercent);

		//Set UI cooldown text countdown value
		FText CooldownTextValue;
		float CooldownCountDownValue = CooldownTimelineComponent->GetTimelineLength() - CooldownTimelineComponent->GetPlaybackPosition();
		if (CooldownCountDownValue > 1) {//Round to either 1 or 0 digits if countdown is less than 1.0
			CooldownTextValue = UKismetTextLibrary::Conv_FloatToText(CooldownCountDownValue, ERoundingMode::HalfToEven, false, 1, 324, 0, 0);
		}
		else {

			CooldownTextValue = UKismetTextLibrary::Conv_FloatToText(CooldownCountDownValue, ERoundingMode::HalfToEven, false, 1, 324, 0, 1);
		}

		if (HotKey->CooldownTextBlock)
			HotKey->CooldownTextBlock->SetText(CooldownTextValue);
	}
}

void ASkill::OnTimelineCastingFinished() {

	OnCastingCompleted();
}


void ASkill::OnCastingCompleted() {

	RepPlayerReference->SkillMainWidget->CastingSpellOverlay->SetVisibility(ESlateVisibility::Hidden);
}

//---------------------------------------------------- Play Animation Montages -----------------------------------------------------
void ASkill::PlayAnimation(UAnimMontage* Animation, float InPlayRate) {

	OnPlayAnimation();
	RepPlayerReference->PlayAnimMontage(Animation, InPlayRate);
	float  sequenceLength = Animation->SequenceLength / InPlayRate;
	GetWorld()->GetTimerManager().SetTimer(SkillAnimationHandler, this, &ASkill::OnPlayAnimationCompleted, sequenceLength);

}

void ASkill::OnPlayAnimation() {

	RepPlayerReference->GetCharacterMovement()->DisableMovement();
	RepPlayerReference->GetCharacterMovement()->StopMovementImmediately();
}

void ASkill::OnPlayAnimationCompleted() {

	RepPlayerReference->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
	OnSpellCastCompleted();

}


void ASkill::OnSkillNotify(){
	
	UWorld* World = GetWorld();
	if (World) {

		FVector Location = RepPlayerReference->GetActorLocation();
		SpawnEffects(World, Location);
	}

}

 void ASkill::SpawnEffects(UWorld* World, FVector Location) {

	 if (SkillSound)
		 UGameplayStatics::PlaySoundAtLocation(World, SkillSound, Location);

	 if (SkillLaunchEffect) {
		 UWorld* LocalWorld = GetWorld();
		 FVector LocalLocation = RepPlayerReference->GetActorLocation();
		 UGameplayStatics::SpawnEmitterAtLocation(LocalWorld, SkillLaunchEffect, LocalLocation);
	 }
}

