// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "AoeSkill.h"



void AAoeSkill::OnSkillNotify() {

	Super::OnSkillNotify();

	UWorld* World = GetWorld();
	if (World) {

		FVector Location = RepPlayerReference->GetActorLocation();
		if (SkillLaunchEffect) {
			UGameplayStatics::SpawnEmitterAtLocation(World, SkillLaunchEffect, Location);

			if (SkillSound)
				UGameplayStatics::PlaySoundAtLocation(World, SkillSound, Location);

			TArray<FHitResult> OutHits;
			bool HasHits = UCombatFunctionLibrary::TraceSphere(this, RepPlayerReference->GetActorLocation(), RepPlayerReference->GetActorLocation(), GetCurrentStage().AreaRadius, OutHits, ECollisionChannel::ECC_Pawn);
			if (HasHits) {

				GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Green, " sphere hits ");

				for (int i = 0; i < OutHits.Num(); i++) {

					AActor* HitActor = OutHits[i].GetActor();
					if (HitActor) {

						IDamageable* Damageable = Cast<IDamageable>(HitActor);
						if (Damageable) {

							FSkillStage SkillStage = GetCurrentStage();
							float AttackerDamage = RepPlayerReference->BaseDamage * SkillStage.Damage;
							//	Damageable->OnReceiveDamage(AttackerDamage, SkillStage.DamageType, SkillInfo.Element, SkillStage.CriticalChance, RepPlayerReference, this);
						}
					}
				}
			}
		}
	}
}

bool AAoeSkill::VTraceSphere(AActor* ActorToIgnore, const FVector& Start, const FVector& End, const float Radius, TArray<FHitResult> OutHits, ECollisionChannel TraceChannel) {

	FCollisionQueryParams TraceParams(FName(TEXT("Replication Skill Trace")), true, ActorToIgnore);
	TraceParams.bTraceComplex = true;
	//TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;

	//Ignore Actors
	TraceParams.AddIgnoredActor(ActorToIgnore);

	//Re-initialize hit info
	//OutHits = FHitResult(ForceInit);

	return GetWorld()->SweepMultiByChannel(OutHits, Start, End, FQuat(), TraceChannel, FCollisionShape::MakeSphere(Radius), TraceParams);
}


