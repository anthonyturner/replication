// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillSystem/Skills/Skill.h"
#include "AoeSkill.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AAoeSkill : public ASkill
{
	GENERATED_BODY()
	
public:
	virtual void OnSkillNotify() override;

private:
	bool VTraceSphere(AActor* ActorToIgnore, const FVector& Start, const FVector& End, const float Radius, TArray<FHitResult> OutHits, ECollisionChannel TraceChannel = ECC_Pawn);
};
