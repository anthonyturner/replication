// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "MissileSkill.h"


AMissileSkill::AMissileSkill() {

	SkillInfo.Target = ETargetTypes::SelectedEnemy;
}


void AMissileSkill::OnSkillNotify() {///Code that actually launches the Missile spell

	Super::OnSkillNotify();

	UWorld* World = GetWorld();
	if (World) {

		if (MagicProjectileClass) {

			FVector Location = RepPlayerReference->GetActorLocation() + FVector(0.0f, 0.0f, 80.f);
			LaunchMissile(World, Location);
		}
	}
}

void AMissileSkill::LaunchMissile(UWorld* World, FVector Location) {

	FTransform SpawnLocAndRotation;
	SpawnLocAndRotation.SetLocation(Location);

	//Direction to launch missile
	FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(Location, RepPlayerReference->SelectedEnemy->GetActorLocation());
	SpawnLocAndRotation.SetRotation(Rotation.Quaternion());

	AMagicProjectile* MagicProjectile = World->SpawnActorDeferred<AMagicProjectile>(MagicProjectileClass, SpawnLocAndRotation, RepPlayerReference);
	MagicProjectile->Target = RepPlayerReference->SelectedEnemy;
	MagicProjectile->Skill = this;
	MagicProjectile->MissleEffectParticle = MissleEffectParticle;
	MagicProjectile->ImpactEffectParticle = ImpactEffectParticle;
	MagicProjectile->FinishSpawning(SpawnLocAndRotation);
}

void AMissileSkill::StopTimer() {


	GetWorld()->GetTimerManager().ClearTimer(DistanceCheckTimerHandle);
	DistanceCheckTimerHandle.Invalidate();
	RepPlayerReference->RepPlayerController->MissileSpell = nullptr;
}

void AMissileSkill::InitializeSpellCast() {

	if (InAttackRange()) {

		ManaCheck();

	}
	else {

		GetWorld()->GetTimerManager().SetTimer(DistanceCheckTimerHandle, this, &AMissileSkill::DistanceCheck, .2f, true);
		RepPlayerReference->RepPlayerController->MissileSpell = this;
	}

}

void AMissileSkill::OnTryCastSpell() {

	if (RepPlayerReference->SelectedEnemy && !bIsCoolDown && !bIsCurrentlyCasted && !(RepPlayerReference->bIsCasting)) {

		InitializeSpellCast();
	}

}


void AMissileSkill::OnSpellCast() {

	FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(RepPlayerReference->GetActorLocation(), RepPlayerReference->SelectedEnemy->GetActorLocation());
	RepPlayerReference->SetActorRotation(LookAtRotation);
	Super::OnSpellCast();

}

void AMissileSkill::DistanceCheck() {

	if (RepPlayerReference->SelectedEnemy) {
		if (InAttackRange()) {
			StopTimer();
			ManaCheck();
		}
		else {

			AActor*Actor = Cast<AActor>(RepPlayerReference->SelectedEnemy);
			if (Actor)
				UNavigationSystem::SimpleMoveToActor(RepPlayerReference->RepPlayerController, Actor);
			else {
				GEngine->AddOnScreenDebugMessage(INDEX_NONE, 3.f, FColor::Red, "AMissileSkill::DistanceCheck() - Failed casting to AActor for enemy.");
			}
		}

	}
	else {

		StopTimer();
	}
}

bool AMissileSkill::InAttackRange() {

	FSkillStage SkillStage = GetCurrentStage();
	const AActor* Enemy = Cast<AActor>(RepPlayerReference->SelectedEnemy);
	if (Enemy)
		return RepPlayerReference->GetDistanceTo(Enemy) <= SkillStage.Range;
	else {
		return false;
	}
}



