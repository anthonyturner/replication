// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillSystem/Skills/Skill.h"
#include "MissileSkill.generated.h"

/**
 * 
 */
UCLASS()
class REPLICATION_API AMissileSkill : public ASkill
{
	GENERATED_BODY()
	


public:

	AMissileSkill();

	bool InAttackRange();

	UFUNCTION()
		void DistanceCheck();

	void StopTimer();

	UPROPERTY()
		FTimerHandle DistanceCheckTimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		TSubclassOf<AMagicProjectile>MagicProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		UParticleSystem* MissleEffectParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		UParticleSystem* ImpactEffectParticle;

protected:
	virtual void OnSkillNotify() override;
	virtual void OnTryCastSpell() override;
	virtual void InitializeSpellCast() override;
	virtual void OnSpellCast() override;
	virtual void LaunchMissile(UWorld* World, FVector Location);
	
};
