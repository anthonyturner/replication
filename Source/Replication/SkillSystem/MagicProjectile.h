// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MagicProjectile.generated.h"

class AEnemyCharacter;
class ASkill;

UCLASS()
class REPLICATION_API AMagicProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMagicProjectile();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		USphereComponent* SphereCollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		UProjectileMovementComponent* MagicProjectileMovementComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		UParticleSystemComponent* ParticleSystemComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		UParticleSystem* MissleEffectParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		UParticleSystem* ImpactEffectParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		AEnemyCharacter* Target;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile)
		ASkill* Skill;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnMagicProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

};
