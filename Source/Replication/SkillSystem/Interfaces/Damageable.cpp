// Fill out your copyright notice in the Description page of Project Settings.

#include "Replication.h"
#include "Damageable.h"


// Add default functionality here for any IDamageable functions that are not pure virtual.
void IDamageable::OnReceiveDamage(float BaseDamage, EDamageType Type, TSubclassOf<AMasterElement> ElementClass, int CriticalChance, ABaseCharacter* Attacker, ASkill* Spell) {


}