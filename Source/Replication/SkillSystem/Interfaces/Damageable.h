// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SkillSystem/Enums/EDamageType.h"
#include "Damageable.generated.h"


class AMasterElement;
class ASkill;
UINTERFACE(MinimalAPI)
class UDamageable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class REPLICATION_API IDamageable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION()
	virtual void OnReceiveDamage(float BaseDamage, EDamageType Type, TSubclassOf<AMasterElement> ElementClass, int CriticalChance, ABaseCharacter* Attacker, ASkill* Spell);
};
