// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Replication.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Replication, "Replication" );

//Logging for Critical Errors that must always be addressed
DEFINE_LOG_CATEGORY(LogUI);
DEFINE_LOG_CATEGORY(LogReplication);

